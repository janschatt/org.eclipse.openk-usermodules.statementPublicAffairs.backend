#!/bin/sh
echo ------- Login Keycloak -------
sh kcadm.sh config credentials --server http://localhost:8380/auth --realm master --user admin --password admin
realmVar="OpenKRealm"
# ***************** CREATING STA ROLES ****************
role="spa_access"
echo ------- Creating Role: $role
sh kcadm.sh create roles -r $realmVar -s name=$role
role="spa_official_in_charge"
echo ------- Creating Role: $role
sh kcadm.sh create roles -r $realmVar -s name=$role
role="spa_approver"
echo ------- Creating Role: $role
sh kcadm.sh create roles -r $realmVar -s name=$role
role="spa_division_member"
echo ------- Creating Role: $role
sh kcadm.sh create roles -r $realmVar -s name=$role
role="spa_customer"
echo ------- Creating Role: $role
sh kcadm.sh create roles -r $realmVar -s name=$role
role="spa_admin"
echo ------- Creating Role: $role
sh kcadm.sh create roles -r $realmVar -s name=$role

# ***************** CREATING NEW USER *****************
# SPA admin 
usernameVar="spa_admin"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Spa -s lastName=Admin  -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename spa_access --rolename spa_admin -r $realmVar

# SPA official in charge (Sachbearbeiter)
usernameVar="spa_oic"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Test -s lastName=Sachbearbeiter  -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename spa_access --rolename spa_official_in_charge -r $realmVar
echo roles set

# SPA approver (Genehmiger)
usernameVar="spa_approver"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Test -s lastName=Genehmiger  -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename spa_access --rolename spa_approver -r $realmVar

# SPA division member (Fachbereichsarbeiter)
usernameVar="spa_division_member"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Test -s lastName=Fachbereichsarbeiter  -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename spa_access --rolename spa_division_member -r $realmVar

# SPA customer (einfacher Kunde, not used at the moment)
usernameVar="spa_customer"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Test -s lastName=Kunde  -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename spa_access --rolename spa_customer -r $realmVar

# SPA multi (Sachbearbeiter, Genehmiger, Fachbereichsarbeiter)
usernameVar="spa_multi"
echo ------- Creating User: $usernameVar -------
sh kcadm.sh create users -s username=$usernameVar -s firstName=Test -s lastName=Mitarbeiter  -s enabled=true -r $realmVar
sh kcadm.sh set-password -r $realmVar --username $usernameVar --new-password $usernameVar
echo pwd set
sh kcadm.sh add-roles --uusername $usernameVar --rolename spa_access --rolename spa_approver --rolename spa_division_member --rolename spa_official_in_charge -r $realmVar

sh kcadm.sh get roles -r $realmVar
