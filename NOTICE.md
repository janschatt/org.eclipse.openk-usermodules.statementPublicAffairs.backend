# Notices for Eclipse openK User Modules

This content is produced and maintained by the Eclipse openK User Modules project.

 * Project home: https://projects.eclipse.org/projects/technology.openk-usermodules

## Trademarks

Eclipse openK User Modules is a trademark of the Eclipse Foundation.

## Copyright

All content is the property of the respective authors or their employers.
For more information regarding authorship of content, please consult the
listed source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/openk-usermodules
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.elogbook
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.elogbookFE
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.gridFailureInformation.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.gridFailureInformation.documentation
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.gridFailureInformation.frontend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.mics.centralService
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.mics.homeService
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.documentation
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.frontend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.docu
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.documentation
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.frontend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.statementPublicAffairs.backend
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.statementPublicAffairs.documentation
* https://git.eclipse.org/r/plugins/gitiles/openk-usermodules/org.eclipse.openk-usermodules.statementPublicAffairs.frontend

## Third-party Content

antlr (2.7.7)
 * License: BSD
 * Project: http://www.antlr.org/ 
 * Source: https://mvnrepository.com/artifact/antlr/antlr/2.7.7

logback-classic (1.2.3)
 * License: EPL 1.0 LGPL 2.1
 * Project: http://logback.qos.ch
 * Source: https://mvnrepository.com/artifact/ch.qos.logback/logback-classic/1.2.3

logback-core (1.2.3)
 * License: EPL 1.0 LGPL 2.1
 * Project: http://logback.qos.ch
 * Source: https://mvnrepository.com/artifact/ch.qos.logback/logback-core/1.2.3

proj4j (1.1.0)
 * License: Apache-2.0
 * Project: https://github.com/locationtech/proj4j
 * Source: https://mvnrepository.com/artifact/org.locationtech.proj4j/proj4j/1.1.0

jackson-annotations (2.10.0)
 * License: Apache-2.0
 * Project: http://github.com/FasterXML/jackson
 * Source: https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-annotations/2.10.0

jackson-core (2.10.0)
 * License: Apache-2.0
 * Project: http://github.com/FasterXML/jackson
 * Source: https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core/2.10.0

jackson-databind (2.10.0)
 * License: Apache-2.0
 * Project: http://github.com/FasterXML/jackson
 * Source: https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind/2.10.0

jackson-datatype-jdk8 (2.10.0)
 * License: Apache-2.0
 * Project: http://github.com/FasterXML/jackson
 * Source: https://mvnrepository.com/artifact/com.fasterxml.jackson.datatype/jackson-datatype-jdk8/2.10.0

jackson-datatype-jsr310 (2.10.0)
 * License: Apache-2.0
 * Project: http://github.com/FasterXML/jackson
 * Source: https://mvnrepository.com/artifact/com.fasterxml.jackson.datatype/jackson-datatype-jsr310/2.10.0

jackson-module-parameter-names (2.10.0)
 * License: Apache-2.0
 * Project: http://github.com/FasterXML/jackson
 * Source: https://mvnrepository.com/artifact/com.fasterxml.jackson.module/jackson-module-parameter-names/2.10.0

classmate (1.5.1)
 * License: Apache-2.0
 * Project: https://github.com/FasterXML/java-classmate
 * Source: https://mvnrepository.com/artifact/com.fasterxml/classmate/1.5.1

guava (20.0)
 * License: Apache-2.0
 * Project: https://github.com/google/guava
 * Source: https://mvnrepository.com/artifact/com.google.guava/guava/20.0

h2 (1.4.200)
 * License: EPL-1.0, MPL-2.0
 * Project: https://h2database.com
 * Source: https://mvnrepository.com/artifact/com.h2database/h2/1.4.200

archaius-core (0.7.6)
 * License: Apache-2.0
 * Project: https://github.com/Netflix/archaius
 * Source: https://mvnrepository.com/artifact/com.netflix.archaius/archaius-core/0.7.6

hystrix-core (1.5.18)
 * License: Apache-2.0
 * Project: https://github.com/Netflix/Hystrix
 * Source: https://mvnrepository.com/artifact/com.netflix.hystrix/hystrix-core/1.5.18

netflix-commons-util (0.1.1)
 * License: Apache-2.0
 * Project: https://github.com/Netflix/netflix-commons 
 * Source: https://mvnrepository.com/artifact/com.netflix.netflix-commons/netflix-commons-util/0.1.1

netflix-statistics (0.1.1)
 * License: Apache-2.0
 * Project: https://github.com/Netflix/netflix-commons
 * Source: https://mvnrepository.com/artifact/com.netflix.netflix-commons/netflix-statistics/0.1.1

ribbon-core (2.3.0)
 * License: Apache-2.0
 * Project: https://github.com/Netflix/ribbon 
 * Source: https://mvnrepository.com/artifact/com.netflix.ribbon/ribbon-core/2.3.0

ribbon-httpclient (2.3.0)
 * License: Apache-2.0
 * Project: https://github.com/Netflix/ribbon 
 * Source: https://mvnrepository.com/artifact/com.netflix.ribbon/ribbon-httpclient/2.3.0

ribbon-loadbalancer (2.3.0)
 * License: Apache-2.0
 * Project: https://github.com/Netflix/ribbon 
 * Source: https://mvnrepository.com/artifact/com.netflix.ribbon/ribbon-loadbalancer/2.3.0

ribbon-transport (2.3.0)
 * License: Apache-2.0
 * Project: https://github.com/Netflix/ribbon 
 * Source: https://mvnrepository.com/artifact/com.netflix.ribbon/ribbon-transport/2.3.0

ribbon (2.3.0)
 * License: Apache-2.0
 * Project: https://github.com/Netflix/ribbon 
 * Source: https://mvnrepository.com/artifact/com.netflix.ribbon/ribbon/2.3.0


servo-core (0.10.1)
 * License: Apache-2.0
 * Project: https://github.com/Netflix/servo
 * Source: https://mvnrepository.com/artifact/com.netflix.servo/servo-core/0.10.1

servo-internal (0.10.1)
 * License: Apache-2.0
 * Project: https://github.com/Netflix/servo
 * Source: https://mvnrepository.com/artifact/com.netflix.servo/servo-internal/0.10.1

jakarta.activation (1.2.1)
 * License: EDL-1.0
 * Project: https://projects.eclipse.org/projects/ee4j.jaf 
 * Source: https://mvnrepository.com/artifact/com.sun.activation/jakarta.activation/1.2.1

istack-commons-runtime (3.0.8)
 * License: EDL-1.0
 * Project: 
 * Source: https://mvnrepository.com/artifact/com.sun.istack/istack-commons-runtime/3.0.8

jersey-apache-client4 (1.19.1)
 * License: CDDL-1.1, GPL-1.1 
 * Project:
 * Source: https://mvnrepository.com/artifact/com.sun.jersey.contribs/jersey-apache-client4/1.19.1

jersey-client (1.19.1)
 * License: CDDL-1.1, GPL-1.1 
 * Project: 
 * Source: https://mvnrepository.com/artifact/com.sun.jersey/jersey-client/1.19.1

jersey-core (1.19.1)
 * License: CDDL-1.1, GPL-1.1 
 * Project: 
 * Source: https://mvnrepository.com/artifact/com.sun.jersey/jersey-core/1.19.1

jakarta.mail (1.6.4)
 * License: EDL-1.0, EPL-2.0
 * Project: https://eclipse-ee4j.github.io/mail/ 
 * Source: https://mvnrepository.com/artifact/com.sun.mail/jakarta.mail/1.6.4

FastInfoset (1.2.16)
 * License: Apache-2.0, EDL-1.0 
 * Project: 
 * Source: https://mvnrepository.com/artifact/com.sun.xml.fastinfoset/FastInfoset/1.2.16

HikariCP (3.4.1)
 * License: Apache-2.0 
 * Project: https://github.com/brettwooldridge/HikariCP 
 * Source: https://mvnrepository.com/artifact/com.zaxxer/HikariCP/3.4.1

commons-codec (1.13)
 * License: Apache-2.0 
 * Project: https://commons.apache.org/proper/commons-codec/
 * Source: https://mvnrepository.com/artifact/commons-codec/commons-codec/1.13

commons-collections (3.2.2)
 * License: Apache-2.0 
 * Project: http://commons.apache.org/collections/  
 * Source: https://mvnrepository.com/artifact/commons-collections/commons-collections/3.2.2

commons-configuration (1.8)
 * License: Apache-2.0 
 * Project: http://commons.apache.org/configuration/ 
 * Source: https://mvnrepository.com/artifact/commons-configuration/commons-configuration/1.8

commons-fileupload (1.4)
 * License: Apache-2.0 
 * Project: http://commons.apache.org/proper/commons-fileupload/ 
 * Source: https://mvnrepository.com/artifact/commons-fileupload/commons-fileupload/1.4

commons-io (2.2)
 * License: Apache-2.0 
 * Project: http://commons.apache.org/io/ 
 * Source: https://mvnrepository.com/artifact/commons-io/commons-io/2.2

commons-lang (2.6)
 * License: Apache-2.0 
 * Project: http://commons.apache.org/lang/ 
 * Source: https://mvnrepository.com/artifact/commons-lang/commons-lang/2.6

commons-logging (1.2)
 * License: Apache-2.0 
 * Project: http://commons.apache.org/proper/commons-logging/
 * Source: https://mvnrepository.com/artifact/commons-logging/commons-logging/1.2

feign-form-spring (3.8.0)
 * License: Apache-2.0 
 * Project: 
 * Source: https://mvnrepository.com/artifact/io.github.openfeign.form/feign-form-spring/3.8.0

feign-form (3.8.0)
 * License: Apache-2.0 
 * Project: 
 * Source: https://mvnrepository.com/artifact/io.github.openfeign.form/feign-form/3.8.0

feign-core (10.4.0)
 * License: Apache-2.0 
 * Project: 
 * Source: https://mvnrepository.com/artifact/io.github.openfeign/feign-core/10.4.0

feign-hystrix (10.4.0)
 * License: Apache-2.0 
 * Project: 
 * Source: https://mvnrepository.com/artifact/io.github.openfeign/feign-hystrix/10.4.0

feign-slf4j (10.4.0)
 * License: Apache-2.0 
 * Project: 
 * Source: https://mvnrepository.com/artifact/io.github.openfeign/feign-slf4j/10.4.0

jjwt (0.9.1)
 * License: Apache-2.0 
 * Project: https://github.com/jwtk/jjwt
 * Source: https://mvnrepository.com/artifact/io.jsonwebtoken/jjwt/0.9.1

rxjava (1.3.8)
 * License: Apache-2.0 
 * Project: https://github.com/ReactiveX/RxJava   
 * Source: https://mvnrepository.com/artifact/io.reactivex/rxjava/1.3.8

rxnetty-contexts (0.4.9)
 * License: Apache-2.0 
 * Project: https://github.com/ReactiveX/RxNetty 
 * Source: https://mvnrepository.com/artifact/io.reactivex/rxnetty-contexts/0.4.9

rxnetty-servo (0.4.9)
 * License: Apache-2.0 
 * Project: https://github.com/ReactiveX/RxNetty
 * Source: https://mvnrepository.com/artifact/io.reactivex/rxnetty-servo/0.4.9

rxnetty (0.4.9)
 * License: Apache-2.0 
 * Project: https://github.com/ReactiveX/RxNetty
 * Source: https://mvnrepository.com/artifact/io.reactivex/rxnetty/0.4.9

springfox-core (2.9.2)
 * License: Apache-2.0 
 * Project: https://github.com/springfox/springfox 
 * Source: https://mvnrepository.com/artifact/io.springfox/springfox-core/2.9.2

springfox-schema (2.9.2)
 * License: Apache-2.0 
 * License: 
 * Project: 
 * Project: https://github.com/springfox/springfox 
 * Source: https://mvnrepository.com/artifact/io.springfox/springfox-schema/2.9.2

springfox-spi (2.9.2)
 * License: Apache-2.0 
 * Project: https://github.com/springfox/springfox 
 * Source: https://mvnrepository.com/artifact/io.springfox/springfox-spi/2.9.2

springfox-spring-web (2.9.2)
 * License: Apache-2.0 
 * Project: https://github.com/springfox/springfox 
 * Source: https://mvnrepository.com/artifact/io.springfox/springfox-spring-web/2.9.2

springfox-swagger-common (2.9.2)
 * License: Apache-2.0 
 * Project: https://github.com/springfox/springfox 
 * Source: https://mvnrepository.com/artifact/io.springfox/springfox-swagger-common/2.9.2

springfox-swagger-ui (2.9.2)
 * License: Apache-2.0 
 * Project: https://github.com/springfox/springfox 
 * Source: https://mvnrepository.com/artifact/io.springfox/springfox-swagger-ui/2.9.2

springfox-swagger2 (2.9.2)
 * License: Apache-2.0 
 * Project: https://github.com/springfox/springfox 
 * Source: https://mvnrepository.com/artifact/io.springfox/springfox-swagger2/2.9.2

swagger-annotations (1.5.20)
 * License: Apache-2.0 
 * Project: 
 * Source: https://mvnrepository.com/artifact/io.swagger/swagger-annotations/1.5.20

swagger-models (1.5.20)
 * License: Apache-2.0 
 * Project:  
 * Source: https://mvnrepository.com/artifact/io.swagger/swagger-models/1.5.20

jakarta.activation-api (1.2.1)
 * License: EDL-1.0
 * Project: 
 * Source: https://mvnrepository.com/artifact/jakarta.activation/jakarta.activation-api/1.2.1

jakarta.annotation-api (1.3.5)
 * License: EPL-2.0
 * Project: https://projects.eclipse.org/projects/ee4j.ca 
 * Source: https://mvnrepository.com/artifact/jakarta.annotation/jakarta.annotation-api/1.3.5

jakarta.persistence-api (2.2.3)
 * License: EDL-1.0, EPL-2.0
 * Project: https://github.com/eclipse-ee4j/jpa-api 
 * Source: https://mvnrepository.com/artifact/jakarta.persistence/jakarta.persistence-api/2.2.3

jakarta.transaction-api (1.3.3)
 * License: EPL-2.0
 * Project: https://projects.eclipse.org/projects/ee4j.jta 
 * Source: https://mvnrepository.com/artifact/jakarta.transaction/jakarta.transaction-api/1.3.3

jakarta.validation-api (2.0.1)
 * License: Apache-2.0 
 * Project: http://beanvalidation.org
 * Source: https://mvnrepository.com/artifact/jakarta.validation/jakarta.validation-api/2.0.1

jakarta.xml.bind-api (2.3.2)
 * License: EDL-1.0
 * Project: 
 * Source: https://mvnrepository.com/artifact/jakarta.xml.bind/jakarta.xml.bind-api/2.3.2

javax.inject (1)
 * License: Apache-2.0
 * Project: http://code.google.com/p/atinject/
 * Source: https://mvnrepository.com/artifact/javax.inject/javax.inject/1

jsr311-api (1.1.1)
 * License: CDDL-1.0 
 * Project: https://jsr311.dev.java.net 
 * Source: https://mvnrepository.com/artifact/javax.ws.rs/jsr311-api/1.1.1

byte-buddy (1.10.2)
 * License: Apache-2.0 
 * Project: 
 * Source: https://mvnrepository.com/artifact/net.bytebuddy/byte-buddy/1.10.2

httpclient (4.5.10)
 * License: Apache-2.0 
 * Project: http://hc.apache.org/httpcomponents-client 
 * Source: https://mvnrepository.com/artifact/org.apache.httpcomponents/httpclient/4.5.10

httpcore (4.4.12)
 * License: Apache-2.0 
 * Project: http://hc.apache.org/httpcomponents-core-ga
 * Source: https://mvnrepository.com/artifact/org.apache.httpcomponents/httpcore/4.4.12

log4j-api (2.17.1)
 * License: Apache-2.0 
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-api/2.17.1

log4j-to-slf4j (2.17.1)
 * License: Apache-2.0 
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.apache.logging.log4j/log4j-to-slf4j/2.17.1

fontbox (2.0.6)
 * License: Apache-2.0 
 * Project: http://pdfbox.apache.org/
 * Source: https://mvnrepository.com/artifact/org.apache.pdfbox/fontbox/2.0.6

pdfbox (2.0.6)
 * License: Apache-2.0 
 * Project: http://pdfbox.apache.org/
 * Source: https://mvnrepository.com/artifact/org.apache.pdfbox/pdfbox/2.0.6

tomcat-embed-core (9.0.27)
 * License: Apache-2.0 
 * Project: https://tomcat.apache.org/
 * Source: https://mvnrepository.com/artifact/org.apache.tomcat.embed/tomcat-embed-core/9.0.27

tomcat-embed-el (9.0.27)
 * License: Apache-2.0 
 * Project: https://tomcat.apache.org/
 * Source: https://mvnrepository.com/artifact/org.apache.tomcat.embed/tomcat-embed-el/9.0.27

tomcat-embed-websocket (9.0.27)
 * License: Apache-2.0 
 * Project: https://tomcat.apache.org/
 * Source: https://mvnrepository.com/artifact/org.apache.tomcat.embed/tomcat-embed-websocket/9.0.27

aspectjweaver (1.9.4)
 * License: EPL-1.0
 * Project: http://www.aspectj.org
 * Source: https://mvnrepository.com/artifact/org.aspectj/aspectjweaver/1.9.4

bcpkix-jdk15on (1.56)
 * License: BouncyCastle 
 * Project: http://www.bouncycastle.org/java.html
 * Source: https://mvnrepository.com/artifact/org.bouncycastle/bcpkix-jdk15on/1.56

bcprov-jdk15on (1.56)
 * License: BouncyCastle 
 * Project: http://www.bouncycastle.org/java.html
 * Source: https://mvnrepository.com/artifact/org.bouncycastle/bcprov-jdk15on/1.56

dom4j (2.1.1)
 * License: Apache-Style, https://github.com/dom4j/dom4j/blob/master/LICENSE
 * Project: http://dom4j.github.io/
 * Source: https://mvnrepository.com/artifact/org.dom4j/dom4j/2.1.1

jaxb-runtime (2.3.2)
 * License: EDL-1.0
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.glassfish.jaxb/jaxb-runtime/2.3.2

txw2 (2.3.2)
 * License: EDL-1.0
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.glassfish.jaxb/txw2/2.3.2

HdrHistogram (2.1.9)
 * License: CCO-1.0
 * Project: http://hdrhistogram.github.io/HdrHistogram/
 * Source: https://mvnrepository.com/artifact/org.hdrhistogram/HdrHistogram/2.1.9

hibernate-commons-annotations (5.1.0.Final)
 * License: LGPL-2.1
 * Project: http://hibernate.org 
 * Source: https://mvnrepository.com/artifact/org.hibernate.common/hibernate-commons-annotations/5.1.0.Final

hibernate-validator (6.0.18.Final)
 * License: Apache-2.0 
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.hibernate.validator/hibernate-validator/6.0.18.Final

hibernate-core (5.4.8.Final)
 * License: LGPL-2.1
 * Project: http://hibernate.org/orm 
 * Source: https://mvnrepository.com/artifact/org.hibernate/hibernate-core/5.4.8.Final

javassist (3.24.0-GA)
 * License: Apache-2.0, LGPL-2.1, MPL-1.1
 * Project: http://www.javassist.org/
 * Source: https://mvnrepository.com/artifact/org.javassist/javassist/3.24.0-GA

jboss-logging (3.4.1.Final)
 * License: Apache-2.0
 * Project: http://www.jboss.org
 * Source: https://mvnrepository.com/artifact/org.jboss.logging/jboss-logging/3.4.1.Final

jandex (2.0.5.Final)
 * License: Apache-2.0
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.jboss/jandex/2.0.5.Final

stax-ex (1.8.1)
 * License: EDL-1.0
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.jvnet.staxex/stax-ex/1.8.1

keycloak-common (3.4.2.Final)
 * License: Apache-2.0
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.keycloak/keycloak-common/3.4.2.Final

keycloak-core (3.4.2.Final)
 * License: Apache-2.0
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.keycloak/keycloak-core/3.4.2.Final

mapstruct (1.2.0.Final)
 * License: Apache-2.0
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.mapstruct/mapstruct/1.2.0.Final

postgresql (42.2.8)
 * License: BSD-2-clause
 * Project: https://github.com/pgjdbc/pgjdbc 
 * Source: https://mvnrepository.com/artifact/org.postgresql/postgresql/42.2.8

lombok (1.18.10)
 * License: MIT
 * Project: https://projectlombok.org 
 * Source: https://mvnrepository.com/artifact/org.projectlombok/lombok/1.18.10

jul-to-slf4j (1.7.29)
 * License: MIT 
 * Project: http://www.slf4j.org 
 * Source: https://mvnrepository.com/artifact/org.slf4j/jul-to-slf4j/1.7.29

slf4j-api (1.7.29)
 * License: MIT 
 * Project: http://www.slf4j.org 
 * Source: https://mvnrepository.com/artifact/org.slf4j/slf4j-api/1.7.29

spring-boot-autoconfigure (2.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-autoconfigure
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-autoconfigure/2.2.1.RELEASE

spring-boot-starter-jdbc (2.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-jdbc
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-jdbc/2.2.1.RELEASE

spring-boot-starter-json (2.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-json
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-json/2.2.1.RELEASE

spring-boot-starter-logging (2.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-logging
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-logging/2.2.1.RELEASE

spring-boot-starter-mail (2.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-mail
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-mail/2.2.1.RELEASE

spring-boot-starter-security (2.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-security
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-security/2.2.1.RELEASE

spring-boot-starter-tomcat (2.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-tomcat
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-tomcat/2.2.1.RELEASE

spring-boot-starter-validation (2.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-validation
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-validation/2.2.1.RELEASE

spring-boot-starter-web (2.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter-web
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-web/2.2.1.RELEASE

spring-boot-starter (2.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot-starters/spring-boot-starter
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter/2.2.1.RELEASE

spring-boot (2.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-boot/#/spring-boot-parent/spring-boot
 * Source: https://mvnrepository.com/artifact/org.springframework.boot/spring-boot/2.2.1.RELEASE

spring-cloud-commons (2.2.0.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-cloud/spring-cloud-commons/
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-commons/2.2.0.RELEASE

spring-cloud-context (2.2.0.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-cloud/spring-cloud-context/
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-context/2.2.0.RELEASE

spring-cloud-netflix-archaius (2.2.0.RELEASE)
 * License: Apache-2.0
 * Project: https://spring.io/spring-cloud/spring-cloud-netflix/spring-cloud-netflix-archaius
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-netflix-archaius/2.2.0.RELEASE

spring-cloud-netflix-ribbon (2.2.0.RELEASE)
 * License: Apache-2.0
 * Project: https://spring.io/spring-cloud/spring-cloud-netflix/spring-cloud-netflix-ribbon
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-netflix-ribbon/2.2.0.RELEASE

spring-cloud-openfeign-core (2.2.0.RELEASE)
 * License: Apache-2.0
 * Project: https://spring.io/spring-cloud/spring-cloud-openfeign/spring-cloud-openfeign-core
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-openfeign-core/2.2.0.RELEASE

spring-cloud-starter-netflix-archaius (2.2.0.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-cloud
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-netflix-archaius/2.2.0.RELEASE

spring-cloud-starter-netflix-ribbon (2.2.0.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-cloud
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-netflix-ribbon/2.2.0.RELEASE

spring-cloud-starter-openfeign (2.2.0.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-cloud
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter-openfeign/2.2.0.RELEASE

spring-cloud-starter (2.2.0.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-cloud
 * Source: https://mvnrepository.com/artifact/org.springframework.cloud/spring-cloud-starter/2.2.0.RELEASE

spring-data-commons (2.2.1.RELEASE)
 * License: Apache-2.0
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.springframework.data/spring-data-commons/2.2.1.RELEASE

spring-data-jpa (2.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://projects.spring.io/spring-data-jpa
 * Source: https://mvnrepository.com/artifact/org.springframework.data/spring-data-jpa/2.2.1.RELEASE

spring-plugin-core (1.2.0.RELEASE)
 * License: Apache-2.0
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.springframework.plugin/spring-plugin-core/1.2.0.RELEASE

spring-plugin-metadata (1.2.0.RELEASE)
 * License: Apache-2.0
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.springframework.plugin/spring-plugin-metadata/1.2.0.RELEASE

spring-security-config (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: http://spring.io/spring-security
 * Source: https://mvnrepository.com/artifact/org.springframework.security/spring-security-config/5.2.1.RELEASE

spring-security-core (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: http://spring.io/spring-security
 * Source: https://mvnrepository.com/artifact/org.springframework.security/spring-security-core/5.2.1.RELEASE

spring-security-crypto (5.2.1.RELEASE)
 * License: Apache-2.0
 * License: 
 * Project: 
 * Source: https://mvnrepository.com/artifact/org.springframework.security/spring-security-crypto/5.2.1.RELEASE

spring-security-rsa (1.0.7.RELEASE)
 * License: Apache-2.0
 * Project: http://github.com/spring-projects/spring-security-oauth
 * Source: https://mvnrepository.com/artifact/org.springframework.security/spring-security-rsa/1.0.7.RELEASE

spring-security-web (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: http://spring.io/spring-security
 * Source: https://mvnrepository.com/artifact/org.springframework.security/spring-security-web/5.2.1.RELEASE

spring-aop (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-aop/5.2.1.RELEASE

spring-aspects (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-aspects/5.2.1.RELEASE

spring-beans (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-beans/5.2.1.RELEASE

spring-context-support (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-context-support/5.2.1.RELEASE

spring-context (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-context/5.2.1.RELEASE

spring-core (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-core/5.2.1.RELEASE

spring-expression (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-expression/5.2.1.RELEASE

spring-jcl (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-jcl/5.2.1.RELEASE

spring-jdbc (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-jdbc/5.2.1.RELEASE

spring-orm (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-orm/5.2.1.RELEASE

spring-tx (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-tx/5.2.1.RELEASE

spring-web (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-web/5.2.1.RELEASE

spring-webmvc (5.2.1.RELEASE)
 * License: Apache-2.0
 * Project: https://github.com/spring-projects/spring-framework
 * Source: https://mvnrepository.com/artifact/org.springframework/spring-webmvc/5.2.1.RELEASE

snakeyaml (1.25)
 * License: Apache-2.0
 * Project: http://www.snakeyaml.org/
 * Source: https://mvnrepository.com/artifact/org.yaml/snakeyaml/1.25


## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.
