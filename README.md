# Eclipse openK User Modules - Statement Public Affairs back end

This application represents a user module for the [Eclipse openK User Modules](https://projects.eclipse.org/projects/technology.openk-usermodules) project.

## Build

Run `mvn clean install -DskipTests` to build the project. All build artifacts will be stored in the `targets/` directory.

## Running unit tests

Run `mvn clean install` to execute the unit tests. All results will be stored in the `targets/surefire-reports` directory.

Further details about how to build, configure and run this module in a production or test environment, please see the `howToBuild` and `howToRun` documentation.