/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


/**
 * Throw exception to create not found Http responses.
 * @author Tobias Stummer
 *
 */
@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class NotFoundException extends RuntimeException{
   
    private static final String DEFAULT_MESSAGE = "Requested ressource could not be found.";
    
    public NotFoundException() {
        super(DEFAULT_MESSAGE);
    }

    /**
     * Create NotFoundException with own message. 
     * @param message Message
     */
    public NotFoundException(String message) {
        super(message);
    }
    

    /**
     * Create NotFoundException with own message and cause.
     * @param message Message
     * @param cause Cause
     */
    public NotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}

