/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.util;

import java.time.LocalDateTime;
import java.time.ZoneId;

public class Time {

	private static final ZoneId UTC_ZONE = ZoneId.of("UTC");

	private Time() {
	}

	public static LocalDateTime currentTimeLocalTimeUTC() {
		return LocalDateTime.now(UTC_ZONE);
	}

}
