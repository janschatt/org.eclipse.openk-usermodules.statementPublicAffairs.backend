/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.repository;

import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementSearch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface PagedAndSortedStatementRepository extends PagingAndSortingRepository<VwStatementSearch, Long> {
	  
	Page<VwStatementSearch> findAll(Pageable sPageable);
  
	Page<VwStatementSearch> findAll(Specification<VwStatementSearch> specification, Pageable sPageable);

}