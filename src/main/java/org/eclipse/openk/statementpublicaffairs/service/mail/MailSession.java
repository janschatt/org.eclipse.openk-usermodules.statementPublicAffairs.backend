/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service.mail;

import java.util.Properties;

import javax.mail.NoSuchProviderException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMessage;


public class MailSession {

	private Session session;

	public MailSession(Properties properties) {
		session = Session.getInstance(properties, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(properties.getProperty("mail.imap.user"),
						properties.getProperty("mail.imap.password"));
			}
		});	
	}

	public String getProperty(String property) {
		return session.getProperty(property);
	}

	public MimeMessage newMessage() {
		return new MimeMessage(session);
	}

	public Store getStore(String store) throws NoSuchProviderException {
		return session.getStore(store);
	}
	
	

	
}
