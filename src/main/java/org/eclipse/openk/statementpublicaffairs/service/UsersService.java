/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.openk.statementpublicaffairs.api.AuthNAuthApi;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.KeyCloakUser;
import org.eclipse.openk.statementpublicaffairs.model.ReqDepartmentDetailsModel;
import org.eclipse.openk.statementpublicaffairs.model.UserModel;
import org.eclipse.openk.statementpublicaffairs.model.conf.AuthorizationRuleActions;
import org.eclipse.openk.statementpublicaffairs.model.conf.Rule;
import org.eclipse.openk.statementpublicaffairs.model.db.TblUser;
import org.eclipse.openk.statementpublicaffairs.model.db.TblUser2Department;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementReqdepartmentUsers;
import org.eclipse.openk.statementpublicaffairs.repository.User2DepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.UserRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementReqdepartmentUsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * UsersService uses the AuthNAuthApi to manage users.
 * 
 * @author Tobias Stummer
 *
 */
@Service
public class UsersService {

	/**
	 * AuthNAuthApi to access the authNAuth service.
	 */
	@Autowired
	private AuthNAuthApi authNAuthApi;

	@Autowired
	private SessionService sessionService;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private StatementAuthorizationService authorizationService;

	@Autowired
	private VwStatementReqdepartmentUsersRepository vwStatementReqDepartmentUsersRepository;

	@Autowired
	private User2DepartmentRepository user2DepartmentRepository;

	@Autowired
	private UserInfoService userInfoService;

	@Value("${jwt.useStaticJwt}")
	private boolean useStaticJwt;

	@Value("${authnauth.technical-username}")
	private String technicalUserName;

	@Value("${authnauth.technical-userpassword}")
	private String technicalUserPassword;

	private List<UserModel> getKcUsersWithRole(String role) throws InternalErrorServiceException {
		String token = sessionService.getToken(technicalUserName, technicalUserPassword);

		List<KeyCloakUser> response = authNAuthApi.getUsersForRole(token, UserRoles.keyCloakRoleIdof(role));

		List<UserModel> userModels = new ArrayList<>();
		for (KeyCloakUser kcUser : response) {
			UserModel userModel = new UserModel();
			userModel.setFirstName(kcUser.getFirstName());
			userModel.setLastName(kcUser.getLastName());
			userModel.setUsername(kcUser.getUsername());
			Set<String> roles = new HashSet<>();
			if (kcUser.getAllRoles() != null) {
				roles.addAll(kcUser.getAllRoles().stream().map(UserRoles::userRoleOfKeyCloakRoleId)
						.collect(Collectors.toList()));
			}
			if (kcUser.getRealmRoles() != null) {
				roles.addAll(kcUser.getRealmRoles().stream().map(UserRoles::userRoleOfKeyCloakRoleId)
						.collect(Collectors.toList()));
			}
			userModel.setRoles(new ArrayList<>(roles));
			userModels.add(userModel);
		}
		return userModels;

	}

	public void syncKeycloakUsers() throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_SYNC_KEYCLOAK_USERS);
		List<UserModel> spaUsers = getKcUsersWithRole(UserRoles.SPA_ACCESS);
		for (UserModel spaUser : spaUsers) {
			createIfUnknownUser(spaUser);
		}
	}

	public List<UserModel> getUsersWithRole(String role) throws InternalErrorServiceException {
		List<UserModel> userModels = getKcUsersWithRole(role);
		for (UserModel userModel : userModels) {
			List<TblUser> users = userRepository.findByUsername(userModel.getUsername());
			if (users.size() == 1) {
				TblUser user = users.get(0);
				userModel.setEmailAddress(user.getEmailAddress());
				userModel.setPhone(user.getPhone());
				userModel.setFax(user.getFax());
				userModel.setInitials(user.getInitials());
			}
		}
		return userModels;
	}

	private void createIfUnknownUser(UserModel spaUser) {
		List<TblUser> users = userRepository.findByUsername(spaUser.getUsername());
		if (users.isEmpty()) {
			TblUser user = new TblUser();
			user.setUsername(spaUser.getUsername());
			user.setFirstName(spaUser.getFirstName());
			user.setLastName(spaUser.getLastName());
			userRepository.save(user);
		}
	}

	public Set<UserModel> getUsersOfRequiredDivisionsOfStatement(Long statementId) {
		Set<UserModel> userModels = new HashSet<>();
		List<VwStatementReqdepartmentUsers> users = vwStatementReqDepartmentUsersRepository
				.findByStatementId(statementId);
		for (VwStatementReqdepartmentUsers user : users) {
			UserModel userModel = new UserModel();
			userModel.setFirstName(user.getFirstName());
			userModel.setLastName(user.getLastName());
			userModel.setUsername(user.getUserName());
			userModel.setEmailAddress(user.getEmailAddress());
			userModels.add(userModel);
		}

		return userModels;
	}

	public Optional<TblUser> getTblUser(String username) {
		List<TblUser> users = userRepository.findByUsername(username);
		if (users.size() != 1) {
			return Optional.empty();
		}
		return Optional.of(users.get(0));
	}

	public UserModel userOfRequiredDivisionsOfStatement(Long statementId, String username) {
		UserModel user = null;
		if (username == null) {
			return null;
		}
		for (UserModel userModel : getUsersOfRequiredDivisionsOfStatement(statementId)) {
			if (username.equals(userModel.getUsername())) {
				user = userModel;
				break;
			}
		}
		return user;
	}

	public List<ReqDepartmentDetailsModel> requiredDepartmentUsers(Long statementId) {
		List<ReqDepartmentDetailsModel> rdms = new ArrayList<>();

		List<VwStatementReqdepartmentUsers> reqDepartmentUsers = vwStatementReqDepartmentUsersRepository
				.findByStatementId(statementId);
		Map<Long, ReqDepartmentDetailsModel> reqDepartmentDetailsModels = new HashMap<>();

		for (VwStatementReqdepartmentUsers reqDepartmentUser : reqDepartmentUsers) {
			if (!reqDepartmentDetailsModels.containsKey(reqDepartmentUser.getDepartmentId())) {
				ReqDepartmentDetailsModel rdm = new ReqDepartmentDetailsModel();
				rdm.setOptional(reqDepartmentUser.getDepartmentOptional());
				rdm.setContributed(reqDepartmentUser.getDepartmentContributed());
				rdm.setDepartmentGroup(reqDepartmentUser.getDepartmentGroup());
				rdm.setDepartmentName(reqDepartmentUser.getDepartmentName());
				rdm.setDepartmentUsers(new ArrayList<>());
				reqDepartmentDetailsModels.put(reqDepartmentUser.getDepartmentId(), rdm);
			}

			ReqDepartmentDetailsModel rdm = reqDepartmentDetailsModels.get(reqDepartmentUser.getDepartmentId());
			UserModel uModel = new UserModel();
			uModel.setEmailAddress(reqDepartmentUser.getEmailAddress());
			uModel.setFirstName(reqDepartmentUser.getFirstName());
			uModel.setLastName(reqDepartmentUser.getLastName());
			uModel.setUsername(reqDepartmentUser.getUserName());
			rdm.getDepartmentUsers().add(uModel);
		}
		rdms.addAll(reqDepartmentDetailsModels.values());
		return rdms;
	}

	public boolean isRequiredDepartmentUser(Long statementId, String userName) {
		boolean isReqDepartmentUser = false;
		List<VwStatementReqdepartmentUsers> reqDepartmentUsers = vwStatementReqDepartmentUsersRepository
				.findByStatementId(statementId);
		for (VwStatementReqdepartmentUsers reqDepartmentUser : reqDepartmentUsers) {
			if (userName.equals(reqDepartmentUser.getUserName())) {
				isReqDepartmentUser = true;
				break;
			}
		}
		return isReqDepartmentUser;
	}

	/**
	 * Get departmentIds for username.
	 * 
	 * @param username username
	 * @return id of department, the user is assigned to. Optional.empty if user is
	 *         not assigned to any department.
	 */
	public Set<Long> departmentIds(String username) {

		Set<Long> userDepartmentIds = new HashSet<>();
		List<TblUser> users = userRepository.findByUsername(username);
		if (!users.isEmpty()) {
			TblUser user = users.get(0);
			List<TblUser2Department> userDepartments = user2DepartmentRepository.findByUserId(user.getId());
			for (TblUser2Department user2Department : userDepartments) {
				userDepartmentIds.add(user2Department.getDepartmentId());
			}
		}
		return userDepartmentIds;
	}

	public Map<Long, Boolean> departmentsStandIns(String username) {
		Map<Long, Boolean> ownDepartmentsStandIns = new HashMap<>();
		List<TblUser> users = userRepository.findByUsername(username);
		if (!users.isEmpty()) {
			TblUser user = users.get(0);
			List<TblUser2Department> userDepartments = user2DepartmentRepository.findByUserId(user.getId());
			for (TblUser2Department user2Department : userDepartments) {
				ownDepartmentsStandIns.put(user2Department.getDepartmentId(), user2Department.getStandIn());
			}
		}
		return ownDepartmentsStandIns;
	}

	/**
	 * Evaluate if it will be allowed to claim a task with the taskDefinitionKey of
	 * the statement with the given statementId by the current user.
	 * 
	 * @param statementId       statement id
	 * @param taskDefinitionKey task definition key
	 * @return true if allowed. Otherwise false.
	 */
	public boolean isAllowedToClaimTask(Long statementId, String taskDefinitionKey) {
		boolean allowed;
		switch (authorizationService.evaluateAuthorization(taskDefinitionKey)) {

		case ACCEPTED:
			allowed = true;
			break;
		case ONLY_REQ_DEPARTMENT_USER:
			allowed = isRequiredDepartmentUser(statementId, userInfoService.getUserName());
			break;
		case DENIED:
		default:
			allowed = false;
		}
		return allowed;
	}

	/**
	 * Get identifier of department, the current user is assigned to.
	 * 
	 * @return id of department, the user is assigned to. Optional.empty if user is
	 *         not assigned to any department.
	 */
	public Set<Long> ownDepartmentIds() {
		return departmentIds(userInfoService.getUserName());
	}
	
	public Map<Long, Boolean> ownDepartmentsStandIns() {
		return departmentsStandIns(userInfoService.getUserName());
	}

	/**
	 * Get database identifier of user with username.
	 * 
	 * @param username username to search for
	 * @return userId if successful. Otherwise Optional.empty
	 */
	public Optional<Long> getUserId(String username) {
		List<TblUser> users = userRepository.findByUsername(username);
		if (users.isEmpty()) {
			return Optional.empty();
		}
		TblUser user = users.get(0);
		return Optional.of(user.getId());
	}
	
	
	public Optional<Long> getOwnUserId() {
		return getUserId(userInfoService.getUserName());
	}

}
