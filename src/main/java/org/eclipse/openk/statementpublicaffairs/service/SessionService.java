/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.statementpublicaffairs.api.AuthNAuthApi;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.JwtToken;
import org.eclipse.openk.statementpublicaffairs.model.LoginCredentials;
import org.eclipse.openk.statementpublicaffairs.model.UserDetails;
import org.keycloak.RSATokenVerifier;
import org.keycloak.common.VerificationException;
import org.keycloak.representations.AccessToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import feign.FeignException;
import feign.Response;
import lombok.extern.java.Log;

/**
 * SessionService uses the AuthNAuthApi to manage session states.
 * 
 * @author Tobias Stummer
 *
 */
@Log
@Service
public class SessionService {

	/**
	 * AuthNAuthApi to access the authNAuth service.
	 */
	@Autowired
	private AuthNAuthApi authNAuthApi;

	@Value("${jwt.useStaticJwt}")
	private boolean useStaticJwt;

	/**
	 * Invalidate the provided authentication token.
	 */
	public int logout() {
		String bearerToken = (String) SecurityContextHolder.getContext().getAuthentication().getDetails();
		feign.Response resp = authNAuthApi.logout(bearerToken);
		return resp.status();
	}

	public boolean validateToken(String token) {
		if (useStaticJwt) {
			return true;
		}
		Response res = authNAuthApi.isTokenValid(token);
		return res.status() == HttpStatus.OK.value();
	}

	public UsernamePasswordAuthenticationToken getTokenDetails(String bearerToken) throws VerificationException {
		AccessToken token = RSATokenVerifier.create(bearerToken).getToken();

		List<GrantedAuthority> authorities = new ArrayList<>();
		token.getRealmAccess().getRoles().stream()
				.forEach(x -> authorities.add(new SimpleGrantedAuthority("ROLE_" + x.toUpperCase())));

		UserDetails userDetails = new UserDetails();
		userDetails.setUserName(token.getPreferredUsername());
		userDetails.setName(token.getName());
		userDetails.setFirstName(token.getGivenName());
		userDetails.setLastName(token.getFamilyName());
		return new UsernamePasswordAuthenticationToken(userDetails, null, authorities);
	}

	public List<String> getRoles(String token) {
		List<String> roles = new ArrayList<>();
		if (validateToken(token)) {
			try {
				UsernamePasswordAuthenticationToken uat = getTokenDetails(token);
				for (GrantedAuthority a : uat.getAuthorities()) {
					roles.add(a.getAuthority());
				}
			} catch (VerificationException e) {
				log.warning(
						"Verification of valid token caused exception: " + e.getMessage() + " - " + e.getStackTrace());
			}
		}
		return roles;
	}

	public boolean hasRoles(List<String> requiredRoles, String token) {
		List<String> roles = getRoles(token);
		return roles.containsAll(requiredRoles);
	}

	public String getToken(String username, String password) throws InternalErrorServiceException {
		JwtToken token;
		try {
			token = authNAuthApi.login(new LoginCredentials(username, password));
		} catch (FeignException e) {
			throw new InternalErrorServiceException("Error occurred accessing the authNAuthApi login.", e);
		}
		return token.getAccessToken();
	}

}
