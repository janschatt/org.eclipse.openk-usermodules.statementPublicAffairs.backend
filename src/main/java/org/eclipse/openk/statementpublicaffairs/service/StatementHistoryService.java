/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.HistoryEntryContentModel;
import org.eclipse.openk.statementpublicaffairs.model.HistoryEntryContentModel.DiffType;
import org.eclipse.openk.statementpublicaffairs.model.HistoryEntryContentModel.MoveType;
import org.eclipse.openk.statementpublicaffairs.model.HistoryEntryModel;
import org.eclipse.openk.statementpublicaffairs.model.HistoryModel;
import org.eclipse.openk.statementpublicaffairs.model.Textblock;
import org.eclipse.openk.statementpublicaffairs.model.Textblock.TextblockType;
import org.eclipse.openk.statementpublicaffairs.model.TextblockDefinition;
import org.eclipse.openk.statementpublicaffairs.model.conf.AuthorizationRuleActions;
import org.eclipse.openk.statementpublicaffairs.model.conf.Rule;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.TblWorkflowdata;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementdraftHistory;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementDraftHistoryRepository;
import org.eclipse.openk.statementpublicaffairs.service.compile.TextCompileUtil;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatementHistoryService {

	@Autowired
	private VwStatementDraftHistoryRepository statementDraftHistoryRepository;

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private StatementAuthorizationService authorizationService;

	@Autowired
	private ReplacementStringsService replacementStringsService;

	public HistoryModel getHistory(Long statementId)
			throws ForbiddenServiceException, NotFoundServiceException, InternalErrorServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);

		Optional<TblStatement> oStatement = statementRepository.findById(statementId);
		if (!oStatement.isPresent()) {
			throw new NotFoundServiceException("Could not find requested statemend");
		}
		TblStatement statement = oStatement.get();

		HistoryModel model = new HistoryModel();
		model.setStatementId(statementId);
		model.setVersionOrder(new ArrayList<>());
		model.setVersions(new HashMap<>());
		TypeConversion.iso8601InstantStringOfLocalDateTimeUTC(LocalDateTime.now()).ifPresent(model::setTimestamp);
		TblWorkflowdata workflowData = statement.getWorkflowdata();
		if (workflowData != null && workflowData.getTextBlockDefinition() != null) {
			List<VwStatementdraftHistory> statementDrafts = statementDraftHistoryRepository
					.findByStatementIdOrderByVersionAsc(statementId);

			Map<String, String> replacements = replacementStringsService.getAllReplacements(statement);
			TextblockDefinition textblockDef = workflowData.getTextBlockDefinition().getDefinition();

			HistoryEntryModel prevHem = null;
			for (VwStatementdraftHistory statementDraft : statementDrafts) {
				HistoryEntryModel hem = new HistoryEntryModel();
				hem.setFirstName(statementDraft.getUserFirstName());
				hem.setLastName(statementDraft.getUserLastName());
				hem.setMsg(statementDraft.getMsg());
				TypeConversion.iso8601InstantStringOfLocalDateTimeUTC(statementDraft.getTimestamp())
						.ifPresent(hem::setTimestamp);
				hem.setUser(statementDraft.getUsername());
				hem.setVersion(String.valueOf(statementDraft.getVersion()));

				hem.setContent(generateContent(prevHem, statementDraft, replacements, textblockDef));
				model.getVersions().put(hem.getVersion(), hem);
				model.getVersionOrder().add(hem.getVersion());
				model.setCurrentVersion(hem.getVersion());
				prevHem = hem;
			}
		}
		return model;
	}

	private List<HistoryEntryContentModel> generateContent(HistoryEntryModel prevHem,
			VwStatementdraftHistory statementDraft, Map<String, String> replacements, TextblockDefinition textblockDef)
			throws InternalErrorServiceException {
		List<HistoryEntryContentModel> content = new ArrayList<>();
		if (statementDraft != null) {
			Set<String> newTextblockIds = new HashSet<>();

			// add content entries
			for (Textblock textBlock : statementDraft.getDraft()) {
				HistoryEntryContentModel contentEntry = new HistoryEntryContentModel();
				contentEntry.setType(textBlock.getType());
				contentEntry.setId(textBlock.getTextblockId());
				contentEntry.setText(generateText(textBlock, replacements, textblockDef));
				contentEntry.setDiffType(DiffType.NEW);
				setDiffType(contentEntry, prevHem);
				contentEntry.setMoved(MoveType.NOT);
				content.add(contentEntry);
				newTextblockIds.add(contentEntry.getId());
			}

			// check and update movement
			checkAndUpdateMovement(prevHem, content);

			// add deleted entries
			addDeletedEntries(prevHem, content, newTextblockIds);
		}
		return content;
	}

	private void checkAndUpdateMovement(HistoryEntryModel prevHem, List<HistoryEntryContentModel> content) {
		if (prevHem == null) {
			return;
		}
		Map<String, Integer> prevCleanedIdMap = preparePrevCleanedIdMap(prevHem);
		List<HistoryEntryContentModel> newCleaned = content.stream()
				.filter(e -> e.getDiffType() != DiffType.DELETED
						&& (e.getType() == TextblockType.block || e.getType() == TextblockType.text))
				.collect(Collectors.toList());

		for (int i = 0; i < newCleaned.size(); i++) {
			calcAndFillMovement(prevCleanedIdMap, newCleaned, i);
		}
	}

	private void calcAndFillMovement(Map<String, Integer> prevCleanedIdMap, List<HistoryEntryContentModel> newCleaned,
			int i) {
		HistoryEntryContentModel newEntry = newCleaned.get(i);
		Integer prevIndex = prevCleanedIdMap.get(newEntry.getId());
		if (prevIndex == null || prevIndex == i) {
			newEntry.setMoved(MoveType.NOT);
		} else {
			if (prevIndex < i) {
				newEntry.setMoved((prevIndex - i) <= 2 ? MoveType.DOWN : MoveType.LONGDOWN);
			} else {
				newEntry.setMoved((i - prevIndex) <= 2 ? MoveType.UP : MoveType.LONGUP);
			}
		}
	}

	private Map<String, Integer> preparePrevCleanedIdMap(HistoryEntryModel prevHem) {
		List<String> prevCleanedIds = prevHem.getContent().stream()
				.filter(e -> e.getDiffType() != DiffType.DELETED
						&& (e.getType() == TextblockType.block || e.getType() == TextblockType.text))
				.map(HistoryEntryContentModel::getId).collect(Collectors.toList());
		Map<String, Integer> prevCleanedIdMap = new HashMap<>();
		for (int i = 0; i < prevCleanedIds.size(); i++) {
			prevCleanedIdMap.put(prevCleanedIds.get(i), i);
		}
		return prevCleanedIdMap;
	}

	private void addDeletedEntries(HistoryEntryModel prevHem, List<HistoryEntryContentModel> content,
			Set<String> newTextblockIds) {
		if (prevHem != null) {
			for (HistoryEntryContentModel entry : prevHem.getContent()) {
				if (entry.getDiffType() != DiffType.DELETED && !newTextblockIds.contains(entry.getId())) {
					HistoryEntryContentModel deletedEntry = new HistoryEntryContentModel();
					deletedEntry.setId(entry.getId());
					deletedEntry.setDiffType(DiffType.DELETED);
					deletedEntry.setMoved(MoveType.NOT);
					if (DiffType.UNCHANGED == entry.getDiffType()) {
						deletedEntry.setPrevVersion(entry.getPrevVersion());
						deletedEntry.setPrevText(entry.getPrevText());
					} else {
						deletedEntry.setPrevVersion(prevHem.getVersion());
						deletedEntry.setPrevText(entry.getText());
					}
					deletedEntry.setType(entry.getType());
					deletedEntry.setText(null);
					content.add(deletedEntry);
				}
			}
		}
	}

	private void setDiffType(HistoryEntryContentModel currentEntry, HistoryEntryModel prevHem) {
		if (prevHem == null) {
			return;
		}
		Map<String, HistoryEntryContentModel> notDeletedPrevEntries = prevHem.getContent().stream()
				.filter(x -> DiffType.DELETED != x.getDiffType() && x.getId() != null)
				.collect(Collectors.toMap(HistoryEntryContentModel::getId, m -> m));
		HistoryEntryContentModel matchingPrev = notDeletedPrevEntries.get(currentEntry.getId());
		if (matchingPrev == null) {
			currentEntry.setDiffType(DiffType.NEW);
		} else if (sameTexts(currentEntry, matchingPrev)) {
			currentEntry.setDiffType(DiffType.UNCHANGED);
			if (DiffType.UNCHANGED == matchingPrev.getDiffType()) {
				currentEntry.setPrevVersion(matchingPrev.getPrevVersion());
				currentEntry.setPrevText(matchingPrev.getPrevText());
			} else {
				currentEntry.setPrevVersion(prevHem.getVersion());
				currentEntry.setPrevText(matchingPrev.getText());
			}
		} else {
			currentEntry.setDiffType(DiffType.CHANGED);
			if (DiffType.UNCHANGED.equals(matchingPrev.getDiffType())) {
				currentEntry.setPrevVersion(matchingPrev.getPrevVersion());
				currentEntry.setPrevText(matchingPrev.getPrevText());
			} else {
				currentEntry.setPrevVersion(prevHem.getVersion());
				currentEntry.setPrevText(matchingPrev.getText());
			}
		}
	}

	private boolean sameTexts(HistoryEntryContentModel currentEntry, HistoryEntryContentModel matchingPrev) {
		if (currentEntry.getText() == null) {
			return matchingPrev.getText() == null;
		}
		return currentEntry.getText().equals(matchingPrev.getText());
	}

	private String generateText(Textblock block, Map<String, String> replacements, TextblockDefinition textblockDef)
			throws InternalErrorServiceException {
		String text;
		switch (block.getType()) {
		case text:
			text = TextCompileUtil.fillPlaceholder(block.getReplacement(), block.getPlaceholderValues(), replacements,
					textblockDef.getSelects());
			break;
		case block:
			if (block.getReplacement() == null) {
				text = TextCompileUtil.convertToText(block, textblockDef, replacements);
			} else {
				text = TextCompileUtil.fillPlaceholder(block.getReplacement(), block.getPlaceholderValues(),
						replacements, textblockDef.getSelects());
			}
			break;
		case newline:
		case pagebreak:
		default:
			text = null;
			break;
		}
		return text;
	}

}
