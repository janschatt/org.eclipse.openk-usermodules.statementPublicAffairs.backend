/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.persistence.PersistenceException;

import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.SearchParams;
import org.eclipse.openk.statementpublicaffairs.model.conf.AuthorizationRuleActions;
import org.eclipse.openk.statementpublicaffairs.model.conf.Rule;
import org.eclipse.openk.statementpublicaffairs.model.db.TblReqdepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatementtype;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementPositionSearch;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementSearch;
import org.eclipse.openk.statementpublicaffairs.model.db.VwUserStatementSearch;
import org.eclipse.openk.statementpublicaffairs.repository.PagedAndSortedStatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.PagedAndSortedUserStatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.ReqDepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementEditLogRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementtypeRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementPositionRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementPositionSearchSpecification;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementSearchSpecification;
import org.eclipse.openk.statementpublicaffairs.repository.VwUserStatementSearchSpecification;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DashboardStatement;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementPosition;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementTypeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

/**
 * StatementOverviewService uses the StatementRepository to access the current
 * statements.
 * 
 * @author Tobias Stummer
 *
 */
@Service
public class StatementOverviewService {

	private static final String EXCEPTION_ACCESSING_STATEMENT_REPOSITORY = "Exception occurred when accessing the statement repository.";

	private static final String INVALID_PARAMETERS_BROWSING = "Invalid parameters when browsing statement repository for all statements.";

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private PagedAndSortedStatementRepository pagedStatementRepository;

	@Autowired
	private PagedAndSortedUserStatementRepository pagedUserStatementRepository;

	@Autowired
	private StatementtypeRepository statementtypeRepository;

	@Autowired
	private StatementEditLogRepository statementEditLogRepository;

	@Autowired
	private ReqDepartmentRepository reqDepartmentRepository;

	@Autowired
	private VwStatementPositionRepository statementPositionRepository;

	@Autowired
	private StatementAuthorizationService authorizationService;

	@Autowired
	private StatementProcessService statementProcessService;

	@Autowired
	private StatementService statementService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private UserInfoService userInfoService;

	/**
	 * Get all statements from the database.
	 * 
	 * @return List of StatementModel containing the statement overview data of each
	 *         statement in the database.
	 * @throws ForbiddenServiceException
	 */
	public List<StatementDetailsModel> getAllStatementModels()
			throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		List<StatementDetailsModel> statements = new ArrayList<>();
		try {
			Iterable<TblStatement> results = statementRepository.findAll();
			statements.addAll(tblStatementsToStatementModels(results));
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException(INVALID_PARAMETERS_BROWSING, e);
		} catch (PersistenceException e) {
			throw new InternalErrorServiceException(EXCEPTION_ACCESSING_STATEMENT_REPOSITORY, e);
		}
		return statements;
	}

	private List<StatementDetailsModel> tblStatementsToStatementModels(Iterable<TblStatement> tblStatements) {
		List<StatementDetailsModel> statements = new ArrayList<>();
		for (TblStatement statement : tblStatements) {
			StatementDetailsModel model = new StatementDetailsModel();
			model.setFinished(statement.getFinished());
			model.setId(statement.getId());
			if (statement.getBusinessKey() != null) {
				model.setBusinessKey(statement.getBusinessKey());
			}
			TypeConversion.dateStringOfLocalDate(statement.getDueDate()).ifPresent(model::setDueDate);
			TypeConversion.dateStringOfLocalDate(statement.getDepartmentsDueDate())
					.ifPresent(model::setDepartmentsDueDate);
			TypeConversion.dateStringOfLocalDate(statement.getReceiptDate()).ifPresent(model::setReceiptDate);
			TypeConversion.dateStringOfLocalDate(statement.getCreationDate()).ifPresent(model::setCreationDate);
			TypeConversion.dateStringOfLocalDate(statement.getFinishedDate()).ifPresent(model::setFinishedDate);
			model.setTitle(statement.getTitle());
			model.setDistrict(statement.getDistrict());
			model.setCity(statement.getCity());
			model.setTypeId(statement.getType().getId());
			model.setContactId(statement.getContactDbId());
			model.setCustomerReference(statement.getCustomerReference());
			model.setCanceled(statement.getCanceled());

			statements.add(model);
		}
		return statements;
	}

	/**
	 * Get all statementtypes from the database.
	 * 
	 * @return List of StatementTypeModels
	 * @throws ForbiddenServiceException
	 */
	public List<StatementTypeModel> getStatementTypeModels()
			throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_BASE_DATA);
		List<StatementTypeModel> statementTypes = new ArrayList<>();
		try {
			for (TblStatementtype sType : statementtypeRepository.findAll()) {
				StatementTypeModel model = new StatementTypeModel();
				model.setId(sType.getId());
				model.setName(sType.getName());
				model.setDisabled(sType.getDisabled());
				statementTypes.add(model);
			}
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException(
					"Invalid parameters when browsing statement repository statement types.", e);
		} catch (PersistenceException e) {
			throw new InternalErrorServiceException("Exception occurred when accessing the statement repositor.", e);
		}
		return statementTypes;
	}

	protected String postgresRegexSearch(String[] searchStrings) {
		return "%(" + String.join("|", searchStrings) + ")%";
	}

	public Page<StatementDetailsModel> searchStatementModels(SearchParams params, Pageable pPageable)
			throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);

		VwStatementSearchSpecification spec = new VwStatementSearchSpecification(params);

		Page<VwStatementSearch> tStatements = pagedStatementRepository.findAll(spec, pPageable);
		Pageable pageable = tStatements.getPageable();
		List<StatementDetailsModel> statements = new ArrayList<>();
		for (VwStatementSearch statement : tStatements) {
			StatementDetailsModel model = new StatementDetailsModel();
			model.setFinished(statement.getFinished());
			model.setId(statement.getId());
			if (statement.getBusinessKey() != null) {
				model.setBusinessKey(statement.getBusinessKey());
			}
			TypeConversion.dateStringOfLocalDate(statement.getDueDate()).ifPresent(model::setDueDate);
			TypeConversion.dateStringOfLocalDate(statement.getDepartmentsDueDate())
					.ifPresent(model::setDepartmentsDueDate);
			TypeConversion.dateStringOfLocalDate(statement.getReceiptDate()).ifPresent(model::setReceiptDate);
			TypeConversion.dateStringOfLocalDate(statement.getCreationDate()).ifPresent(model::setCreationDate);
			model.setTitle(statement.getTitle());
			model.setDistrict(statement.getDistrict());
			model.setCity(statement.getCity());
			model.setContactId(statement.getContactDbId());
			model.setTypeId(statement.getTypeId());
			model.setCustomerReference(statement.getCustomerReference());
			model.setSourceMailId(statement.getSourceMailId());
			model.setCanceled(statement.getCanceled());
			statements.add(model);
		}
		return new PageImpl<>(statements, pageable, tStatements.getTotalElements());
	}

	public Page<StatementDetailsModel> searchStatementModelsEditedByMe(SearchParams params, Pageable pPageable)
			throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);

		List<StatementDetailsModel> statements = new ArrayList<>();
		Optional<Long> oUserId = usersService.getUserId(userInfoService.getUserName());
		if (!oUserId.isPresent()) {
			return new PageImpl<>(statements, pPageable, 0);
		}

		VwUserStatementSearchSpecification spec = new VwUserStatementSearchSpecification(params, oUserId.get());

		Page<VwUserStatementSearch> tStatements = pagedUserStatementRepository.findAll(spec, pPageable);
		Pageable pageable = tStatements.getPageable();
		for (VwUserStatementSearch statement : tStatements) {
			StatementDetailsModel model = new StatementDetailsModel();
			model.setId(statement.getId());
			model.setFinished(statement.getFinished());
			TypeConversion.dateStringOfLocalDate(statement.getCreationDate()).ifPresent(model::setCreationDate);
			TypeConversion.dateStringOfLocalDate(statement.getReceiptDate()).ifPresent(model::setReceiptDate);
			TypeConversion.dateStringOfLocalDate(statement.getDepartmentsDueDate())
					.ifPresent(model::setDepartmentsDueDate);
			TypeConversion.dateStringOfLocalDate(statement.getDueDate()).ifPresent(model::setDueDate);
			if (statement.getBusinessKey() != null) {
				model.setBusinessKey(statement.getBusinessKey());
			}
			model.setTypeId(statement.getTypeId());
			model.setContactId(statement.getContactDbId());
			model.setCustomerReference(statement.getCustomerReference());
			model.setTitle(statement.getTitle());
			model.setCity(statement.getCity());
			model.setSourceMailId(statement.getSourceMailId());
			model.setDistrict(statement.getDistrict());
			model.setCanceled(statement.getCanceled());
			statements.add(model);
		}
		return new PageImpl<>(statements, pageable, tStatements.getTotalElements());
	}

	public List<StatementDetailsModel> getStatementModelsByInIds(List<Long> statementIds)
			throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		List<StatementDetailsModel> statements = new ArrayList<>();
		try {
			List<TblStatement> results;
			results = statementRepository.findByIdIn(statementIds);
			statements.addAll(tblStatementsToStatementModels(results));
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException(INVALID_PARAMETERS_BROWSING, e);
		} catch (PersistenceException e) {
			throw new InternalErrorServiceException(EXCEPTION_ACCESSING_STATEMENT_REPOSITORY, e);
		}
		return statements;
	}

	public List<DashboardStatement> getDashboardStatements()
			throws InternalErrorServiceException, ForbiddenServiceException {

		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_DASHBOARD_STATEMENTS);
		List<DashboardStatement> statements = new ArrayList<>();
		List<StatementDetailsModel> currentStatements = tblStatementsToStatementModels(
				statementRepository.findByFinishedAndCanceled(false, false));
		Map<Long, Boolean> ownDepartmentsStandIns = usersService.ownDepartmentsStandIns();
		String username = userInfoService.getUserName();
		for (StatementDetailsModel statement : currentStatements) {
			if (Boolean.TRUE.equals(statement.getCanceled())) {
				continue;
			}
			DashboardStatement dstatement = new DashboardStatement();
			dstatement.setMandatoryContributionsCount(0);
			dstatement.setMandatoryDepartmentsCount(0);
			dstatement.setInfo(statement);
			dstatement.setTasks(statementProcessService.getCurrentStatementTasks(statement.getId())
					.orElse(Collections.emptyList()));

			dstatement.setEditedByMe(
					!statementEditLogRepository.findByStatementIdAndUsername(statement.getId(), username).isEmpty());

			List<TblReqdepartment> reqDepartments = reqDepartmentRepository.findByStatementId(statement.getId());

			for (TblReqdepartment rdp : reqDepartments) {
				fillDashboardStatementContributions(dstatement, ownDepartmentsStandIns, rdp);
			}
			statements.add(dstatement);
		}
		return statements;
	}

	private void fillDashboardStatementContributions(DashboardStatement dstatement,
			Map<Long, Boolean> ownDepartmentsStandIns, TblReqdepartment rdp) {
		if (ownDepartmentsStandIns.containsKey(rdp.getDepartment().getId())) {

			dsContributionsSetCompletedForMyDepartment(dstatement, rdp);

			dsContributionsSetOptionalForMyDepartment(dstatement, rdp);

			dsContributionsSetDepartmentStandIn(dstatement, ownDepartmentsStandIns, rdp);
		}

		if (!Boolean.TRUE.equals(rdp.getOptional())) {
			dstatement.setMandatoryDepartmentsCount(dstatement.getMandatoryDepartmentsCount() + 1);
			if (Boolean.TRUE.equals(rdp.getContributed())) {
				dstatement.setMandatoryContributionsCount(dstatement.getMandatoryContributionsCount() + 1);
			}
		}
	}

	private void dsContributionsSetDepartmentStandIn(DashboardStatement dstatement,
			Map<Long, Boolean> ownDepartmentsStandIns, TblReqdepartment rdp) {
		if (dstatement.getDepartmentStandIn() == null) {
			dstatement.setDepartmentStandIn(ownDepartmentsStandIns.get(rdp.getDepartment().getId()));
		} else {
			dstatement.setDepartmentStandIn(
					dstatement.getDepartmentStandIn() && ownDepartmentsStandIns.get(rdp.getDepartment().getId()));
		}
	}

	private void dsContributionsSetOptionalForMyDepartment(DashboardStatement dstatement, TblReqdepartment rdp) {
		if (dstatement.getOptionalForMyDepartment() == null) {
			dstatement.setOptionalForMyDepartment(rdp.getOptional() != null && rdp.getOptional());
		} else {
			dstatement.setOptionalForMyDepartment(dstatement.getOptionalForMyDepartment() && rdp.getOptional());
		}
	}

	private void dsContributionsSetCompletedForMyDepartment(DashboardStatement dstatement, TblReqdepartment rdp) {
		if (dstatement.getCompletedForMyDepartment() == null) {
			dstatement.setCompletedForMyDepartment(rdp.getContributed() != null && rdp.getContributed());
		} else {
			dstatement.setCompletedForMyDepartment(dstatement.getCompletedForMyDepartment() && rdp.getContributed());
		}
	}

	public List<StatementPosition> searchStatementPositions(SearchParams params) throws ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_READ_STATEMENT);
		VwStatementPositionSearchSpecification spec = new VwStatementPositionSearchSpecification(params);
		List<VwStatementPositionSearch> results = statementPositionRepository.findAll(spec);
		List<StatementPosition> statementPositions = new ArrayList<>();
		for (VwStatementPositionSearch res : results) {
			StatementPosition pos = new StatementPosition();
			pos.setId(res.getId());
			TypeConversion.dateStringOfLocalDate(res.getDueDate()).ifPresent(pos::setDueDate);
			pos.setFinished(res.getFinished());
			pos.setPosition(res.getPosition());
			pos.setTitle(res.getTitle());
			pos.setTypeId(res.getTypeId());
			pos.setCanceled(res.getCanceled());
			statementPositions.add(pos);
		}
		return statementPositions;
	}

	public void addStatementType(String label) throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_CREATE_TAG);
		try {
			if (!statementtypeRepository.findByName(label).isPresent()) {
				TblStatementtype statementType = new TblStatementtype();
				statementType.setName(label);
				statementType.setStatements(new ArrayList<>());
				statementType.setDisabled(false);
				statementtypeRepository.save(statementType);
			}
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException("Could not disable statement-type", e);
		}
		statementService.cleanupDeletableStatementTypes();
	}

	public void disableStatementType(String label) throws InternalErrorServiceException, ForbiddenServiceException {
		authorizationService.authorize(Rule.ANY, AuthorizationRuleActions.A_CREATE_TAG);
		try {
			Optional<TblStatementtype> oStatementType = statementtypeRepository.findByName(label);
			if (oStatementType.isPresent()) {
				TblStatementtype statementType = oStatementType.get();
				statementType.setDisabled(true);
				statementtypeRepository.save(statementType);
			}
		} catch (IllegalArgumentException e) {
			throw new InternalErrorServiceException("Could not disable statement-type", e);
		}
		statementService.cleanupDeletableStatementTypes();
	}

}
