/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.UserModel;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartmentstructure;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.repository.DepartmentstructureRepository;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ProcessActivityHistoryViewModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ProcessHistoryModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.java.Log;

/**
 * ReplacementStringsService generates a map of replacement strings for
 * placeholder values. The service primary focus lies on statement specific
 * replacement values that can be used in generated text output for notification
 * messages and pdf generates.
 * 
 * @author Tobias Stummer
 *
 */
@Log
@Service
public class ReplacementStringsService {

	public static final String REPLACEMENT_KEY_CONTACT_C_GREETING = "c-greeting";

	public static final String REPLACEMENT_KEY_CONTACT_C_TITLE = "c-title";

	public static final String REPLACEMENT_KEY_CONTACT_C_STREET = "c-street";

	public static final String REPLACEMENT_KEY_CONTACT_C_SALUTATION = "c-salutation";

	public static final String REPLACEMENT_KEY_CONTACT_C_POST_CODE = "c-postCode";

	public static final String REPLACEMENT_KEY_CONTACT_C_LAST_NAME = "c-lastName";

	public static final String REPLACEMENT_KEY_CONTACT_C_HOUSE_NUMBER = "c-houseNumber";

	public static final String REPLACEMENT_KEY_CONTACT_C_FIRST_NAME = "c-firstName";

	public static final String REPLACEMENT_KEY_CONTACT_C_EMAIL = "c-email";

	public static final String REPLACEMENT_KEY_CONTACT_C_COMPANY = "c-company";

	public static final String REPLACEMENT_KEY_CONTACT_C_COMMUNITY_SUFFIX = "c-communitySuffix";

	public static final String REPLACEMENT_KEY_CONTACT_C_COMMUNITY = "c-community";

	public static final String REPLACEMENT_KEY_CURRENT_DATE = "b-currentDate";

	public static final String REPLACEMENT_KEY_STATEMENT_SACHBEARBEITER = "sachbearbeiter";
	
	public static final String REPLACEMENT_KEY_STATEMENT_OIC_NAME = "oic-name";

	public static final String REPLACEMENT_KEY_STATEMENT_OIC_EMAIL = "oic-email";

	public static final String REPLACEMENT_KEY_STATEMENT_OIC_PHONE = "oic-phone";

	public static final String REPLACEMENT_KEY_STATEMENT_OIC_FAX = "oic-fax";

	public static final String REPLACEMENT_KEY_STATEMENT_OIC_INITIALS = "oic-initials";

	public static final String REPLACEMENT_KEY_STATEMENT_SECTORS = "sectors";

	public static final String REPLACEMENT_KEY_STATEMENT_LOCATION_DESIGNATION = "locationDesignation";

	public static final String REPLACEMENT_KEY_STATEMENT_CITY_WITH_LOCATION_DESIGNATION = "cityWithLocationDesignation";

	public static final String REPLACEMENT_KEY_STATEMENT_TYPE = "type";

	public static final String REPLACEMENT_KEY_STATEMENT_DISTRICT = "district";

	public static final String REPLACEMENT_KEY_STATEMENT_CITY = "city";

	public static final String REPLACEMENT_KEY_STATEMENT_CUSTOMER_REFERENCE = "customerReference";

	public static final String REPLACEMENT_KEY_STATEMENT_CREATION_DATE = "creationDate";

	public static final String REPLACEMENT_KEY_STATEMENT_RECEIPT_DATE = "receiptDate";

	public static final String REPLACEMENT_KEY_STATEMENT_DUE_DATE = "dueDate";

	public static final String REPLACEMENT_KEY_STATEMENT_DEPARTMENTS_DUE_DATE = "departmentsDueDate";

	public static final String REPLACEMENT_KEY_STATEMENT_TITLE = "title";

	public static final String REPLACEMENT_KEY_STATEMENT_ID = "id";

	@Value("${statement.text.replacementStringsJson}")
	private String specialReplacementsConfig;

	@Value("${statement.compile.dateFormatPattern:\"dd.MM.yyyy\"}")
	private String dateFormatPattern;

	private Map<String, String> staticReplacements = new HashMap<>();

	@Autowired
	private StatementService statementService;

	@Autowired
	private StatementProcessService statementProcessService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private ContactService contactService;

	@Autowired
	private DepartmentstructureRepository departmentStructureRepository;

	@PostConstruct
	public void init() {
		importReplacementsConfig();
	}

	@SuppressWarnings("unchecked")
	private void importReplacementsConfig() {
		try {
			File file = ResourceUtils.getFile(specialReplacementsConfig);
			ObjectMapper objectMapper = new ObjectMapper();
			Map<String, String> replacementConfigValues = objectMapper.readValue(file, Map.class);
			if (replacementConfigValues != null) {
				staticReplacements.putAll(replacementConfigValues);
			}
		} catch (IOException e) {
			log.warning("Could not load text replacements from file " + specialReplacementsConfig);
		}
	}

	public Map<String, String> getAllReplacements(TblStatement statement) {
		Map<String, String> replacements = new HashMap<>();

		addStatementReplacements(replacements, statement);
		addWorkflowReplacements(replacements, statement);
		addSectorsReplacements(replacements, statement);
		addContactReplacements(replacements, statement);

		Optional<String> currentDate = TypeConversion.dateStringOfLocalDate(LocalDate.now(), dateFormatPattern);
		if (currentDate.isPresent()) {
			putReplacement(replacements, REPLACEMENT_KEY_CURRENT_DATE, currentDate.get(), false);
		}
		return replacements;

	}

	private Map<String, String> addContactReplacements(Map<String, String> replacements, TblStatement statement) {
		try {
			contactService.getContactDetails(statement.getContactDbId(), true).ifPresent(contact -> {
				putReplacement(replacements, REPLACEMENT_KEY_CONTACT_C_COMMUNITY, contact.getCommunity(), false);
				putReplacement(replacements, REPLACEMENT_KEY_CONTACT_C_COMMUNITY_SUFFIX, contact.getCommunitySuffix(),
						true);
				putReplacement(replacements, REPLACEMENT_KEY_CONTACT_C_COMPANY, contact.getCompany(), false);
				putReplacement(replacements, REPLACEMENT_KEY_CONTACT_C_EMAIL, contact.getEmail(), false);
				putReplacement(replacements, REPLACEMENT_KEY_CONTACT_C_FIRST_NAME, contact.getFirstName(), true);
				putReplacement(replacements, REPLACEMENT_KEY_CONTACT_C_HOUSE_NUMBER, contact.getHouseNumber(), false);
				putReplacement(replacements, REPLACEMENT_KEY_CONTACT_C_LAST_NAME, contact.getLastName(), false);
				putReplacement(replacements, REPLACEMENT_KEY_CONTACT_C_POST_CODE, contact.getPostCode(), false);
				putReplacement(replacements, REPLACEMENT_KEY_CONTACT_C_SALUTATION, contact.getSalutation(), true);
				putReplacement(replacements, REPLACEMENT_KEY_CONTACT_C_STREET, contact.getStreet(), false);
				putReplacement(replacements, REPLACEMENT_KEY_CONTACT_C_TITLE, contact.getTitle(), true);
				putReplacement(replacements, REPLACEMENT_KEY_CONTACT_C_GREETING, getGreeting(contact.getSalutation()),
						false);
			});
		} catch (ForbiddenServiceException | InternalErrorServiceException e) {
			log.warning("Error looking up contact replacements - " + e.getMessage() + " - " + e);
		}
		return replacements;
	}

	
	private Optional<UserModel> getLastoicUserForStatement(TblStatement statement) throws InternalErrorServiceException {
		Optional<ProcessHistoryModel> oHistory = statementProcessService.getStatementProcessHistoryInternal(statement.getId());
		if (oHistory.isPresent()) {
			ProcessHistoryModel history = oHistory.get();
			List<String> processUser = new ArrayList<>();
			for (ProcessActivityHistoryViewModel activity : history.getCurrentProcessActivities()) {
				if (activity.getAssignee() != null) {
					processUser.add(activity.getAssignee());
				}
			}
			for (ProcessActivityHistoryViewModel activity : history.getFinishedProcessActivities()) {
				if (activity.getAssignee() != null) {
					processUser.add(activity.getAssignee());
				}
			}
			Collections.reverse(processUser);
			return userModelOfFirstOICUserInUserIdList(processUser);
		}
		return Optional.empty();
		
	}
	
	private String getOICName(Optional<UserModel> oUserModel) {
		if (oUserModel.isPresent()) {
			UserModel um = oUserModel.get();
			return um.getFirstName() + " " + um.getLastName();
		}
		return staticReplacements.getOrDefault("oic-fallback-name", "Vorname Nachname");
	}

	private Optional<UserModel> userModelOfFirstOICUserInUserIdList(List<String> processUser) throws InternalErrorServiceException {
		List<UserModel> oicUsers = usersService.getUsersWithRole(UserRoles.SPA_OFFICIAL_IN_CHARGE);
		Map<String, UserModel> oicUserMap = new HashMap<>();
		for (UserModel user : oicUsers) {
			oicUserMap.put(user.getUsername(), user);
		}
		UserModel userModel = null;
		for (String user : processUser) {
			if (oicUserMap.containsKey(user)) {
				// found last OIC User
				userModel = oicUserMap.get(user);
				break;
			}
		}
		return Optional.ofNullable(userModel);
	}

	private String getGreeting(String salutation) {
		String searchSalutation = salutation;
		if (searchSalutation == null) {
			searchSalutation = "fallback";
		}
		return staticReplacements.getOrDefault("salutation-" + searchSalutation,
				staticReplacements.getOrDefault("salutation-fallback", null));
	}

	private Map<String, String> addWorkflowReplacements(Map<String, String> replacements, TblStatement statement) {
		try {
			Optional<UserModel> oUserModel = getLastoicUserForStatement(statement);
			String oicName = getOICName(oUserModel);
			replacements.put(REPLACEMENT_KEY_STATEMENT_SACHBEARBEITER, oicName);
			replacements.put(REPLACEMENT_KEY_STATEMENT_OIC_NAME, oicName);
			if (oUserModel.isPresent()) {
				UserModel userModel = oUserModel.get();
				replacements.put(REPLACEMENT_KEY_STATEMENT_OIC_EMAIL, userModel.getEmailAddress() == null ? "" : userModel.getEmailAddress());
				replacements.put(REPLACEMENT_KEY_STATEMENT_OIC_PHONE, userModel.getPhone() == null ? "" : userModel.getPhone());
				replacements.put(REPLACEMENT_KEY_STATEMENT_OIC_FAX, userModel.getFax() == null ? "" : userModel.getFax());
				replacements.put(REPLACEMENT_KEY_STATEMENT_OIC_INITIALS, userModel.getInitials() == null ? "" : userModel.getInitials());
			} else {
				replacements.put(REPLACEMENT_KEY_STATEMENT_OIC_EMAIL, "");
				replacements.put(REPLACEMENT_KEY_STATEMENT_OIC_PHONE, "");
				replacements.put(REPLACEMENT_KEY_STATEMENT_OIC_FAX, ""); 
				replacements.put(REPLACEMENT_KEY_STATEMENT_OIC_INITIALS, "");
			}
		} catch (InternalErrorServiceException e) {
			log.warning("Error looking up contact replacements - " + e.getMessage() + " - " + e);
		}
		return replacements;
	}

	private Map<String, String> addSectorsReplacements(Map<String, String> replacements, TblStatement statement) {
		try {
			Map<String, List<String>> sectors = statementService.getAllSectorsInternal(statement.getId());
			String sectorkey = statement.getCity() + "#" + statement.getDistrict();
			if (sectors.containsKey(sectorkey)) {
				List<String> sectorStrings = sectors.get(sectorkey);
				if (!sectorStrings.isEmpty()) {
					String sectorsReplacementValue = "";
					if (sectorStrings.size() == 1) {
						sectorsReplacementValue += sectorStrings.get(0);
					} else {
						String listElementJoin = staticReplacements.getOrDefault("list-element-join", ", ");
						String listElementJoinLast = staticReplacements.getOrDefault("list-element-join-last", " and ");
						sectorsReplacementValue = String.join(listElementJoin,
								sectorStrings.subList(0, sectorStrings.size() - 1));
						sectorsReplacementValue = sectorsReplacementValue + listElementJoinLast
								+ sectorStrings.get(sectorStrings.size() - 1);
					}
					replacements.put(REPLACEMENT_KEY_STATEMENT_SECTORS, sectorsReplacementValue);
				}
			}
		} catch (NotFoundServiceException e) {
			log.warning("Error looking up sector replacements - " + e.getMessage() + " - " + e);
		}
		return replacements;
	}

	private Map<String, String> addStatementReplacements(Map<String, String> replacements, TblStatement statement) {
		replacements.put(REPLACEMENT_KEY_STATEMENT_ID, statement.getId().toString());
		replacements.put(REPLACEMENT_KEY_STATEMENT_TITLE, statement.getTitle());
		Optional<String> dueDate = TypeConversion.dateStringOfLocalDate(statement.getDueDate(), dateFormatPattern);
		if (dueDate.isPresent()) {
			replacements.put(REPLACEMENT_KEY_STATEMENT_DUE_DATE, dueDate.get());
		}

		Optional<String> receiptDate = TypeConversion.dateStringOfLocalDate(statement.getReceiptDate(),
				dateFormatPattern);
		if (receiptDate.isPresent()) {
			replacements.put(REPLACEMENT_KEY_STATEMENT_RECEIPT_DATE, receiptDate.get());
		}

		Optional<String> creationDate = TypeConversion.dateStringOfLocalDate(statement.getCreationDate(),
				dateFormatPattern);
		if (creationDate.isPresent()) {
			replacements.put(REPLACEMENT_KEY_STATEMENT_CREATION_DATE, creationDate.get());
		}

		Optional<String> departmentsDueDate = TypeConversion.dateStringOfLocalDate(statement.getDepartmentsDueDate(),
				dateFormatPattern);
		if (departmentsDueDate.isPresent()) {
			replacements.put(REPLACEMENT_KEY_STATEMENT_DEPARTMENTS_DUE_DATE, departmentsDueDate.get());
		}

		replacements.put(REPLACEMENT_KEY_STATEMENT_CUSTOMER_REFERENCE,
				statement.getCustomerReference() == null ? "" : statement.getCustomerReference());

		replacements.put(REPLACEMENT_KEY_STATEMENT_CITY, statement.getCity());
		replacements.put(REPLACEMENT_KEY_STATEMENT_DISTRICT, statement.getDistrict());
		replacements.put(REPLACEMENT_KEY_STATEMENT_TYPE, statement.getType().getName());

		String sectorkey = statement.getCity() + "#" + statement.getDistrict();

		Optional<TblDepartmentstructure> oDepartmentStructure = departmentStructureRepository
				.findById(statement.getDepartmentStructureId());
		if (oDepartmentStructure.isPresent()) {
			TblDepartmentstructure departmentStructure = oDepartmentStructure.get();
			Optional.ofNullable(departmentStructure.getDefinition().getOrDefault(sectorkey, null))
					.ifPresent(departmentsModel -> {
						String locationDesignationString = departmentsModel.getLocationDesignation();
						if (locationDesignationString == null) {
							replacements.put(REPLACEMENT_KEY_STATEMENT_CITY_WITH_LOCATION_DESIGNATION,
									statement.getCity());
							replacements.put(REPLACEMENT_KEY_STATEMENT_LOCATION_DESIGNATION, "");
						} else {
							replacements.put(REPLACEMENT_KEY_STATEMENT_CITY_WITH_LOCATION_DESIGNATION,
									locationDesignationString + " " + statement.getCity());
							replacements.put(REPLACEMENT_KEY_STATEMENT_LOCATION_DESIGNATION, locationDesignationString);
						}
					});
		}

		return replacements;
	}

	protected void putReplacement(Map<String, String> replacements, String key, String value, boolean setBlankIfNull) {

		if (value == null) {
			if (setBlankIfNull) {
				replacements.put(key, "");
			}
		} else {
			replacements.put(key, value.trim());
		}
	}

}
