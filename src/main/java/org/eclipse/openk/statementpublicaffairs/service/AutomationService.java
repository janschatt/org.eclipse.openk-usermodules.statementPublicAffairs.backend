/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.WorkflowTaskModel;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.extern.java.Log;

@Service
@Log
public class AutomationService {

	private static final String COULD_NOT_UNCLAIM_STATEMENT_WORKFLOW = "Could not unclaim statement workflow ";

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private WorkflowService workflowService;

	@Autowired
	private NotifyService notificationService;

	@Value("${statement.claim.autoUnclaimAllStatementsEnable}")
	private boolean autoUnclaimAllStatementsEnable;

	@Value("${statement.claim.unclaimTimedoutStatementEnable}")
	private boolean unclaimTimedoutStatementEnable;

	@Value("${statement.claim.unclaimTimeoutMinutes}")
	private int unclaimTimeoutMinutes;

	@Value("${statement.notify.enhanceDueDateEnable}")
	private boolean notifyEnhanceDueDateEnable;

	@Value("${statement.notify.enhanceDueDateBeforeDays}")
	private int notifyEnhanceDueDateBeforeDays;

	public AutomationService() {

		log.info("Init AutoUnClaim");
	}

	/**
	 * Unclaims all statements at scheduled time.
	 */
	@Scheduled(cron = "${statement.claim.autoUnclaimAllStatementsCron}")
	public void autoUnClaim() {
		if (!autoUnclaimAllStatementsEnable) {
			return;
		}
		log.finest("Run AutoUnClaim");
		for (TblStatement statement : statementRepository.findByFinishedAndCanceled(false, false)) {
			try {
				Optional<List<WorkflowTaskModel>> workflowTaskModels = workflowService
						.getCurrentTasksOfProcessBusinessKey(statement.getBusinessKey());

				if (workflowTaskModels.isPresent()) {

					for (WorkflowTaskModel t : workflowTaskModels.get()) {
						if (t.isAssigned()) {
							workflowService.unClaimTask(t.getTaskId());
						}

					}
				}
			} catch (InternalErrorServiceException e) {
				log.warning(COULD_NOT_UNCLAIM_STATEMENT_WORKFLOW + e.getMessage() + e);
			}
		}
	}

	/**
	 * Unclaims all statements at scheduled time with a claim timestamp older than
	 * configured time delta.
	 */
	@Scheduled(cron = "${statement.claim.unclaimTimedoutStatementCron}")
	public void unclaimTimedoutStatement() {
		if (!unclaimTimedoutStatementEnable) {
			return;
		}
		log.finest("Run unclaimTimedoutStatement");

		ZonedDateTime afterTimeStamp = ZonedDateTime.now().minusMinutes(unclaimTimeoutMinutes);

		for (TblStatement statement : statementRepository.findByFinishedAndCanceled(false, false)) {
			try {
				Optional<List<WorkflowTaskModel>> workflowTaskModels = workflowService
						.getCurrentTasksOfProcessBusinessKey(statement.getBusinessKey());

				if (workflowTaskModels.isPresent()) {
					for (WorkflowTaskModel t : workflowTaskModels.get()) {
						if (t.isAssigned()
								&& !workflowService.hasUserOperationAfterTimestamp(t.getTaskId(), afterTimeStamp)) {
							workflowService.unClaimTask(t.getTaskId());
							log.info("Unclaim timedout task " + t.getTaskId());
						}
					}
				}
			} catch (InternalErrorServiceException e) {
				log.warning(COULD_NOT_UNCLAIM_STATEMENT_WORKFLOW + e.getMessage() + e);
			}
		}
	}

	/**
	 * Checks unfinished Workflows that are overdue in task "enrichDraft" and
	 * initiates a Due for Department Contribution Notification to the Department
	 * Members.
	 */
	@Scheduled(cron = "${statement.notify.enhanceDueDateCron}")
	public void notifyDepartmentsLackingContribution() {
		if (!notifyEnhanceDueDateEnable) {
			return;
		}
		log.finest("Run NotifyDepartmentsLackingContribution");

		for (TblStatement statement : statementRepository.findByFinishedAndCanceled(false, false)) {
			LocalDate today = LocalDate.now();
			if (statement.getDepartmentsDueDate().minusDays(notifyEnhanceDueDateBeforeDays + 1L).isBefore(today)) {
				notifyNotContributedDepartmentsWhenInEnrichWFTask(statement);
			}
		}
	}

	private void notifyNotContributedDepartmentsWhenInEnrichWFTask(TblStatement statement) {
		try {
			Optional<List<WorkflowTaskModel>> workflowTaskModels = workflowService
					.getCurrentTasksOfProcessBusinessKey(statement.getBusinessKey());

			if (workflowTaskModels.isPresent()) {
				for (WorkflowTaskModel t : workflowTaskModels.get()) {
					if ("enrichDraft".equals(t.getTaskDefinitionKey())) {
						notificationService.notifyDepartmentsDueDate(statement.getId());
					}
				}
			}
		} catch (InternalErrorServiceException e) {
			log.warning(COULD_NOT_UNCLAIM_STATEMENT_WORKFLOW + e.getMessage() + e);
		}
	}

}
