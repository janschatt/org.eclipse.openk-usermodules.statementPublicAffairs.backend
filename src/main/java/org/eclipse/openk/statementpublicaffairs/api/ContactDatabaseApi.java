/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.api;

import java.util.List;

import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiAddressModel;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiCompanyContactPersonModel;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiCompanyDetailsModel;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiContactDetailsModel;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiPagedCompanyContactPersonModel;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiPagedCompanyDetailsModel;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiPagedDetailedContact;
import org.eclipse.openk.statementpublicaffairs.model.contactdatabase.ContactApiPagedExternalPersonModel;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Interfaces used to access the ContactBaseData module.
 * 
 * @author Tobias Stummer
 *
 */
@FeignClient(name = "${services.contactdatabase.name}")
public interface ContactDatabaseApi {

	@GetMapping(value = "contacts")
	public ContactApiPagedDetailedContact searchContacts(@RequestHeader("Authorization") String token,
			@RequestParam("contactType") String contactType, @RequestParam("searchText") String searchText,
			Pageable pageable);

	@GetMapping(value = "contacts/{contact-uuid}")
	public ContactApiContactDetailsModel getContact(@RequestHeader("Authorization") String token,
			@RequestParam("contact-uuid") String contactuuid);

	@GetMapping(value = "contacts/{contact-uuid}/addresses")
	public List<ContactApiAddressModel> getContactAddresses(@RequestHeader("Authorization") String token,
			@RequestParam("contact-uuid") String contactuuid);

	@GetMapping(value = "companies")
	public ContactApiPagedCompanyDetailsModel getCompanyList(@RequestHeader("Authorization") String token);

	@GetMapping(value = "companies/{company-uuid}")
	public ContactApiCompanyDetailsModel getCompany(@RequestHeader("Authorization") String token,
			@RequestParam("company-uuid") String companyuuid);

	@GetMapping(value = "companies/{company-uuid}/contact-persons")
	public ContactApiPagedCompanyContactPersonModel getCompanyContactPersons(
			@RequestHeader("Authorization") String token, @RequestParam("company-uuid") String companyuuid);

	@GetMapping(value = "external-persons")
	public ContactApiPagedExternalPersonModel getExternalPersonsList(@RequestHeader("Authorization") String token);

	@GetMapping(value = "contact-persons/{contact-uuid}")
	public ContactApiCompanyContactPersonModel getContactPerson(@RequestHeader("Authorization") String token,
			@RequestParam("contact-uuid") String contactPersonUUID);

}
