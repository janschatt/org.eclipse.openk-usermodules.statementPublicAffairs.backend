/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model;

import org.eclipse.openk.statementpublicaffairs.model.Textblock.TextblockType;

import lombok.Data;

@Data
public class HistoryEntryContentModel {

	private TextblockType type;
	
	private String id;
	
	private DiffType diffType;
	
	private String prevVersion;
	
	private String prevText;
	
	private String text;
	
	private MoveType moved;

	public enum DiffType {
		UNCHANGED,
		NEW,
		CHANGED,
		DELETED
	}
	
	public enum MoveType {
		LONGUP,
		UP,
		NOT,
		DOWN,
		LONGDOWN
	}
}
