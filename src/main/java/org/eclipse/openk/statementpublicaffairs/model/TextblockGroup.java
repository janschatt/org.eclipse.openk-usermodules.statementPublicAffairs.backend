/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model;

import java.util.List;

import lombok.Data;

@Data
public class TextblockGroup {
	private String groupName;
	private List<TextblockItem> textBlocks;

	public boolean valid() {
		if (groupName == null) {
			return false;
		}
		if (textBlocks == null) {
			return false;
		}
		for (TextblockItem item : textBlocks) {
			if (item == null || !item.valid()) {
				return false;
			}
		}
		return true;
	}
}
