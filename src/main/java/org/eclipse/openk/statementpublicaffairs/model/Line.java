/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.pdfbox.pdmodel.font.PDFont;

public class Line {
	
	List<LineSegment> lineSegments;
	Map<FontModification, PDFont> fonts;
	float fontSize;

	public Line(Map<FontModification, PDFont> fonts, float fontSize) {
		this.fonts = fonts;
		this.fontSize = fontSize;
		this.lineSegments = new ArrayList<>();
	}
	
	public List<LineSegment> getSegments() {
		return lineSegments;
	}
	
	public void addSegment(LineSegment segment) {
		this.lineSegments.add(segment);
	}
	
	protected float calculateWidth(List<LineSegment> lineSegments) throws IOException {
		double width = calcSize(fonts.get(FontModification.Normal), fontSize, " ") * (lineSegments.size() - 1);
		for (LineSegment s : lineSegments) {
			width += calcSize(fonts.get(s.getFontMod()), fontSize, s.getWord());
		}
		return (float)width;
	}
	
	
	private static double calcSize(PDFont font, float fontSize, String text) throws IOException {
		return (double)fontSize * font.getStringWidth(text) / 1000.0;
	}

	public float calculateWidth() throws IOException {
		return calculateWidth(lineSegments);
	}

	public float calculateWidth(LineSegment additionalSegment) throws IOException {
		List<LineSegment> segments = new ArrayList<>(lineSegments);
		segments.add(additionalSegment);
		return calculateWidth(segments);
	}
}
