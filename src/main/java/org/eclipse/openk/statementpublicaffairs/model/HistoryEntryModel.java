/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model;

import java.util.List;

import lombok.Data;

@Data
public class HistoryEntryModel {

	private String version;
	
	private String timestamp;
	
	private String user;
	
	private String firstName;
	
	private String lastName;
	
	private String msg;
	
	private List<HistoryEntryContentModel> content;
	
}
