/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.model.db;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.eclipse.openk.statementpublicaffairs.model.Textblock;
import org.springframework.data.annotation.Immutable;

import lombok.Data;

/**
 * @author Tobias Stummer
 *
 */
@Immutable
@Entity
@Data
public class TblStatementdraft {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false)
	private Long id;

	private Long workflowId;

	private Long userId;

	@Column(name = "ts", columnDefinition = "TIMESTAMP")
	private LocalDateTime timestamp;

	private String msg;

	@Convert(converter = TextblockArrangementConverter.class)
	private List<Textblock> draft;

}
