/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Basic response and error text constants.
 * 
 * @author Tobias Stummer
 *
 */
public class Constants {

	private Constants() {
	}

	public static final String STATEMENT_WITH_ID_NOT_FOUND = "Statement with given id could not be found.";
	public static final String ATTACHMENT_OF_STATEMENT_WITH_ID_NOT_FOUND = "Attachment with given attachmentId and given statementId not found.";
	public static final String NO_ATTACHMENT_FILE_SPECIFIED = "Request has no attachment file.";
	public static final String MORE_THAN_ONE_ATTACHMENT_FILE = "Request contains more than one multipart attachment file.";

	public static final ResponseEntity<Object> RESPONSE_OK_NO_CONTENT = ResponseEntity.status(HttpStatus.NO_CONTENT)
			.build();
	public static final ResponseEntity<Object> RESPONSE_OK = ResponseEntity.status(HttpStatus.OK).build();
	public static final ResponseEntity<Object> RESPONSE_NOT_IMPLEMENTED_YET = ResponseEntity
			.status(HttpStatus.NOT_IMPLEMENTED).build();
	public static final ResponseEntity<Object> RESPONSE_SERVICE_NOT_AVAILABLE = ResponseEntity
			.status(HttpStatus.SERVICE_UNAVAILABLE).build();
	public static final ResponseEntity<Object> RESPONSE_OK_CREATED = ResponseEntity.status(HttpStatus.CREATED).build();

}
