/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service.mail;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Store;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.MessageIDTerm;

import org.eclipse.openk.statementpublicaffairs.api.MailUtil;
import org.eclipse.openk.statementpublicaffairs.exceptions.ConfigurationException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.model.AttachmentFile;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailEntry;
import org.eclipse.openk.statementpublicaffairs.model.mail.NewMailContext;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.AttachmentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailSendReport;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class MailContextTest {

	private MailUtil mailUtil = Mockito.mock(MailUtil.class);
	private MailSession session = Mockito.mock(MailSession.class);
	private Store store = Mockito.mock(Store.class);

	private String propertiesPath = "propertiespath";

	@BeforeEach
	void prepare() throws ConfigurationException, NoSuchProviderException {
		Mockito.when(mailUtil.sessionFor(propertiesPath)).thenReturn(session);
		Mockito.when(session.getProperty("mail.smtp.from")).thenReturn("from");
		Mockito.when(session.getStore("imap")).thenReturn(store);
	}

	@Test
	void newMessageShouldReturnMimeMessage() throws ConfigurationException {
		MimeMessage msg = Mockito.mock(MimeMessage.class);
		Mockito.when(session.newMessage()).thenReturn(msg);
		MailContext mailContext = new MailContext(mailUtil, propertiesPath);
		MimeMessage message = mailContext.newMessage();
		assertNotNull(message);
		assertEquals(msg, message);
	}

	@Test
	void getFolderForPath() throws ConfigurationException, MessagingException {
		String folderPath = "folderPath";
		Mockito.when(store.isConnected()).thenReturn(false);
		Mockito.when(session.getStore(folderPath)).thenReturn(store);

		Folder folder = Mockito.mock(Folder.class);
		Mockito.when(store.getFolder(folderPath)).thenReturn(folder);

		MailContext mailContext = new MailContext(mailUtil, propertiesPath);
		Store store = mailContext.getStore();
		Folder responseFolder = store.getFolder(folderPath);
		assertEquals(folder, responseFolder);
		Mockito.verify(store).isConnected();
		Mockito.verify(store).connect();
	}

	@Test
	void getInbox() throws MessagingException, ConfigurationException {
		String folderPath = "INBOX";
		Mockito.when(store.isConnected()).thenReturn(false);
		Mockito.when(session.getStore(folderPath)).thenReturn(store);

		Folder folder = Mockito.mock(Folder.class);
		Mockito.when(store.getFolder(folderPath)).thenReturn(folder);

		MailContext mailContext = new MailContext(mailUtil, propertiesPath);
		Store store = mailContext.getStore();
		Folder responseFolder = mailContext.getInbox(store);
		assertEquals(folder, responseFolder);
		Mockito.verify(store).isConnected();
		Mockito.verify(store).connect();
	}

	@Test
	void getTrash() throws MessagingException, ConfigurationException {
		String folderPath = "Trash";
		Mockito.when(store.isConnected()).thenReturn(false);
		Mockito.when(session.getStore(folderPath)).thenReturn(store);

		Folder folder = Mockito.mock(Folder.class);
		Mockito.when(store.getFolder(folderPath)).thenReturn(folder);

		MailContext mailContext = new MailContext(mailUtil, propertiesPath);
		Store store = mailContext.getStore();
		Folder responseFolder = mailContext.getTrash(store);
		assertEquals(folder, responseFolder);
		Mockito.verify(store).isConnected();
		Mockito.verify(store).connect();
	}

	@Test
	void getProcessStatementBox() throws MessagingException, ConfigurationException {
		String folderPath = "Processed Statements";
		Mockito.when(store.isConnected()).thenReturn(false);
		Mockito.when(session.getStore(folderPath)).thenReturn(store);

		Folder folder = Mockito.mock(Folder.class);
		Mockito.when(store.getFolder(folderPath)).thenReturn(folder);

		MailContext mailContext = new MailContext(mailUtil, propertiesPath);
		Store store = mailContext.getStore();
		Folder responseFolder = mailContext.getProcessedStatementBox(store);
		assertEquals(folder, responseFolder);
		Mockito.verify(store).isConnected();
		Mockito.verify(store).connect();
	}

	@Test
	void getInboxMessageIds() throws MessagingException, ConfigurationException {
		String folderPath = "INBOX";
		Mockito.when(store.isConnected()).thenReturn(false);
		Mockito.when(session.getStore(folderPath)).thenReturn(store);

		Folder inbox = Mockito.mock(Folder.class);
		Mockito.when(store.getFolder(folderPath)).thenReturn(inbox);

		Message messages[] = new Message[2];

		Message ownMessage = Mockito.mock(Message.class);
		InternetAddress ownAddress = Mockito.mock(InternetAddress.class);
		String filterMailToAddress = "filterMailToAddress@nix.tld";
		Mockito.when(ownAddress.getAddress()).thenReturn(filterMailToAddress);
		InternetAddress[] ownAddresses = { ownAddress };
		Mockito.when(ownMessage.getAllRecipients()).thenReturn(ownAddresses);
		InternetAddress otherAddress = Mockito.mock(InternetAddress.class);
		Mockito.when(otherAddress.getAddress()).thenReturn("other");
		InternetAddress[] otherAddresses = { otherAddress };
		Message otherMessage = Mockito.mock(Message.class);
		Mockito.when(otherMessage.getAllRecipients()).thenReturn(otherAddresses);

		Mockito.when(ownMessage.getHeader("Message-ID")).thenReturn(new String[] { "own" });
		Mockito.when(otherMessage.getHeader("Message-ID")).thenReturn(new String[] { "other" });

		messages[0] = ownMessage;
		messages[1] = otherMessage;
		Mockito.when(inbox.getMessages()).thenReturn(messages);

		MailContext mailContext = new MailContext(mailUtil, propertiesPath);
		List<String> messageIdsAll = mailContext.getInboxMessageIds(null);

		Mockito.verify(inbox).open(Folder.READ_ONLY);
		Mockito.verify(inbox).close();

		List<String> messageIdsOwn = mailContext.getInboxMessageIds(filterMailToAddress);

		assertEquals(2, messageIdsAll.size());
		assertEquals(1, messageIdsOwn.size());
		assertTrue(messageIdsAll.contains("own"));
		assertTrue(messageIdsAll.contains("other"));

		assertTrue(messageIdsOwn.contains("own"));
		assertFalse(messageIdsOwn.contains("other"));
	}

	@Test
	void getInboxMessages() throws MessagingException, ConfigurationException, IOException {
		String folderPath = "INBOX";
		Mockito.when(store.isConnected()).thenReturn(false);
		Mockito.when(session.getStore(folderPath)).thenReturn(store);

		Folder inbox = Mockito.mock(Folder.class);
		Mockito.when(store.getFolder(folderPath)).thenReturn(inbox);

		Message messages[] = new Message[2];

		Message ownMessage = Mockito.mock(Message.class);
		InternetAddress ownAddress = Mockito.mock(InternetAddress.class);
		InternetAddress fromAddress = Mockito.mock(InternetAddress.class);
		Mockito.when(fromAddress.toString()).thenReturn("from");
		String filterMailToAddress = "filterMailToAddress@nix.tld";
		Mockito.when(ownAddress.getAddress()).thenReturn(filterMailToAddress);
		InternetAddress[] ownAddresses = { ownAddress };
		Mockito.when(ownMessage.getAllRecipients()).thenReturn(ownAddresses);
		InternetAddress otherAddress = Mockito.mock(InternetAddress.class);
		Mockito.when(otherAddress.getAddress()).thenReturn("other");
		InternetAddress[] otherAddresses = { otherAddress };
		Message otherMessage = Mockito.mock(Message.class);
		Mockito.when(otherMessage.getAllRecipients()).thenReturn(otherAddresses);

		// messageid
		Mockito.when(ownMessage.getHeader("Message-ID")).thenReturn(new String[] { "own" });
		Mockito.when(otherMessage.getHeader("Message-ID")).thenReturn(new String[] { "other" });

		// from
		Mockito.when(ownMessage.getFrom()).thenReturn(new Address[] { fromAddress });
		Mockito.when(otherMessage.getFrom()).thenReturn(new Address[] { fromAddress });

		// subject
		Mockito.when(ownMessage.getSubject()).thenReturn("subject");
		Mockito.when(otherMessage.getSubject()).thenReturn("subject");

		// text
		Mockito.when(ownMessage.isMimeType("text/plain")).thenReturn(true);
		Mockito.when(ownMessage.getContent()).thenReturn("content");
		Mockito.when(otherMessage.isMimeType("text/plain")).thenReturn(true);
		Mockito.when(otherMessage.getContent()).thenReturn("content");

		messages[0] = ownMessage;
		messages[1] = otherMessage;
		Mockito.when(inbox.getMessages()).thenReturn(messages);

		MailContext mailContext = new MailContext(mailUtil, propertiesPath);
		List<MailEntry> messagesAll = mailContext.getInboxMessages(null);

		Mockito.verify(inbox).open(Folder.READ_ONLY);
		Mockito.verify(inbox).close();

		List<MailEntry> messagesOwn = mailContext.getInboxMessages(filterMailToAddress);

		assertEquals(2, messagesAll.size());
		assertEquals(1, messagesOwn.size());

		assertEquals("own", messagesOwn.get(0).getIdentifier());
		assertEquals("own", messagesAll.get(0).getIdentifier());
		assertEquals("other", messagesAll.get(1).getIdentifier());

	}

	@Test
	void getAttachments() throws ConfigurationException, MessagingException, IOException {
		String messageId = "messageId";
		Set<String> fileNames = new HashSet<>();
		fileNames.add("fileName");

		String folderPath = "INBOX";
		String folderPath2 = "Processed Statements";
		Mockito.when(store.isConnected()).thenReturn(false);
		Mockito.when(session.getStore(folderPath)).thenReturn(store);
		Mockito.when(session.getStore(folderPath2)).thenReturn(store);

		Folder inbox = Mockito.mock(Folder.class);
		Mockito.when(store.getFolder(folderPath)).thenReturn(inbox);

		Folder statementbox = Mockito.mock(Folder.class);
		Mockito.when(store.getFolder(folderPath2)).thenReturn(statementbox);

		Message[] inboxMessages = new Message[1];
		Message msg = Mockito.mock(Message.class);
		inboxMessages[0] = msg;
		Mockito.when(inbox.search(Mockito.any(MessageIDTerm.class))).thenReturn(inboxMessages);
		Mockito.when(statementbox.search(Mockito.any(MessageIDTerm.class))).thenReturn(new Message[] {});

		Mockito.when(msg.isMimeType("multipart/*")).thenReturn(true);

		MimeMultipart multipart = Mockito.mock(MimeMultipart.class);
		Mockito.when(msg.getContent()).thenReturn(multipart);
		Mockito.when(multipart.getCount()).thenReturn(1);
		BodyPart bodyPart = Mockito.mock(BodyPart.class);
		Mockito.when(multipart.getBodyPart(0)).thenReturn(bodyPart);
		Mockito.when(bodyPart.getContent()).thenReturn(new Object());
		Mockito.when(bodyPart.getContentType()).thenReturn("contentType");
		Mockito.when(bodyPart.getFileName()).thenReturn("fileName");
		byte[] bytes = "TestDaten".getBytes();
		InputStream is = new ByteArrayInputStream(bytes);
		Mockito.when(bodyPart.getInputStream()).thenReturn(is);

		MailContext mailContext = new MailContext(mailUtil, propertiesPath);
		Map<String, AttachmentFile> attachments = mailContext.getAttachments(messageId, fileNames);

		assertTrue(attachments.containsKey("fileName"));
		assertEquals("contentType", attachments.get("fileName").getType());
		assertEquals("fileName", attachments.get("fileName").getName());
		assertEquals(bytes.length, attachments.get("fileName").getLength());
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		AttachmentFile file = attachments.get("fileName");
		int nRead;
		byte[] data = new byte[(int) file.getLength()];
		while ((nRead = file.getRessource().read(data, 0, data.length)) != -1) {
			buffer.write(data, 0, nRead);
		}

		byte[] resultbytes = buffer.toByteArray();
		assertEquals(bytes.length, resultbytes.length);
		for (int i = 0; i < bytes.length; i++) {
			assertEquals(bytes[i], resultbytes[i]);
		}

	}

	@Test
	void getAttachmentsFromMessage() throws ConfigurationException, IOException, MessagingException {

		MailContext mailContext = new MailContext(mailUtil, propertiesPath);
		Message msg = Mockito.mock(Message.class);

		Mockito.when(msg.isMimeType("multipart/*")).thenReturn(true);
		MimeMultipart multipart = Mockito.mock(MimeMultipart.class);
		Mockito.when(msg.getContent()).thenReturn(multipart);
		Mockito.when(multipart.getCount()).thenReturn(1);
		BodyPart bodyPart = Mockito.mock(BodyPart.class);
		Mockito.when(multipart.getBodyPart(0)).thenReturn(bodyPart);
		Mockito.when(bodyPart.getContent()).thenReturn(new Object());
		Mockito.when(bodyPart.getContentType()).thenReturn("contentType");
		Mockito.when(bodyPart.getFileName()).thenReturn("fileName");
		Mockito.when(bodyPart.getSize()).thenReturn(12);
		List<AttachmentModel> attachments = mailContext.getAttachmentsFromMessage(msg);
		assertEquals(1, attachments.size());
		AttachmentModel result = attachments.get(0);
		assertEquals("contentType", result.getType());
		assertEquals("fileName", result.getName());
		assertEquals(12, result.getSize());
	}

	@Test
	void getTextFromMessage() throws MessagingException, IOException, ConfigurationException {
		Message msg = Mockito.mock(Message.class);

		Mockito.when(msg.isMimeType("text/plain")).thenReturn(false);
		Mockito.when(msg.isMimeType("multipart/*")).thenReturn(true);
		MimeMultipart multipart = Mockito.mock(MimeMultipart.class);
		Mockito.when(msg.getContent()).thenReturn(multipart);
		Mockito.when(multipart.getCount()).thenReturn(1);
		BodyPart bodyPart = Mockito.mock(BodyPart.class);
		Mockito.when(multipart.getBodyPart(0)).thenReturn(bodyPart);
		Mockito.when(msg.isMimeType("text/plain")).thenReturn(false);
		MimeMultipart multipart2 = Mockito.mock(MimeMultipart.class);
		Mockito.when(bodyPart.getContent()).thenReturn(multipart2);

		Mockito.when(multipart.getCount()).thenReturn(1);
		BodyPart bodyPart2 = Mockito.mock(BodyPart.class);
		Mockito.when(multipart.getBodyPart(0)).thenReturn(bodyPart2);

		Mockito.when(bodyPart2.isMimeType("text/plain")).thenReturn(true);
		Mockito.when(bodyPart2.getContent()).thenReturn("Text");

		MailContext mailContext = new MailContext(mailUtil, propertiesPath);
		String text = mailContext.getTextFromMessage(msg);

		assertEquals("\nText", text);

	}

	@Test
	void deleteMail() throws InternalErrorServiceException, NotFoundServiceException, InterruptedException,
			ConfigurationException, MessagingException {
		String messageId = "messageId";
		String folderPath = "INBOX";
		String folderPath2 = "Trash";

		Mockito.when(store.isConnected()).thenReturn(false);
		Mockito.when(session.getStore(folderPath)).thenReturn(store);
		Mockito.when(session.getStore(folderPath2)).thenReturn(store);

		Folder inbox = Mockito.mock(Folder.class);
		Mockito.when(store.getFolder(folderPath)).thenReturn(inbox);

		Folder trash = Mockito.mock(Folder.class);
		Mockito.when(store.getFolder(folderPath2)).thenReturn(trash);

		Message msg = Mockito.mock(Message.class);
		Mockito.when(inbox.search(Mockito.any(MessageIDTerm.class))).thenReturn(new Message[] { msg });

		MailContext mailContext = new MailContext(mailUtil, propertiesPath);
		mailContext.deleteMail(messageId);

		Mockito.verify(inbox).copyMessages(Mockito.any(), Mockito.eq(trash));

		Mockito.verify(inbox).expunge();

	}

	@Test
	void setProcessedMail() throws InternalErrorServiceException, NotFoundServiceException, InterruptedException,
			ConfigurationException, MessagingException {
		String messageId = "messageId";
		String folderPath = "INBOX";
		String folderPath2 = "Processed Statements";

		Mockito.when(store.isConnected()).thenReturn(false);
		Mockito.when(session.getStore(folderPath)).thenReturn(store);
		Mockito.when(session.getStore(folderPath2)).thenReturn(store);

		Folder inbox = Mockito.mock(Folder.class);
		Mockito.when(store.getFolder(folderPath)).thenReturn(inbox);

		Folder statements = Mockito.mock(Folder.class);
		Mockito.when(store.getFolder(folderPath2)).thenReturn(statements);

		Message msg = Mockito.mock(Message.class);
		Mockito.when(inbox.search(Mockito.any(MessageIDTerm.class))).thenReturn(new Message[] { msg });

		MailContext mailContext = new MailContext(mailUtil, propertiesPath);
		mailContext.setProcessedMail(messageId);

		Mockito.verify(inbox).copyMessages(Mockito.any(), Mockito.eq(statements));

		Mockito.verify(inbox).expunge();

	}

	@Test
	void sendMail() throws ConfigurationException, MessagingException {
		NewMailContext newMailContext = new NewMailContext();

		String recipient1 = "recipient1@testtest.xyz";
		String recipient2 = "recipient2@testtest.xyz";
		newMailContext.setRecipients(new HashSet<>());
		newMailContext.getRecipients().add(recipient1);
		newMailContext.getRecipients().add(recipient2);
		newMailContext.setSubject("subject");
		newMailContext.setMailText("Text");
		newMailContext.setAttachments(new ArrayList<>());

		MimeMessage msg = Mockito.mock(MimeMessage.class);
		Mockito.when(session.newMessage()).thenReturn(msg);

		MailSendReport report = new MailSendReport();
		report.setSuccessful(true);
		Mockito.when(mailUtil.sendMail(Mockito.any(MimeMessage.class))).thenReturn(report);

		MailContext mailContext = new MailContext(mailUtil, propertiesPath);

		assertTrue(mailContext.sendMail(newMailContext).getSuccessful());

		Mockito.verify(mailUtil).sendMail(Mockito.any(MimeMessage.class));

	}

	@Test
	void getMail() throws ConfigurationException, MessagingException {
		String messageId = "messageId";
		String folderPath = "INBOX";
		String folderPath2 = "Processed Statements";

		Mockito.when(store.isConnected()).thenReturn(false);
		Mockito.when(session.getStore(folderPath)).thenReturn(store);
		Mockito.when(session.getStore(folderPath2)).thenReturn(store);

		Folder inbox = Mockito.mock(Folder.class);
		Mockito.when(store.getFolder(folderPath)).thenReturn(inbox);

		Folder statements = Mockito.mock(Folder.class);
		Mockito.when(store.getFolder(folderPath2)).thenReturn(statements);

		Message msg = Mockito.mock(Message.class);
		String subject = "subject";
		Mockito.when(msg.getSubject()).thenReturn(subject);
		String fromString = "from@server.tld";
		Address[] from = new Address[] { new InternetAddress(fromString) };
		Mockito.when(msg.getFrom()).thenReturn(from);
		Date date = Date.from(Instant.now());
		Mockito.when(msg.getSentDate()).thenReturn(date);
		Mockito.when(msg.getHeader("Message-ID")).thenReturn(new String[] { "messageId" });
		Mockito.when(inbox.search(Mockito.any(MessageIDTerm.class))).thenReturn(new Message[] { msg });
		Mockito.when(statements.search(Mockito.any(MessageIDTerm.class))).thenReturn(new Message[] {});

		MailContext mailContext = new MailContext(mailUtil, propertiesPath);
		Optional<MailEntry> oMail = mailContext.getMail(messageId);
		assertTrue(oMail.isPresent());
		MailEntry mail = oMail.get();
		assertEquals(subject, mail.getSubject());
		assertEquals(fromString, mail.getFrom());
		assertEquals(TypeConversion.iso8601InstantStringOfDate(date).get(), mail.getDate());

	}

}
