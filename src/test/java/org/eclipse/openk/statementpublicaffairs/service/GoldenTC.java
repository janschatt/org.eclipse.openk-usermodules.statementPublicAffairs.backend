/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.List;
import java.util.Map;

import org.eclipse.openk.statementpublicaffairs.model.HistoryEntryModel;
import org.eclipse.openk.statementpublicaffairs.model.TextblockDefinition;

import lombok.Data;

@Data
public class GoldenTC {

	public Long statementId;
	public TextblockDefinition textBlockDefinition;
	public List<DraftEntry> draftEntries;
	public Map<String, HistoryEntryModel> versions;
}