/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.api.AuthNAuthApi;
import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationUsersService;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.KeyCloakUser;
import org.eclipse.openk.statementpublicaffairs.model.ReqDepartmentDetailsModel;
import org.eclipse.openk.statementpublicaffairs.model.UserModel;
import org.eclipse.openk.statementpublicaffairs.model.conf.EvaluationResult;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblReqdepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblUser;
import org.eclipse.openk.statementpublicaffairs.model.db.TblUser2Department;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementReqdepartmentUsers;
import org.eclipse.openk.statementpublicaffairs.repository.ReqDepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.User2DepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.UserRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementReqdepartmentUsersRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = TestConfigurationUsersService.class)
@ActiveProfiles("test")
class UsersServiceTest {

	@Autowired
	private SessionService sessionService;

	@Autowired
	private AuthNAuthApi authNAuthApi;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private VwStatementReqdepartmentUsersRepository vwsRepository;

	@Autowired
	private UsersService usersService;

	@Autowired
	private UserInfoService userInfoService;

	@Autowired
	private ReqDepartmentRepository reqDepartmentRepository;

	@Autowired
	private User2DepartmentRepository user2DepartmentRepository;

	@Autowired
	private StatementAuthorizationService authorizationService;

	@Test
	void syncKeycloakUsers() throws InternalErrorServiceException, ForbiddenServiceException {

		String token = "token";

		String firstName = "firstName";
		String lastName = "lastName";
		String username = "username";

		Mockito.when(sessionService.getToken(Mockito.anyString(), Mockito.anyString())).thenReturn(token);

		List<KeyCloakUser> kcUsers = new ArrayList<>();
		KeyCloakUser kcUser = new KeyCloakUser();
		kcUsers.add(kcUser);
		kcUser.setFirstName(firstName);
		kcUser.setLastName(lastName);
		kcUser.setUsername(username);
		kcUser.setAllRoles(new ArrayList<>());

		String roleId = UserRoles.keyCloakRoleIdof(UserRoles.SPA_ACCESS);

		Mockito.when(authNAuthApi.getUsersForRole(token, roleId)).thenReturn(kcUsers);

		Mockito.when(userRepository.findByUsername(username)).thenReturn(new ArrayList<>());

		TblUser newUser = new TblUser();
		newUser.setUsername(username);
		newUser.setFirstName(firstName);
		newUser.setLastName(lastName);

		usersService.syncKeycloakUsers();

		Mockito.verify(userRepository).save(newUser);
	}

	@Test
	void getUsersWithRole() throws InternalErrorServiceException {
		String token = "token";

		String firstName = "firstName";
		String lastName = "lastName";
		String username = "username";

		String emailAddress = "emailAddress";
		String fax = "fax";
		String phone = "phone";
		String initials = "initials";

		Mockito.when(sessionService.getToken(Mockito.anyString(), Mockito.anyString())).thenReturn(token);

		List<KeyCloakUser> kcUsers = new ArrayList<>();
		KeyCloakUser kcUser = new KeyCloakUser();
		kcUsers.add(kcUser);
		kcUser.setFirstName(firstName);
		kcUser.setLastName(lastName);
		kcUser.setUsername(username);
		kcUser.setAllRoles(new ArrayList<>());

		String role = "role";
		Mockito.when(authNAuthApi.getUsersForRole(token, role)).thenReturn(kcUsers);

		List<TblUser> tblUsers = new ArrayList<TblUser>();
		TblUser tblUser = new TblUser();
		tblUsers.add(tblUser);
		tblUser.setFirstName(firstName);
		tblUser.setLastName(lastName);
		tblUser.setUsername(username);
		tblUser.setEmailAddress(emailAddress);
		tblUser.setFax(fax);
		tblUser.setPhone(phone);
		tblUser.setInitials(initials);
		Mockito.when(userRepository.findByUsername(username)).thenReturn(tblUsers);

		List<UserModel> users = usersService.getUsersWithRole(role);
		assertEquals(1, users.size());
		UserModel user = users.get(0);
		assertEquals(firstName, user.getFirstName());
		assertEquals(lastName, user.getLastName());
		assertEquals(username, user.getUsername());
		assertEquals(emailAddress, user.getEmailAddress());
		
		assertEquals(fax, user.getFax());
		assertEquals(phone, user.getPhone());
		assertEquals(initials, user.getInitials());
	}

	@Test
	void getUsersOfRequiredDivisionsOfStatement() {
		Long statementId = 1234L;
		String firstName = "firstName";
		String lastName = "lastName";
		String userName = "userName";

		String emailAddress = "emailAddress";

		List<VwStatementReqdepartmentUsers> vwUsers = new ArrayList<>();
		VwStatementReqdepartmentUsers vwUser = new VwStatementReqdepartmentUsers();
		vwUsers.add(vwUser);
		vwUser.setFirstName(firstName);
		vwUser.setLastName(lastName);
		vwUser.setUserName(userName);
		vwUser.setEmailAddress(emailAddress);

		Mockito.when(vwsRepository.findByStatementId(statementId)).thenReturn(vwUsers);

		Set<UserModel> result = usersService.getUsersOfRequiredDivisionsOfStatement(statementId);

		assertEquals(1, result.size());

		for (UserModel resultModel : result) {
			assertEquals(firstName, resultModel.getFirstName());
			assertEquals(lastName, resultModel.getLastName());
			assertEquals(userName, resultModel.getUsername());
			assertEquals(emailAddress, resultModel.getEmailAddress());
		}
	}

	@Test
	void userOfRequiredDivisionsOfStatement() {
		Long statementId = 1234L;
		String firstName = "firstName";
		String lastName = "lastName";
		String userName = "userName";

		String emailAddress = "emailAddress";

		List<VwStatementReqdepartmentUsers> vwUsers = new ArrayList<>();
		VwStatementReqdepartmentUsers vwUser = new VwStatementReqdepartmentUsers();
		vwUsers.add(vwUser);
		vwUser.setFirstName(firstName);
		vwUser.setLastName(lastName);
		vwUser.setUserName(userName);
		vwUser.setEmailAddress(emailAddress);

		Mockito.when(vwsRepository.findByStatementId(statementId)).thenReturn(vwUsers);

		UserModel user = usersService.userOfRequiredDivisionsOfStatement(statementId, userName);

		assertNotNull(user);
		assertEquals(firstName, user.getFirstName());
		assertEquals(lastName, user.getLastName());
		assertEquals(userName, user.getUsername());
		assertEquals(emailAddress, user.getEmailAddress());
	}

	@Test
	void requiredDepartmentusers() {
		Long statementId = 1234L;
		Boolean optional = true;
		Boolean contributed = true;

		Long departmentId = 11L;
		String departmentGroup = "departmentGroup";
		String departmentName = "departmentName";

		String emailAddress = "emailAddress";
		String firstName = "firstName";
		String lastName = "lastNamej";
		String userName = "userName";

		List<TblReqdepartment> tblReqDepartments = new ArrayList<TblReqdepartment>();

		TblReqdepartment trd = new TblReqdepartment();
		tblReqDepartments.add(trd);
		trd.setOptional(optional);
		trd.setContributed(contributed);
		TblDepartment dep = new TblDepartment();
		dep.setDepartmentgroup(departmentGroup);
		dep.setName(departmentName);
		trd.setDepartment(dep);

		Set<TblUser> users = new HashSet<>();
		TblUser user = new TblUser();
		user.setEmailAddress(emailAddress);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setUsername(userName);
		users.add(user);

		List<VwStatementReqdepartmentUsers> vws = new ArrayList<>();
		VwStatementReqdepartmentUsers vwu = Mockito.mock(VwStatementReqdepartmentUsers.class);
		vws.add(vwu);
		Mockito.when(vwu.getUserName()).thenReturn(userName);
		Mockito.when(vwu.getFirstName()).thenReturn(firstName);
		Mockito.when(vwu.getLastName()).thenReturn(lastName);
		Mockito.when(vwu.getEmailAddress()).thenReturn(emailAddress);
		Mockito.when(vwu.getDepartmentContributed()).thenReturn(true);
		Mockito.when(vwu.getDepartmentGroup()).thenReturn(departmentGroup);
		Mockito.when(vwu.getDepartmentName()).thenReturn(departmentName);
		Mockito.when(vwu.getDepartmentId()).thenReturn(departmentId);

		Mockito.when(vwsRepository.findByStatementId(statementId)).thenReturn(vws);

		List<ReqDepartmentDetailsModel> reqDepartments = usersService.requiredDepartmentUsers(statementId);

		assertFalse(reqDepartments.isEmpty());
		assertEquals(1, reqDepartments.size());

		ReqDepartmentDetailsModel rdpm = reqDepartments.get(0);

		assertEquals(departmentGroup, rdpm.getDepartmentGroup());
		assertEquals(departmentName, rdpm.getDepartmentName());
		List<UserModel> rdpmus = rdpm.getDepartmentUsers();
		assertEquals(1, rdpmus.size());
		UserModel rdpmu = rdpmus.get(0);

		assertEquals(emailAddress, rdpmu.getEmailAddress());
		assertEquals(firstName, rdpmu.getFirstName());
		assertEquals(lastName, rdpmu.getLastName());
		assertEquals(userName, rdpmu.getUsername());

	}

	@Test
	void isRequiredDepartmentUser() {

		Long statementId = 1234L;
		String userName = "userName";

		List<TblReqdepartment> reqDepartments = new ArrayList<>();
		TblReqdepartment rdp = new TblReqdepartment();
		TblDepartment department = new TblDepartment();
		Set<TblUser> users = new HashSet<>();
		TblUser user = new TblUser();
		user.setUsername(userName);
		users.add(user);
		rdp.setDepartment(department);
		reqDepartments.add(rdp);

		List<VwStatementReqdepartmentUsers> vwsList = new ArrayList<>();

		VwStatementReqdepartmentUsers vws = Mockito.mock(VwStatementReqdepartmentUsers.class);
		Mockito.when(vws.getUserName()).thenReturn(userName);
		vwsList.add(vws);
		Mockito.when(vwsRepository.findByStatementId(statementId)).thenReturn(vwsList);

		assertTrue(usersService.isRequiredDepartmentUser(statementId, userName));
		assertFalse(usersService.isRequiredDepartmentUser(statementId, "invalidUserName"));
	}

	@Test
	void ownDepartmentIds() {
		String username = "username";
		Long userId = 11L;
		Long departmentId = 12L;
		;

		Mockito.when(userInfoService.getUserName()).thenReturn(username);

		assertTrue(usersService.ownDepartmentIds().isEmpty());

		List<TblUser> users = new ArrayList<>();
		TblUser user = new TblUser();
		user.setUsername(username);
		user.setId(userId);
		TblDepartment department = new TblDepartment();
		department.setId(departmentId);

		users.add(user);
		Mockito.when(userRepository.findByUsername(username)).thenReturn(users);

		List<TblUser2Department> user2Departments = new ArrayList<>();
		TblUser2Department user2Department = Mockito.mock(TblUser2Department.class);
		Mockito.when(user2Department.getDepartmentId()).thenReturn(departmentId);
		user2Departments.add(user2Department);

		Mockito.when(user2DepartmentRepository.findByUserId(userId)).thenReturn(user2Departments);

		Set<Long> departmentIds = usersService.ownDepartmentIds();
		Set<Long> compareSet = new HashSet<>();
		compareSet.add(departmentId);
		assertEquals(compareSet, departmentIds);
	}

	@Test
	void isAllowedToClaimTask() {

		Long statementId = 1234L;
		String userName = "userName";
		String taskDefinitionKey1 = "taskDefinitionKey1";
		String taskDefinitionKey2 = "taskDefinitionKey2";
		String taskDefinitionKey3 = "taskDefinitionKey3";

		Mockito.when(authorizationService.evaluateAuthorization(taskDefinitionKey1))
				.thenReturn(EvaluationResult.ACCEPTED);
		Mockito.when(authorizationService.evaluateAuthorization(taskDefinitionKey2))
				.thenReturn(EvaluationResult.DENIED);

		assertTrue(usersService.isAllowedToClaimTask(statementId, taskDefinitionKey1));
		assertFalse(usersService.isAllowedToClaimTask(statementId, taskDefinitionKey2));

		List<TblReqdepartment> reqDepartments = new ArrayList<>();
		TblReqdepartment rdp = new TblReqdepartment();
		TblDepartment department = new TblDepartment();
		rdp.setDepartment(department);
		reqDepartments.add(rdp);
		Mockito.when(reqDepartmentRepository.findByStatementId(statementId)).thenReturn(reqDepartments);

		Mockito.when(userInfoService.getUserName()).thenReturn(userName);

		Mockito.when(authorizationService.evaluateAuthorization(taskDefinitionKey3))
				.thenReturn(EvaluationResult.ONLY_REQ_DEPARTMENT_USER);

		List<VwStatementReqdepartmentUsers> vwsList = new ArrayList<>();

		VwStatementReqdepartmentUsers vws = Mockito.mock(VwStatementReqdepartmentUsers.class);
		Mockito.when(vws.getUserName()).thenReturn(userName);
		vwsList.add(vws);
		Mockito.when(vwsRepository.findByStatementId(statementId)).thenReturn(vwsList);

		assertTrue(usersService.isAllowedToClaimTask(statementId, taskDefinitionKey3));

	}

}
