/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.eclipse.openk.statementpublicaffairs.api.MailUtil;
import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationNotifyService;
import org.eclipse.openk.statementpublicaffairs.config.auth.UserRoles;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.UserModel;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementReqdepartmentUsers;
import org.eclipse.openk.statementpublicaffairs.model.mail.MailConfiguration;
import org.eclipse.openk.statementpublicaffairs.model.mail.MessageConfiguration;
import org.eclipse.openk.statementpublicaffairs.model.mail.NewMailContext;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementReqdepartmentUsersRepository;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = TestConfigurationNotifyService.class)
@ActiveProfiles("test")
class NotifyServiceTest {

	@Autowired
	private MailUtil mailUtil;
	@Autowired
	private NotifyService notificationService;

	@Autowired
	private MailService mailService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private ReplacementStringsService replacementStringsService;

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private VwStatementReqdepartmentUsersRepository vwStateReqDepUserRep;

	@Test
	void notifyNewStatementMailInboxNotificationShouldcallMailServiceSendNotifyMail()
			throws InternalErrorServiceException {
		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		List<UserModel> users = new ArrayList<>();
		UserModel um = new UserModel();
		um.setEmailAddress("emailAddress");
		users.add(um);
		MailConfiguration mailConfig = new MailConfiguration();
		mailConfig.setNotificationInboxNewMail(messageConfig);
		Mockito.when(usersService.getUsersWithRole(UserRoles.SPA_OFFICIAL_IN_CHARGE)).thenReturn(users);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);
		notificationService.notifyNewStatementMailInboxNotification();
		Mockito.verify(mailService).sendNotifyMail(Mockito.any(NewMailContext.class));
	}

	@Test
	void notifyNewStatementMailInboxNotificationExceptionOnGetMailConfigShouldThrowInternalErrorServiceException()
			throws InternalErrorServiceException {
		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		Mockito.when(mailUtil.getMailConfiguration()).thenThrow(new InternalErrorServiceException());
		notificationService.notifyNewStatementMailInboxNotification();
		Mockito.verify(mailService, Mockito.never()).sendNotifyMail(Mockito.any(NewMailContext.class));
	}

	@Test
	void notifyApprovedShouldCallMailServiceSendNotifyMail() throws InternalErrorServiceException {
		Long statementId = 1234L;
		Object value = true;

		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		MailConfiguration mailConfig = new MailConfiguration();
		mailConfig.setNotificationApprovedAndSent(messageConfig);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);

		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statementRepository.findById(statementId)).thenReturn(Optional.of(statement));
		Map<String, String> replacements = new HashMap<>();
		replacements.put("no", "yeah");
		Mockito.when(replacementStringsService.getAllReplacements(statement)).thenReturn(replacements);
		notificationService.notifyApproved(statementId, value);
		Mockito.verify(mailService).sendNotifyMail(Mockito.any(NewMailContext.class));
	}

	@Test
	void notifyApprovedValueFalseShouldCallMailServiceSendNotifyMail() throws InternalErrorServiceException {
		Long statementId = 1234L;
		Object value = false;

		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		MailConfiguration mailConfig = new MailConfiguration();
		mailConfig.setNotificationApprovedAndNotSent(messageConfig);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);

		TblStatement statement = Mockito.mock(TblStatement.class);
		Mockito.when(statementRepository.findById(statementId)).thenReturn(Optional.of(statement));
		Map<String, String> replacements = new HashMap<>();
		replacements.put("no", "yeah");
		Mockito.when(replacementStringsService.getAllReplacements(statement)).thenReturn(replacements);
		notificationService.notifyApproved(statementId, value);
		Mockito.verify(mailService).sendNotifyMail(Mockito.any(NewMailContext.class));
	}

	@Test
	void notifyApprovedNonBooleanValueShouldNotSendNotifyMail() {
		notificationService.notifyApproved(1L, "");
		Mockito.verify(mailService, Mockito.never()).sendNotifyMail(Mockito.any(NewMailContext.class));
	}

	@Test
	void notifyEnrichDraft() throws InternalErrorServiceException {
		Long statementId = 1234L;

		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		List<VwStatementReqdepartmentUsers> reqdeps = new ArrayList<>();
		VwStatementReqdepartmentUsers reqdep = new VwStatementReqdepartmentUsers();
		reqdep.setEmailAddress("emailAddress");
		reqdep.setDepartmentName("departmentName");
		reqdep.setStandIn(false);
		reqdeps.add(reqdep);
		Mockito.when(vwStateReqDepUserRep.findByStatementId(statementId)).thenReturn(reqdeps);
		MailConfiguration mailConfig = new MailConfiguration();
		mailConfig.setNotificationEnrichDraft(messageConfig);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);
		notificationService.notifyEnrichDraft(statementId);
		Mockito.verify(mailService).sendNotifyMail(Mockito.any(NewMailContext.class));
	}

	@Test
	void notifyApprovalRequired() throws InternalErrorServiceException {
		Long statementId = 1234L;

		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		MailConfiguration mailConfig = new MailConfiguration();
		mailConfig.setNotificationApprovalRequired(messageConfig);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);
		notificationService.notifyApprovalRequired(statementId);
		Mockito.verify(mailService).sendNotifyMail(Mockito.any(NewMailContext.class));
	}

	@Test
	void notifyOnAllMandatoryContributions() throws InternalErrorServiceException {
		Long statementId = 1234L;
		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		MailConfiguration mailConfig = new MailConfiguration();
		mailConfig.setNotificationAllMandatoryContributions(messageConfig);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);
		notificationService.notifyOnAllMandatoryContributions(statementId);
		Mockito.verify(mailService).sendNotifyMail(Mockito.any(NewMailContext.class));
	}

	@Test
	void notifyNotApproves() throws InternalErrorServiceException {
		Long statementId = 1234L;
		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		MailConfiguration mailConfig = new MailConfiguration();
		mailConfig.setNotificationNotApproved(messageConfig);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);
		notificationService.notifyNotApproved(statementId);
		Mockito.verify(mailService).sendNotifyMail(Mockito.any(NewMailContext.class));
	}

	@Test
	void notifyDepartmentsDueDate() throws InternalErrorServiceException {
		Long statementId = 1234L;
		MailConfiguration mailConfig = new MailConfiguration();
		MessageConfiguration messageConfig = new MessageConfiguration();
		messageConfig.setSubject("subject");
		messageConfig.setBody("body");
		mailConfig.setNotificationDueDepartmentContributions(messageConfig);
		List<VwStatementReqdepartmentUsers> reqdeps = new ArrayList<>();
		VwStatementReqdepartmentUsers reqdep = new VwStatementReqdepartmentUsers();
		reqdep.setEmailAddress("emailAddress");
		reqdep.setDepartmentName("departmentName");
		reqdep.setStandIn(false);
		reqdeps.add(reqdep);
		Mockito.when(vwStateReqDepUserRep.findByStatementId(statementId)).thenReturn(reqdeps);
		Mockito.when(mailUtil.getMailConfiguration()).thenReturn(mailConfig);
		notificationService.notifyDepartmentsDueDate(statementId);
		Mockito.verify(mailService).sendNotifyMail(Mockito.any(NewMailContext.class));
	}

	@Test
	void notifyDepartmentsDueDateExceptionOnMailConfig() throws InternalErrorServiceException {
		Long statementId = 1234L;
		Mockito.when(mailUtil.getMailConfiguration()).thenThrow(new InternalErrorServiceException());
		notificationService.notifyDepartmentsDueDate(statementId);
		Mockito.verify(mailService, Mockito.never()).sendNotifyMail(Mockito.any(NewMailContext.class));
	}

}
