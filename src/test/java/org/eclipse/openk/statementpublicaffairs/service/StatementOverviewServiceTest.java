/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.fail;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.PersistenceException;

import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationStatementOverviewService;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.model.SearchParams;
import org.eclipse.openk.statementpublicaffairs.model.db.TblDepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblReqdepartment;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatement;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatementEditLog;
import org.eclipse.openk.statementpublicaffairs.model.db.TblStatementtype;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementPositionSearch;
import org.eclipse.openk.statementpublicaffairs.model.db.VwStatementSearch;
import org.eclipse.openk.statementpublicaffairs.model.db.VwUserStatementSearch;
import org.eclipse.openk.statementpublicaffairs.repository.PagedAndSortedStatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.PagedAndSortedUserStatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.ReqDepartmentRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementEditLogRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementRepository;
import org.eclipse.openk.statementpublicaffairs.repository.StatementtypeRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementPositionRepository;
import org.eclipse.openk.statementpublicaffairs.repository.VwStatementPositionSearchSpecification;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.DashboardStatement;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementPosition;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementTaskModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementTypeModel;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest(classes = TestConfigurationStatementOverviewService.class)
@ActiveProfiles("test")

/**
 * Unit test for StatementOverviewService.
 * 
 * @author Tobias Stummer
 *
 */
class StatementOverviewServiceTest {

	@Autowired
	private StatementOverviewService statementOverviewService;

	@Autowired
	private StatementProcessService statementProcessService;

	@Autowired
	private StatementRepository statementRepository;

	@Autowired
	private StatementtypeRepository statementtypeRepository;

	@Autowired
	private ReqDepartmentRepository reqDepartmentRepository;

	@Autowired
	private PagedAndSortedStatementRepository pagedStatementRepository;

	@Autowired
	private PagedAndSortedUserStatementRepository pagedUserStatementRepository;

	@Autowired
	private StatementEditLogRepository statementEditLogRepository;

	@Autowired
	private UsersService usersService;

	@Autowired
	private VwStatementPositionRepository statementPositionRepository;

	@Autowired
	private UserInfoService userInfoService;

	@Autowired
	private StatementService statementService;

	@Test
	void testGetAllStatementModelsShouldRespondListWithAllStatements()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Boolean finished = true;
		Long id = 123L;
		String dueDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String receiptDate = TypeConversion.dateStringOfLocalDate(LocalDate.now()).get();
		String title = "title";
		String city = "city";
		String district = "district";
		Long statementypeId = 1L;
		TblStatementtype statementtype = new TblStatementtype();
		statementtype.setId(statementypeId);

		List<TblStatement> statements = new ArrayList<>();
		TblStatement tblstatement = new TblStatement();
		tblstatement.setFinished(finished);
		tblstatement.setId(id);
		tblstatement.setDueDate(TypeConversion.dateOfDateString(dueDate).get());
		tblstatement.setReceiptDate(TypeConversion.dateOfDateString(receiptDate).get());
		tblstatement.setTitle(title);
		tblstatement.setCity(city);
		tblstatement.setDistrict(district);
		tblstatement.setType(statementtype);
		statements.add(tblstatement);

		Mockito.when(statementRepository.findAll()).thenReturn(statements);

		List<StatementDetailsModel> all = statementOverviewService.getAllStatementModels();
		assertFalse(all.isEmpty());
		StatementDetailsModel s = all.get(0);

		assertEquals(finished, s.getFinished());
		assertEquals(id, s.getId());
		assertEquals(dueDate, s.getDueDate());
		assertEquals(receiptDate, s.getReceiptDate());
		assertEquals(title, s.getTitle());
		assertEquals(city, s.getCity());
		assertEquals(district, s.getDistrict());
		assertEquals(statementypeId, s.getTypeId());

	}

	@Test
	void testGetAllStatementModelsIllegalArgumentExceptionOnRepositoryAccessShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException {
		Mockito.doThrow(new IllegalArgumentException()).when(statementRepository).findAll();

		try {
			statementOverviewService.getAllStatementModels();
			fail("Should have thrown an InternalServiceErrorException");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void testGetAllStatementModelsPersistenceExceptionOnRepositoryAccessShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException {
		Mockito.doThrow(new PersistenceException()).when(statementRepository).findAll();

		try {
			statementOverviewService.getAllStatementModels();
			fail("Should have thrown an InternalServiceErrorException");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void testGetStatementTypeModelsShouldRespondListOfStatementTypeModels()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Long id = 123L;
		String name = "name";
		List<TblStatementtype> types = new ArrayList<>();
		TblStatementtype type = new TblStatementtype();
		type.setId(id);
		type.setName(name);
		type.setDisabled(false);
		types.add(type);

		Mockito.when(statementtypeRepository.findAll()).thenReturn(types);

		List<StatementTypeModel> all = statementOverviewService.getStatementTypeModels();
		assertFalse(all.isEmpty());

		StatementTypeModel s = all.get(0);
		assertEquals(id, s.getId());
		assertEquals(name, s.getName());
		assertFalse(s.isDisabled());

	}

	@Test
	void addStatementType() throws InternalErrorServiceException, ForbiddenServiceException {
		String label = "label";

		TblStatementtype statementType = new TblStatementtype();
		statementType.setName(label);
		statementType.setStatements(new ArrayList<>());
		statementType.setDisabled(false);

		statementOverviewService.addStatementType(label);
		Mockito.verify(statementtypeRepository).save(statementType);
		Mockito.verify(statementService).cleanupDeletableStatementTypes();
	}

	void addStatementTypeWithInternalArgumentExceptionOnSaveShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException {
		String label = "label";

		Mockito.doThrow(new IllegalArgumentException()).when(statementtypeRepository)
				.save(Mockito.any(TblStatementtype.class));
		try {
			statementOverviewService.addStatementType(label);
			fail("Should have thrown Exception");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void disableStatementType() throws InternalErrorServiceException, ForbiddenServiceException {
		String label = "label";
		TblStatementtype statementtype = Mockito.mock(TblStatementtype.class);

		Mockito.when(statementtypeRepository.findByName(label)).thenReturn(Optional.of(statementtype));

		statementOverviewService.disableStatementType(label);

		Mockito.verify(statementtype).setDisabled(true);
		Mockito.verify(statementtypeRepository).save(statementtype);
		Mockito.verify(statementService).cleanupDeletableStatementTypes();
	}

	@Test
	void disableStatementTypeWithStatementtypeNotExists()
			throws InternalErrorServiceException, ForbiddenServiceException {
		String label = "label";

		Mockito.when(statementtypeRepository.findByName(label)).thenReturn(Optional.empty());

		statementOverviewService.disableStatementType(label);

		Mockito.verify(statementtypeRepository, Mockito.never()).save(Mockito.any(TblStatementtype.class));
		Mockito.verify(statementService).cleanupDeletableStatementTypes();
	}

	@Test
	void disableStatementTypeWithIllegalArgumentExceptionOnSaveShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException {
		String label = "label";
		TblStatementtype statementtype = Mockito.mock(TblStatementtype.class);

		Mockito.when(statementtypeRepository.findByName(label)).thenReturn(Optional.of(statementtype));
		Mockito.doThrow(new IllegalArgumentException()).when(statementtypeRepository)
				.save(Mockito.any(TblStatementtype.class));

		try {
			statementOverviewService.disableStatementType(label);
			fail("Should have thrown Exception");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void testGetStatementTypeModelsIllegalArgumentExceptionOnRepositoryAccessShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException {

		Mockito.doThrow(new IllegalArgumentException()).when(statementtypeRepository).findAll();

		try {
			statementOverviewService.getStatementTypeModels();
			fail("Should have thrown an InternalServiceErrorException");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void testGetStatementTypeModelsPersistenceExceptionOnRepositoryAccessShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException {

		Mockito.doThrow(new PersistenceException()).when(statementtypeRepository).findAll();

		try {
			statementOverviewService.getStatementTypeModels();
			fail("Should have thrown an InternalServiceErrorException");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void getStatementModelsByInIdsWithRespondsWithListOfStatementModels()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Boolean finished = true;
		Long id = 1234L;
		LocalDate dueDate = LocalDate.now();
		LocalDate receiptDate = LocalDate.now();
		String dueDateS = TypeConversion.dateStringOfLocalDate(dueDate).get();
		String receiptDateS = TypeConversion.dateStringOfLocalDate(receiptDate).get();
		String title = "title";
		String district = "district";
		String city = "city";
		TblStatementtype statementtype = new TblStatementtype();
		statementtype.setId(1L);
		List<TblStatement> statements = new ArrayList<>();
		TblStatement statement = new TblStatement();
		statement.setFinished(finished);
		statement.setId(id);
		statement.setDueDate(dueDate);
		statement.setReceiptDate(receiptDate);
		statement.setTitle(title);
		statement.setDistrict(district);
		statement.setCity(city);
		statement.setType(statementtype);
		statements.add(statement);

		List<Long> statementIds = new ArrayList<>();
		statementIds.add(id);

		Mockito.when(statementRepository.findByIdIn(statementIds)).thenReturn(statements);
		List<StatementDetailsModel> rStatements = statementOverviewService.getStatementModelsByInIds(statementIds);

		StatementDetailsModel s = rStatements.get(0);
		assertEquals(finished, s.getFinished());
		assertEquals(id, s.getId());
		assertEquals(dueDateS, s.getDueDate());
		assertEquals(receiptDateS, s.getReceiptDate());
		assertEquals(title, s.getTitle());
		assertEquals(district, s.getDistrict());
		assertEquals(city, s.getCity());
		assertEquals(1L, s.getTypeId());
	}

	@Test
	void getStatementModelByInIdsIllegalArgumentExceptionAccessingRepositoryThrowsInternalErrorServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Long id = 1234L;

		List<Long> statementIds = new ArrayList<>();
		statementIds.add(id);
		Mockito.doThrow(new IllegalArgumentException()).when(statementRepository).findByIdIn(statementIds);

		try {
			statementOverviewService.getStatementModelsByInIds(statementIds);
			fail();
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void getStatementModelByInIdsPersistenceExceptionAccessingRepositoryThrowsInternalErrorServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Long id = 1234L;

		List<Long> statementIds = new ArrayList<>();
		statementIds.add(id);
		Mockito.doThrow(new PersistenceException()).when(statementRepository).findByIdIn(statementIds);

		try {
			statementOverviewService.getStatementModelsByInIds(statementIds);
			fail();
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void getDashboardStatements() throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		UUID businessKey = UUID.randomUUID();
		String businessKeyString = TypeConversion.stringOfBusinessKey(businessKey).get();

		LocalDate dueDate = LocalDate.now();
		LocalDate receiptDate = LocalDate.now();
		LocalDate creationDate = LocalDate.now();
		String dueDateS = TypeConversion.dateStringOfLocalDate(dueDate).get();
		String receiptDateS = TypeConversion.dateStringOfLocalDate(receiptDate).get();
		String creationDateS = TypeConversion.dateStringOfLocalDate(creationDate).get();

		String title = "title";
		String district = "district";
		String city = "city";
		Long typeId = 2L;
		String contactId = "contactId";
		String customerReference = "customerReference";

		String assignee = "assignee";
		String processDefinitionKey = "processDefinitionKey";
		String taskDefinitionKey = "taskDefinitionKey";
		String taskId = "taskId";

		Long ownDepartmentId = 23L;
		Long ownDepartmentId2 = 24L;
		Long ownDepartmentId3 = 25L;

		DashboardStatement expectedDS = new DashboardStatement();

		StatementDetailsModel model = new StatementDetailsModel();
		model.setFinished(true);
		model.setCanceled(false);
		model.setId(statementId);
		model.setBusinessKey(businessKeyString);
		model.setDueDate(dueDateS);
		model.setReceiptDate(receiptDateS);
		model.setCreationDate(creationDateS);
		model.setTitle(title);
		model.setDistrict(district);
		model.setCity(city);
		model.setTypeId(typeId);
		model.setContactId(contactId);
		model.setCustomerReference(customerReference);
		expectedDS.setInfo(model);

		List<TblStatement> statements = new ArrayList<>();
		TblStatement statement = new TblStatement();
		statement.setFinished(true);
		statement.setCanceled(false);
		statement.setId(statementId);
		statement.setBusinessKey(businessKeyString);
		statement.setDueDate(dueDate);
		statement.setReceiptDate(receiptDate);
		statement.setCreationDate(creationDate);
		statement.setTitle(title);
		statement.setDistrict(district);
		statement.setCity(city);
		TblStatementtype statementType = new TblStatementtype();
		statementType.setId(typeId);
		statement.setType(statementType);
		statement.setContactDbId(contactId);
		statement.setCustomerReference(customerReference);

		statements.add(statement);
		Mockito.when(statementRepository.findByFinishedAndCanceled(false, false)).thenReturn(statements);

		List<StatementTaskModel> tasks = new ArrayList<>();
		StatementTaskModel task = new StatementTaskModel();
		task.setAssignee(assignee);
		task.setAuthorized(true);
		task.setProcessDefinitionKey(processDefinitionKey);
		task.setRequiredVariables(new HashMap<>());
		task.setStatementId(statementId);
		task.setTaskDefinitionKey(taskDefinitionKey);
		task.setTaskId(taskId);
		tasks.add(task);
		Mockito.when(statementProcessService.getCurrentStatementTasks(statementId)).thenReturn(Optional.of(tasks));

		expectedDS.setTasks(tasks);
		expectedDS.setCompletedForMyDepartment(false);
		expectedDS.setEditedByMe(false);
		expectedDS.setMandatoryContributionsCount(1);
		expectedDS.setMandatoryDepartmentsCount(2);
		expectedDS.setOptionalForMyDepartment(false);
		expectedDS.setDepartmentStandIn(false);

		List<TblStatementEditLog> logs = new ArrayList<>();
		logs.add(new TblStatementEditLog());
		Mockito.when(statementEditLogRepository.findByStatementId(statementId)).thenReturn(logs);

		List<TblReqdepartment> reqDepartments = new ArrayList<>();
		TblReqdepartment reqDep1 = new TblReqdepartment();
		reqDep1.setContributed(true);
		reqDep1.setOptional(false);
		TblDepartment dep1 = new TblDepartment();
		dep1.setId(4711L);
		reqDep1.setDepartment(dep1);
		reqDepartments.add(reqDep1);

		TblReqdepartment reqDep2 = new TblReqdepartment();
		reqDep2.setContributed(true);
		reqDep2.setOptional(true);
		TblDepartment dep2 = new TblDepartment();
		dep2.setId(ownDepartmentId);
		reqDep2.setDepartment(dep2);
		reqDepartments.add(reqDep2);
		TblReqdepartment reqDep3 = new TblReqdepartment();
		reqDep3.setContributed(true);
		reqDep3.setOptional(true);
		TblDepartment dep3 = new TblDepartment();
		dep3.setId(ownDepartmentId2);
		reqDep3.setDepartment(dep3);
		reqDepartments.add(reqDep3);
		TblReqdepartment reqDep4 = new TblReqdepartment();
		reqDep4.setContributed(false);
		reqDep4.setOptional(false);
		TblDepartment dep4 = new TblDepartment();
		dep4.setId(ownDepartmentId3);
		reqDep4.setDepartment(dep4);
		reqDepartments.add(reqDep4);
		Mockito.when(reqDepartmentRepository.findByStatementId(statementId)).thenReturn(reqDepartments);

		Map<Long, Boolean> ownDepartmentStandIns = new HashMap<>();
		ownDepartmentStandIns.put(ownDepartmentId, true);
		ownDepartmentStandIns.put(ownDepartmentId2, true);
		ownDepartmentStandIns.put(ownDepartmentId3, false);

		Mockito.when(usersService.ownDepartmentsStandIns()).thenReturn(ownDepartmentStandIns);
		List<DashboardStatement> dashboardStatements = statementOverviewService.getDashboardStatements();
		assertFalse(dashboardStatements.isEmpty());
		assertEquals(1, dashboardStatements.size());
		assertEquals(expectedDS, dashboardStatements.get(0));

	}

	@Test
	void searchStatements() throws ForbiddenServiceException {
		SearchParams searchParams = Mockito.mock(SearchParams.class);
		Pageable pPageable = Mockito.mock(Pageable.class);

		List<VwStatementSearch> searchResultEntries = new ArrayList<>();
		VwStatementSearch searchResultEntry = new VwStatementSearch();
		boolean finished = true;
		Long statementId = 1234L;
		String businessKey = "businessKey";
		LocalDate creationDate = LocalDate.now();
		LocalDate dueDate = LocalDate.now();
		LocalDate receiptDate = LocalDate.now();

		String title = "title";
		String district = "district";
		String city = "city";
		String contactDbId = "contactDbId";
		Long statementtypeId = 1L;
		String customerReference = "customerReference";

		searchResultEntry.setFinished(finished);
		searchResultEntry.setId(statementId);
		searchResultEntry.setBusinessKey(businessKey);

		searchResultEntry.setCreationDate(creationDate);
		searchResultEntry.setDueDate(dueDate);
		searchResultEntry.setReceiptDate(receiptDate);

		searchResultEntry.setTitle(title);
		searchResultEntry.setDistrict(district);
		searchResultEntry.setCity(city);
		searchResultEntry.setContactDbId(contactDbId);
		searchResultEntry.setTypeId(statementtypeId);
		searchResultEntry.setCustomerReference(customerReference);

		searchResultEntries.add(searchResultEntry);
		Page<VwStatementSearch> resultPage = new PageImpl<>(searchResultEntries);
		Mockito.when(pagedStatementRepository.findAll(Mockito.any(), Mockito.eq(pPageable))).thenReturn(resultPage);
		Page<StatementDetailsModel> result = statementOverviewService.searchStatementModels(searchParams, pPageable);
		assertNotNull(result);
		assertEquals(1, result.getTotalElements());
		StatementDetailsModel sdm = result.getContent().get(0);
		assertEquals(sdm.getId(), statementId);

	}

	@Test
	void searchStatementsEditedByMe() throws ForbiddenServiceException {
		SearchParams searchParams = Mockito.mock(SearchParams.class);
		Pageable pPageable = Mockito.mock(Pageable.class);

		List<VwUserStatementSearch> searchResultEntries = new ArrayList<>();
		VwUserStatementSearch searchResultEntry = new VwUserStatementSearch();
		boolean finished = true;
		Long statementId = 1234L;
		String businessKey = "businessKey";
		LocalDate creationDate = LocalDate.now();
		LocalDate dueDate = LocalDate.now();
		LocalDate receiptDate = LocalDate.now();

		String title = "title";
		String district = "district";
		String city = "city";
		String contactDbId = "contactDbId";
		Long statementtypeId = 1L;
		String customerReference = "customerReference";

		searchResultEntry.setFinished(finished);
		searchResultEntry.setId(statementId);
		searchResultEntry.setBusinessKey(businessKey);

		searchResultEntry.setCreationDate(creationDate);
		searchResultEntry.setDueDate(dueDate);
		searchResultEntry.setReceiptDate(receiptDate);

		searchResultEntry.setTitle(title);
		searchResultEntry.setDistrict(district);
		searchResultEntry.setCity(city);
		searchResultEntry.setContactDbId(contactDbId);
		searchResultEntry.setTypeId(statementtypeId);
		searchResultEntry.setCustomerReference(customerReference);

		String username = "username";
		Mockito.when(userInfoService.getUserName()).thenReturn(username);
		Mockito.when(usersService.getUserId(username)).thenReturn(Optional.of(1L));
		searchResultEntries.add(searchResultEntry);
		Page<VwUserStatementSearch> resultPage = new PageImpl<>(searchResultEntries);
		Mockito.when(pagedUserStatementRepository.findAll(Mockito.any(), Mockito.eq(pPageable))).thenReturn(resultPage);
		Page<StatementDetailsModel> result = statementOverviewService.searchStatementModelsEditedByMe(searchParams,
				pPageable);
		assertNotNull(result);
		assertEquals(1, result.getTotalElements());
		StatementDetailsModel sdm = result.getContent().get(0);
		assertEquals(sdm.getId(), statementId);

	}

	@Test
	void searchStatementPositions() throws ForbiddenServiceException {
		Long id = 123L;
		Boolean finished = true;
		String position = "position";
		String title = "title";
		Long typeId = 23L;
		SearchParams params = new SearchParams();
		List<VwStatementPositionSearch> posSearchResults = new ArrayList<>();
		VwStatementPositionSearch posSearchResult = new VwStatementPositionSearch();
		posSearchResults.add(posSearchResult);
		posSearchResult.setId(id);
		posSearchResult.setFinished(finished);
		posSearchResult.setPosition(position);
		posSearchResult.setTitle(title);
		posSearchResult.setTypeId(typeId);
		Mockito.when(statementPositionRepository.findAll(Mockito.any(VwStatementPositionSearchSpecification.class)))
				.thenReturn(posSearchResults);
		List<StatementPosition> result = statementOverviewService.searchStatementPositions(params);
		assertFalse(result.isEmpty());
		assertEquals(1, result.size());
		StatementPosition res = result.get(0);
		assertEquals(id, res.getId());
		assertEquals(finished, res.getFinished());
		assertEquals(position, res.getPosition());
		assertEquals(title, res.getTitle());
		assertEquals(typeId, res.getTypeId());

	}

}
