/*
 *******************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.InputStream;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.eclipse.openk.statementpublicaffairs.config.TestConfigurationStatementProcessService;
import org.eclipse.openk.statementpublicaffairs.exceptions.BadRequestServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ConflictServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.ForbiddenServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.InternalErrorServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.NotFoundServiceException;
import org.eclipse.openk.statementpublicaffairs.exceptions.UnprocessableEntityServiceException;
import org.eclipse.openk.statementpublicaffairs.model.ProcessActivityHistoryModel;
import org.eclipse.openk.statementpublicaffairs.model.ProcessDefinitionModel;
import org.eclipse.openk.statementpublicaffairs.model.Tags;
import org.eclipse.openk.statementpublicaffairs.model.TaskInfo;
import org.eclipse.openk.statementpublicaffairs.model.WorkflowTaskModel;
import org.eclipse.openk.statementpublicaffairs.model.camunda.CamundaUserOperationLog;
import org.eclipse.openk.statementpublicaffairs.model.db.TblAttachment;
import org.eclipse.openk.statementpublicaffairs.util.TypeConversion;
import org.eclipse.openk.statementpublicaffairs.viewmodel.AttachmentModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ClaimDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.MailSendReport;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ProcessActivityHistoryViewModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.ProcessHistoryModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementDetailsModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.StatementTaskModel;
import org.eclipse.openk.statementpublicaffairs.viewmodel.WorkflowTaskVariableModel;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.util.ReflectionTestUtils;

@SpringBootTest(classes = TestConfigurationStatementProcessService.class)
@ActiveProfiles("test")

class StatementProcessServiceTest {

	private static final String STATEMENTPROCESSSERVICE_MEMBER_NAME_UNCLAIM_TIMEOUT_STATEMENT_ENABLE = "unclaimTimedoutStatementEnable";
	private static final String STATEMENTPROCESSSERVICE_MEMBER_NAME_UNCLAIM_TIMEOUT_MINUTES = "unclaimTimeoutMinutes";

	@Autowired
	private WorkflowService workflowService;

	@Autowired
	private StatementService statementService;

	@Autowired
	private UserInfoService userInfoService;

	@Autowired
	private StatementAuthorizationService authorizationService;

	@Autowired
	private StatementProcessService statementProcessService;

	@Test
	void testGetCurrentStatementTasksWithValidStatementIdShouldReturnListOfWorkflowProcessTasks()
			throws InternalErrorServiceException, ForbiddenServiceException {

		String businessKey = "businessKey";
		Long statementId = 1234L;

		List<WorkflowTaskModel> workflowTasks = new ArrayList<>();

		WorkflowTaskModel wftm = new WorkflowTaskModel();

		String assignee = "assignee";
		ZonedDateTime created = ZonedDateTime.now();
		String description = "description";
		ZonedDateTime due = ZonedDateTime.now();
		String formKey = "formKey";
		String name = "name";
		String owner = "owner";
		String processDefinitionKey = "processDefinitionKey";
		String processInstanceId = "processInstanceId";

		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var1", "Boolean");

		Boolean suspended = true;
		String taskDefinitionKey = "taskDefinitionKey";
		String taskId = "taskId";
		String tenantId = "tenantId";

		wftm.setAssignee(assignee);
		wftm.setCreated(created);
		wftm.setDescription(description);
		wftm.setDue(due);
		wftm.setFormKey(formKey);
		wftm.setName(name);
		wftm.setOwner(owner);
		wftm.setProcessDefinitionKey(processDefinitionKey);
		wftm.setProcessInstanceId(processInstanceId);
		wftm.setRequiredVariables(requiredVariables);
		wftm.setSuspended(suspended);
		wftm.setTaskDefinitionKey(taskDefinitionKey);
		wftm.setTaskId(taskId);
		wftm.setTenantId(tenantId);

		workflowTasks.add(wftm);

		StatementDetailsModel sdm = new StatementDetailsModel();
		sdm.setId(statementId);
		sdm.setBusinessKey(businessKey);
		sdm.setCanceled(false);


		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(sdm));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(workflowTasks));

		Optional<List<StatementTaskModel>> oTasks = statementProcessService.getCurrentStatementTasks(statementId);

		assertTrue(oTasks.isPresent());
		List<StatementTaskModel> tasks = oTasks.get();

		assertEquals(workflowTasks.size(), tasks.size());

		StatementTaskModel task = tasks.get(0);
		assertEquals(task.getAssignee(), assignee);
		assertEquals(task.getProcessDefinitionKey(), processDefinitionKey);
		assertEquals(task.getStatementId(), statementId);
		assertEquals(task.getTaskDefinitionKey(), taskDefinitionKey);
		assertEquals(task.getTaskId(), taskId);
		assertEquals(task.getRequiredVariables(), requiredVariables);
	}

	@Test
	void testGetCurrentStatementTasksWithInvalidStatementIdShouldReturnOptionalEmpty()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Long statementId = 404L;
		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.empty());

		assertFalse(statementProcessService.getCurrentStatementTasks(statementId).isPresent());
	}

	@Test
	void testGetCurrentStatementTasksWithValidStatementIdNotBoundToWorkflowProcessInstanceShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException, InternalErrorServiceException {
		Long statementId = 1234L;
		String businessKey = "businessKey";
		StatementDetailsModel sdm = new StatementDetailsModel();
		sdm.setId(statementId);
		sdm.setBusinessKey(businessKey);
		sdm.setCanceled(false);


		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(sdm));

		try {
			statementProcessService.getCurrentStatementTasks(statementId);
			fail();
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void testGetCurrentStatementTasksWithValidStatementIdBoundedWorkflowProcessNotExistShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException, InternalErrorServiceException {
		Long statementId = 1234L;
		StatementDetailsModel sdm = new StatementDetailsModel();
		sdm.setId(statementId);
		sdm.setBusinessKey(null);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(sdm));

		try {
			statementProcessService.getCurrentStatementTasks(statementId);
			fail();
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void testGetCurrentStatementTasksWithValidStatementIdWorkflowProcessDetailsNotAccessableShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException, InternalErrorServiceException {

		Long statementId = 1234L;
		String businessKey = "businessKey";
		StatementDetailsModel sdm = new StatementDetailsModel();
		sdm.setId(statementId);
		sdm.setBusinessKey(null);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(sdm));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey)).thenReturn(Optional.empty());

		try {
			statementProcessService.getCurrentStatementTasks(statementId);
			fail();
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void testGetCurrentStatementTasksWithValidStatementIdForbiddenShouldThrowForbiddenServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {
		String businessKey = "businessKey";
		Long statementId = 1234L;

		List<WorkflowTaskModel> workflowTasks = new ArrayList<>();

		WorkflowTaskModel wftm = new WorkflowTaskModel();

		String assignee = "assignee";
		ZonedDateTime created = ZonedDateTime.now();
		String description = "description";
		ZonedDateTime due = ZonedDateTime.now();
		String formKey = "formKey";
		String name = "name";
		String owner = "owner";
		String processDefinitionKey = "processDefinitionKey";
		String processInstanceId = "processInstanceId";

		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var1", "Boolean");

		Boolean suspended = true;
		String taskDefinitionKey = "taskDefinitionKey";
		String taskId = "taskId";
		String tenantId = "tenantId";

		wftm.setAssignee(assignee);
		wftm.setCreated(created);
		wftm.setDescription(description);
		wftm.setDue(due);
		wftm.setFormKey(formKey);
		wftm.setName(name);
		wftm.setOwner(owner);
		wftm.setProcessDefinitionKey(processDefinitionKey);
		wftm.setProcessInstanceId(processInstanceId);
		wftm.setRequiredVariables(requiredVariables);
		wftm.setSuspended(suspended);
		wftm.setTaskDefinitionKey(taskDefinitionKey);
		wftm.setTaskId(taskId);
		wftm.setTenantId(tenantId);

		workflowTasks.add(wftm);

		StatementDetailsModel sdm = new StatementDetailsModel();
		sdm.setId(statementId);
		sdm.setBusinessKey(businessKey);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(sdm));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(workflowTasks));

		Mockito.doThrow(new ForbiddenServiceException()).when(authorizationService).authorize(Mockito.eq("ANY"),
				Mockito.anyString());

		try {
			statementProcessService.getCurrentStatementTasks(statementId);
			fail();
		} catch (ForbiddenServiceException e) {
			// pass
		}
	}

	@Test
	void testGetStatementProcessHistoryWithValidStatementIdShouldRespondWithProcessHistory()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;

		String businessKey = "businessKey";

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);


		String processName = "processName";
		Long processVersion = 12L;
		String processDefinitionId = "processDefinitionId";
		String processDefinitionKey = "processDefinitionKey";
		String tenantId = "tenantId";

		ProcessDefinitionModel processDefinition = new ProcessDefinitionModel();
		processDefinition.setVersion(processVersion);
		processDefinition.setName(processName);

		String cActivityId = "cActivityId";
		String cActivityName = "cActivityName";
		String cActivityType = "cActivityType";
		String cAssignee = "cAssignee";
		Boolean cCanceled = false;
		Boolean cCompleteScope = true;
		Long cDurationInMillis = 123L;
		String cEndTime = null;
		String cId = "cId";
		ZonedDateTime czStartTime = ZonedDateTime.now();
		String cStartTime = TypeConversion.iso8601InstantStringOfZonedDateTime(czStartTime).get();
		ProcessActivityHistoryModel cpa = new ProcessActivityHistoryModel();
		cpa.setActivityId(cActivityId);
		cpa.setActivityName(cActivityName);
		cpa.setActivityType(cActivityType);
		cpa.setAssignee(cAssignee);
		cpa.setCanceled(cCanceled);
		cpa.setCompleteScope(cCompleteScope);
		cpa.setDurationInMillis(cDurationInMillis);
		cpa.setEndTime(null);
		cpa.setId(cId);
		cpa.setProcessDefinitionId(processDefinitionId);
		cpa.setProcessDefinitionKey(processDefinitionKey);
		cpa.setStartTime(czStartTime);
		cpa.setTenantId(tenantId);
		List<ProcessActivityHistoryModel> currentProcessActivities = new ArrayList<>();
		currentProcessActivities.add(cpa);

		String fActivityId = "fActivityId";
		String fActivityName = "fActivityName";
		String fActivityType = "fActivityType";
		String fAssignee = "fAssignee";
		Boolean fCanceled = false;
		Boolean fCompleteScope = true;
		Long fDurationInMillis = 1234L;
		String fId = "fId";
		ZonedDateTime fzStartTime = ZonedDateTime.now();
		ZonedDateTime fzEndTime = ZonedDateTime.now();

		ProcessActivityHistoryModel fpa = new ProcessActivityHistoryModel();
		fpa.setActivityId(fActivityId);
		fpa.setActivityName(fActivityName);
		fpa.setActivityType(fActivityType);
		fpa.setAssignee(fAssignee);
		fpa.setCanceled(fCanceled);
		fpa.setCompleteScope(fCompleteScope);
		fpa.setDurationInMillis(fDurationInMillis);
		fpa.setEndTime(fzEndTime);
		fpa.setId(fId);
		fpa.setProcessDefinitionId(processDefinitionId);
		fpa.setProcessDefinitionKey(processDefinitionKey);
		fpa.setStartTime(fzStartTime);
		fpa.setTenantId(tenantId);

		List<ProcessActivityHistoryModel> finishedProcessActivities = new ArrayList<>();
		finishedProcessActivities.add(fpa);

		List<ProcessActivityHistoryModel> processActivities = new ArrayList<>();
		processActivities.addAll(currentProcessActivities);
		processActivities.addAll(finishedProcessActivities);

		Mockito.when(statementService.getStatementInternal(statementId)).thenReturn(Optional.of(statementDetails));
		Mockito.when(workflowService.getProcessDefinition(businessKey)).thenReturn(Optional.of(processDefinition));
		Mockito.when(workflowService.getStatementProcessHistory(businessKey))
				.thenReturn(Optional.of(processActivities));

		Optional<ProcessHistoryModel> oHistory = statementProcessService.getStatmentProcessHistory(statementId);
		assertTrue(oHistory.isPresent());
		ProcessHistoryModel history = oHistory.get();

		assertEquals(history.getProcessName(), processName);
		assertEquals(history.getProcessVersion(), processVersion);

		assertEquals(history.getCurrentProcessActivities().size(), currentProcessActivities.size());
		assertEquals(history.getFinishedProcessActivities().size(), finishedProcessActivities.size());

		ProcessActivityHistoryViewModel cA = history.getCurrentProcessActivities().get(0);

		assertEquals(cA.getActivityId(), cActivityId);
		assertEquals(cA.getActivityName(), cActivityName);
		assertEquals(cA.getActivityType(), cActivityType);
		assertEquals(cA.getAssignee(), cAssignee);
		assertEquals(cA.getCanceled(), cCanceled);
		assertEquals(cA.getCompleteScope(), cCompleteScope);
		assertEquals(cA.getDurationInMillis(), cDurationInMillis);
		assertEquals(cA.getEndTime(), cEndTime);
		assertEquals(cA.getId(), cId);
		assertEquals(cA.getProcessDefinitionId(), processDefinitionId);
		assertEquals(cA.getProcessDefinitionKey(), processDefinitionKey);
		assertEquals(cA.getStartTime(), cStartTime);
		assertEquals(cA.getTenantId(), tenantId);

	}

	@Test
	void testGetStatementProcessHistoryWithInvalidStatementIdShouldRespondWithOptionalEmpty()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 404L;
		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.empty());
		assertFalse(statementProcessService.getStatmentProcessHistory(statementId).isPresent());
	}

	@Test
	void testGetStatementProcessHistoryWithValidStatementNotBoundToWorkflowProcessInstanceShouldThrowInternalErrorServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(null);

		Mockito.when(statementService.getStatementInternal(statementId)).thenReturn(Optional.of(statementDetails));
		try {
			statementProcessService.getStatmentProcessHistory(statementId);
			fail();
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void testGetStatementProcessHistoryWithValidStatementBoundedWorkflowProcessNotExistShouldThrowInternalErrorServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		String businessKey = "businessKey";

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);


		Mockito.when(statementService.getStatementInternal(statementId)).thenReturn(Optional.of(statementDetails));
		Mockito.when(workflowService.getProcessDefinition(businessKey)).thenReturn(Optional.empty());
		try {
			statementProcessService.getStatmentProcessHistory(statementId);
			fail();
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void testGetStatementProcessHistoryWithValidStatementWorkflowProcessHistoryNotAccessableShouldThrowInternalErrorServiceException()
			throws ForbiddenServiceException, InternalErrorServiceException {
		Long statementId = 1234L;
		String businessKey = "businessKey";
		String processName = "processName";
		Long processVersion = 12L;

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);


		ProcessDefinitionModel processDefinition = new ProcessDefinitionModel();
		processDefinition.setVersion(processVersion);
		processDefinition.setName(processName);

		Mockito.when(statementService.getStatementInternal(statementId)).thenReturn(Optional.of(statementDetails));
		Mockito.when(workflowService.getProcessDefinition(businessKey)).thenReturn(Optional.of(processDefinition));
		Mockito.when(workflowService.getStatementProcessHistory(businessKey)).thenReturn(Optional.empty());

		try {
			statementProcessService.getStatmentProcessHistory(statementId);
			fail();
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void testGetStatementProcessHistoryWithValidStatementIdForbiddenShouldThrowForbiddenServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		String businessKey = "businessKey";
		String processName = "processName";
		Long processVersion = 12L;

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);

		ProcessDefinitionModel processDefinition = new ProcessDefinitionModel();
		processDefinition.setVersion(processVersion);
		processDefinition.setName(processName);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));
		Mockito.when(workflowService.getProcessDefinition(businessKey)).thenReturn(Optional.of(processDefinition));
		Mockito.doThrow(new ForbiddenServiceException()).when(authorizationService).authorize(Mockito.anyString(),
				Mockito.anyString());

		try {
			statementProcessService.getStatmentProcessHistory(statementId);
			fail();
		} catch (ForbiddenServiceException e) {
			// pass
		}
	}

	@Test
	void testClaimStatementTaskWithValidStatementIdAndValidTaskIdNotClaimedShouldRespondWithWorkflowProcessTask()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {

		Long statementId = 1234L;
		String businessKey = "businessKey";
		String taskId = "taskId";
		String assignee = "assignee";
		String processDefinitionKey = "processDefinitionKey";
		String taskDefinitionKey = "taskDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);


		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(null);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		WorkflowTaskModel claimedTask = new WorkflowTaskModel();
		claimedTask.setAssignee(assignee);
		claimedTask.setTaskId(taskId);
		claimedTask.setProcessDefinitionKey(processDefinitionKey);
		claimedTask.setTaskDefinitionKey(taskDefinitionKey);
		claimedTask.setRequiredVariables(requiredVariables);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		Mockito.when(workflowService.claimTask(taskId, assignee)).thenReturn(Optional.of(claimedTask));

		Mockito.when(userInfoService.getUserName()).thenReturn(assignee);
		Optional<StatementTaskModel> oTaskModel = statementProcessService.claimStatementTask(statementId, taskId);
		assertTrue(oTaskModel.isPresent());
		StatementTaskModel taskModel = oTaskModel.get();

		assertEquals(assignee, taskModel.getAssignee());
		assertEquals(processDefinitionKey, taskModel.getProcessDefinitionKey());
		assertEquals(requiredVariables, taskModel.getRequiredVariables());
		assertEquals(statementId, taskModel.getStatementId());
		assertEquals(taskDefinitionKey, taskModel.getTaskDefinitionKey());
		assertEquals(taskId, taskModel.getTaskId());

	}

	@Test
	void testClaimStatementTaskWithInvalidStatementIdAndValidTaskIdNotClaimedShouldRespondWithOptionalEmpty()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {

		Long statementId = 404L;
		String taskId = "taskId";

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.empty());
		assertFalse(statementProcessService.claimStatementTask(statementId, taskId).isPresent());
	}

	@Test
	void testClaimStatementTaskWithValidStatementIdAndValidTaskIdNotClaimedClaimNotSuccessfulShouldThrowInternalErrorServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {

		Long statementId = 1234L;
		String businessKey = "businessKey";
		String taskId = "taskId";
		String assignee = "assignee";
		String processDefinitionKey = "processDefinitionKey";
		String taskDefinitionKey = "taskDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);


		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(null);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		Mockito.when(workflowService.claimTask(taskId, assignee)).thenReturn(Optional.empty());

		Mockito.when(userInfoService.getUserName()).thenReturn(assignee);

		try {
			statementProcessService.claimStatementTask(statementId, taskId);
			fail();
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void testClaimStatementTaskWithValidStatementIdAndValidTaskIdClaimedShouldThrowBadRequestServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {

		Long statementId = 1234L;
		String businessKey = "businessKey";
		String taskId = "taskId";
		String assignee = "assignee";
		String processDefinitionKey = "processDefinitionKey";
		String taskDefinitionKey = "taskDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);


		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		try {
			statementProcessService.claimStatementTask(statementId, taskId);
			fail();
		} catch (BadRequestServiceException e) {
			// pass
		}

	}

	@Test
	void testClaimStatementTaskWithValidStatementIdAndValidTaskIdForbiddenShouldThrowForbiddenServiceException()
			throws InternalErrorServiceException, BadRequestServiceException, ForbiddenServiceException {

		Long statementId = 1234L;
		String businessKey = "businessKey";
		String taskId = "taskId";
		String processDefinitionKey = "processDefinitionKey";
		String taskDefinitionKey = "taskDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);

		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(null);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));
		Mockito.doThrow(new ForbiddenServiceException()).when(authorizationService).authorize(Mockito.anyString(),
				Mockito.anyString());

		try {
			statementProcessService.claimStatementTask(statementId, taskId);
			fail();
		} catch (ForbiddenServiceException e) {
			// pass
		}
	}

	@Test
	void testUnClaimStatementTaskWithValidStatementIdAndValidTaskIdClaimedShouldRespondWithWorkflowProcessTask()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {

		Long statementId = 1234L;
		String businessKey = "businessKey";
		String taskId = "taskId";
		String assignee = "assignee";
		String processDefinitionKey = "processDefinitionKey";
		String taskDefinitionKey = "taskDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);

		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		WorkflowTaskModel unclaimedTask = new WorkflowTaskModel();
		unclaimedTask.setAssignee(null);
		unclaimedTask.setTaskId(taskId);
		unclaimedTask.setProcessDefinitionKey(processDefinitionKey);
		unclaimedTask.setTaskDefinitionKey(taskDefinitionKey);
		unclaimedTask.setRequiredVariables(requiredVariables);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		Mockito.when(workflowService.unClaimTask(taskId)).thenReturn(Optional.of(unclaimedTask));

		Mockito.when(userInfoService.getUserName()).thenReturn(assignee);
		Optional<StatementTaskModel> oTaskModel = statementProcessService.unClaimStatementTask(statementId, taskId);
		assertTrue(oTaskModel.isPresent());
		StatementTaskModel taskModel = oTaskModel.get();

		assertEquals(null, taskModel.getAssignee());
		assertEquals(processDefinitionKey, taskModel.getProcessDefinitionKey());
		assertEquals(requiredVariables, taskModel.getRequiredVariables());
		assertEquals(statementId, taskModel.getStatementId());
		assertEquals(taskDefinitionKey, taskModel.getTaskDefinitionKey());
		assertEquals(taskId, taskModel.getTaskId());

	}

	@Test
	void testUnClaimStatementTaskWithInvalidStatementIdAndValidTaskIdClaimedShouldRespondWithOptionalEmpty()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {

		Long statementId = 404L;
		String taskId = "taskId";

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.empty());
		assertFalse(statementProcessService.unClaimStatementTask(statementId, taskId).isPresent());
	}

	@Test
	void testUnClaimStatementTaskWithValidStatementIdAndValidTaskIdClaimedUnClaimNotSuccessfulShouldThrowInternalErrorServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {

		Long statementId = 1234L;
		String businessKey = "businessKey";
		String taskId = "taskId";
		String assignee = "assignee";
		String processDefinitionKey = "processDefinitionKey";
		String taskDefinitionKey = "taskDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);

		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		Mockito.when(workflowService.unClaimTask(taskId)).thenReturn(Optional.empty());

		try {
			statementProcessService.unClaimStatementTask(statementId, taskId);
			fail();
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void testUnClaimStatementTaskWithValidStatementIdAndValidTaskIdNotClaimedShouldThrowBadRequestServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Long statementId = 1234L;
		String businessKey = "businessKey";
		String taskId = "taskId";
		String processDefinitionKey = "processDefinitionKey";
		String taskDefinitionKey = "taskDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);


		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(null);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		try {
			statementProcessService.unClaimStatementTask(statementId, taskId);
			fail();
		} catch (BadRequestServiceException e) {
			// pass
		}
	}

	@Test
	void testUnClaimStatementTaskWithValidStatementIdAndValidTaskIdForbiddenShouldThrowForbiddenServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {
		Long statementId = 1234L;
		String businessKey = "businessKey";
		String taskId = "taskId";
		String assignee = "assignee";
		String processDefinitionKey = "processDefinitionKey";
		String taskDefinitionKey = "taskDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);

		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));
		Mockito.doThrow(new ForbiddenServiceException()).when(authorizationService).authorize(Mockito.anyString(),
				Mockito.anyString());

		try {
			statementProcessService.unClaimStatementTask(statementId, taskId);
			fail();
		} catch (ForbiddenServiceException e) {
			// pass
		}
	}

	@Test
	void testCompleteStatementTaskWithValidStatementIdAndValidTaskIdAndCompleteVariablesShouldRespndWithTrue()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException,
			NotFoundServiceException {
		Long statementId = 1234L;
		String businessKey = "businessKey";
		String taskId = "taskId";
		String assignee = "assignee";

		String processDefinitionKey = "processDefinitionKey";
		String taskDefinitionKey = "taskDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);


		Map<String, WorkflowTaskVariableModel> completeTaskVariables = new HashMap<>();
		WorkflowTaskVariableModel var = new WorkflowTaskVariableModel();
		var.setType("Boolean");
		var.setValue(true);
		completeTaskVariables.put("var", var);

		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(userInfoService.getUserName()).thenReturn(assignee);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));
		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		Mockito.when(workflowService.completeTask(Mockito.eq(statementId), Mockito.any(TaskInfo.class),
				Mockito.eq(completeTaskVariables))).thenReturn(Optional.of(true));

		Optional<Boolean> successful = statementProcessService.completeStatementTask(statementId, taskId,
				completeTaskVariables);
		assertTrue(successful.isPresent());
		assertTrue(successful.get());
	}

	@Test
	void testCompleteStatementTaskWithInvalidStatementIdShouldRespondWithOptionalEmpty()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException,
			NotFoundServiceException {
		Long statementId = 404L;
		String taskId = "taskId";

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.empty());
		assertFalse(statementProcessService.completeStatementTask(statementId, taskId, null).isPresent());

	}

	@Test
	void testCompleteStatementTaskWithValidStatementIdAndValidTaskIdForbiddenShouldThrowForbiddenServiceException()
			throws ForbiddenServiceException, InternalErrorServiceException, BadRequestServiceException,
			NotFoundServiceException {

		Long statementId = 1234L;
		String businessKey = "businessKey";
		String taskId = "taskId";
		String assignee = "assignee";

		String processDefinitionKey = "processDefinitionKey";
		String taskDefinitionKey = "taskDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);

		Map<String, WorkflowTaskVariableModel> completeTaskVariables = new HashMap<>();
		WorkflowTaskVariableModel var = new WorkflowTaskVariableModel();
		var.setType("Boolean");
		var.setValue(true);
		completeTaskVariables.put("var", var);

		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));
		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		Mockito.doThrow(new ForbiddenServiceException()).when(authorizationService).authorize(Mockito.anyString(),
				Mockito.anyString());

		try {
			statementProcessService.completeStatementTask(statementId, taskId, completeTaskVariables);
			fail();
		} catch (ForbiddenServiceException e) {
			// pass
		}

	}

	@Test
	void testCompleteStatementTaskWithValidStatementIdAndValidTaskIdAndCompleteVariablesNotClaimedShouldThrowBadRequestServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException {
		Long statementId = 1234L;
		String businessKey = "businessKey";
		String taskId = "taskId";

		String processDefinitionKey = "processDefinitionKey";
		String taskDefinitionKey = "taskDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);

		Map<String, WorkflowTaskVariableModel> completeTaskVariables = new HashMap<>();
		WorkflowTaskVariableModel var = new WorkflowTaskVariableModel();
		var.setType("Boolean");
		var.setValue(true);
		completeTaskVariables.put("var", var);

		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(null);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));
		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		try {
			statementProcessService.completeStatementTask(statementId, taskId, completeTaskVariables);
			fail();
		} catch (BadRequestServiceException e) {
			// pass
		}

	}

	@Test
	void testCompleteStatementTaskWithValidStatementIdAndValidTaskIdAndInCompleteVariablesShouldThrowBadRequestServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException,
			NotFoundServiceException {
		Long statementId = 1234L;
		String businessKey = "businessKey";
		String taskId = "taskId";

		String processDefinitionKey = "processDefinitionKey";
		String taskDefinitionKey = "taskDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);

		Map<String, WorkflowTaskVariableModel> completeTaskVariables = new HashMap<>();

		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(null);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));
		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		TaskInfo taskInfo = Mockito.mock(TaskInfo.class);
		Mockito.doThrow(new BadRequestServiceException()).when(workflowService).completeTask(statementId, taskInfo,
				completeTaskVariables);

		try {
			statementProcessService.completeStatementTask(statementId, taskId, completeTaskVariables);
			fail();
		} catch (BadRequestServiceException e) {
			// pass
		}

	}

	@Test
	void testCreateStatementWithValidStatementShouldRespondWithStatementDetails()
			throws InternalErrorServiceException, BadRequestServiceException, ForbiddenServiceException,
			NotFoundServiceException, InterruptedException, UnprocessableEntityServiceException {
		Long statementId = 1234L;
		String fakeTaskId = "fakeTaskId";
		String businessKey = "businessKey";

		StatementDetailsModel statementModel = new StatementDetailsModel();
		statementModel.setId(statementId);
		statementModel.setBusinessKey(fakeTaskId);

		StatementDetailsModel createdModel = new StatementDetailsModel();
		createdModel.setId(statementId);
		createdModel.setBusinessKey(businessKey);

		Mockito.when(workflowService.startProcessLatestVersion()).thenReturn(businessKey);

		Mockito.when(statementService.createStatement(createdModel)).thenReturn(Optional.of(createdModel));

		Optional<StatementDetailsModel> oStatement = statementProcessService.createStatement(statementModel);
		assertTrue(oStatement.isPresent());
		assertEquals(createdModel, oStatement.get());

	}

	@Test
	void testCreateStatementWithValidStatementCreationFailedShouldRespondWithOptionalEmpty()
			throws InternalErrorServiceException, BadRequestServiceException, ForbiddenServiceException,
			NotFoundServiceException, InterruptedException, UnprocessableEntityServiceException {
		Long statementId = 1234L;
		String fakeTaskId = "fakeTaskId";
		String businessKey = "businessKey";

		StatementDetailsModel statementModel = new StatementDetailsModel();
		statementModel.setId(statementId);
		statementModel.setBusinessKey(fakeTaskId);

		StatementDetailsModel createdModel = new StatementDetailsModel();
		createdModel.setId(statementId);
		createdModel.setBusinessKey(businessKey);

		Mockito.when(workflowService.startProcessLatestVersion()).thenReturn(businessKey);

		Mockito.when(statementService.createStatement(createdModel)).thenReturn(Optional.empty());

		assertFalse(statementProcessService.createStatement(statementModel).isPresent());
	}

	@Test
	void testCreateStatementWithValidStatementStatementServiceThrowsInternalErrorServiceExceptionShouldDeleteProcessInstanceAndThrowInternalErrorServiceException()
			throws BadRequestServiceException, InternalErrorServiceException, ForbiddenServiceException,
			NotFoundServiceException, InterruptedException, UnprocessableEntityServiceException {
		Long statementId = 1234L;
		String fakeTaskId = "fakeTaskId";
		String businessKey = "businessKey";

		StatementDetailsModel statementModel = new StatementDetailsModel();
		statementModel.setId(statementId);
		statementModel.setBusinessKey(fakeTaskId);

		StatementDetailsModel createdModel = new StatementDetailsModel();
		createdModel.setId(statementId);
		createdModel.setBusinessKey(businessKey);

		Mockito.when(workflowService.startProcessLatestVersion()).thenReturn(businessKey);
		Mockito.doThrow(new InternalErrorServiceException()).when(statementService).createStatement(Mockito.any());

		try {
			statementProcessService.createStatement(statementModel);
			fail();
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void getStatementWorkflowWithValidStatementIdShouldRespondWithXMLValue()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Long statementId = 1234L;
		String businessKey = "businessKey";
		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		String processDefinitionId = "processDefinitionId";
		ProcessDefinitionModel processDefinition = new ProcessDefinitionModel();
		processDefinition.setId(processDefinitionId);

		Mockito.when(workflowService.getProcessDefinition(businessKey))
				.thenReturn(Optional.of(processDefinition));

		String xmlValue = "xmlValue";

		Mockito.when(workflowService.getWorkflowDefinitionXML(processDefinitionId))
				.thenReturn(Optional.of(xmlValue));

		Optional<String> oXmlValue = statementProcessService.getStatementWorkflow(statementId);
		assertTrue(oXmlValue.isPresent());
		assertEquals(xmlValue, oXmlValue.get());
	}

	@Test
	void getStatementWorkflowWithInvalidStatementIdShouldRespondWithOptionalEmpty()
			throws InternalErrorServiceException, ForbiddenServiceException {

		Long statementId = 404L;

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.empty());

		assertFalse(statementProcessService.getStatementWorkflow(statementId).isPresent());

	}

	@Test
	void getStatementWorkflowBusinessKeyIsNullShouldThrowInternalErrorServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		String businessKey = null;
		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		try {
			statementProcessService.getStatementWorkflow(statementId);
			fail("Should have thrown InternalErrorServiceException");
		} catch (InternalErrorServiceException e) {
			// pass
		}
	}

	@Test
	void getStatementWorkflowNoProcessDefinitionFoundForBusinessKeyShouldThrowInternalErrorServiceException()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		String businessKey = "businessKey";
		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);


		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getProcessDefinition(businessKey)).thenReturn(Optional.empty());

		try {
			statementProcessService.getStatementWorkflow(statementId);
			fail("Should have thrown InternalErrorServiceException");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void getTaskInfoWithValidStatementIdTaskIdShouldRespondWithTaskInfo()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		String taskId = "taskId";
		String taskDefinitionKey = "taskDefinitionKey";
		String assignee = "assignee";

		String businessKey = "businessKey";
		String processDefinitionKey = "processDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);

		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		Optional<TaskInfo> oTaskInfo = statementProcessService.getTaskInfo(statementId, taskId);
		assertTrue(oTaskInfo.isPresent());
		TaskInfo taskInfo = oTaskInfo.get();
		assertEquals(assignee, taskInfo.getAssignee());
		assertEquals(taskDefinitionKey, taskInfo.getTaskDefinitionKey());
		assertEquals(taskId, taskInfo.getTaskId());
	}

	@Test
	void getTaskInfoWithInvalidStatementIdTaskIdShouldRespondWithOptionalEmpty()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 404L;
		String taskId = "taskId";

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.empty());

		assertFalse(statementProcessService.getTaskInfo(statementId, taskId).isPresent());
	}

	@Test
	void getTaskInfoWithValidStatementIdInvlaidTaskIdShouldRespondWithOptionalEmpty()
			throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		String taskId = "taskId";

		String businessKey = "businessKey";

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);


		List<WorkflowTaskModel> currentTasks = new ArrayList<>();

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		assertFalse(statementProcessService.getTaskInfo(statementId, taskId).isPresent());

	}

	@Test
	void updateStatementWithValidStatementIdTaskidStatementModelShouldReturnStatementModel()
			throws NotFoundServiceException, BadRequestServiceException, InternalErrorServiceException,
			ForbiddenServiceException, UnprocessableEntityServiceException {

		Long statementId = 1234L;
		String taskId = "taskId";

		StatementDetailsModel savedStatement = Mockito.mock(StatementDetailsModel.class);

		StatementDetailsModel statement = new StatementDetailsModel();
		statement.setId(statementId);

		Mockito.when(statementService.updateStatement(statementId, statement))
				.thenReturn(Optional.of(savedStatement));

		Optional<StatementDetailsModel> result = statementProcessService.updateStatement(statementId, taskId,
				statement);
		assertTrue(result.isPresent());
		assertEquals(savedStatement, result.get());

	}

	@Test
	void updateStatementWithNotMatchingStatmentIdShouldThrowBadRequestServiceException()
			throws ForbiddenServiceException, InternalErrorServiceException, NotFoundServiceException,
			UnprocessableEntityServiceException {

		Long statementId = 1234L;
		String taskId = "taskId";

		StatementDetailsModel statement = new StatementDetailsModel();
		statement.setId(2345L);

		try {
			statementProcessService.updateStatement(statementId, taskId, statement);
			fail("Should have thrown BadRequestServiceException");
		} catch (BadRequestServiceException e) {
			// pass
		}
	}

	@Test
	void setAttachmentTags() throws ForbiddenServiceException, InternalErrorServiceException, NotFoundServiceException,
			BadRequestServiceException {
		Long statementId = 1234L;
		String taskId = "taskId";
		Long attachmentId = 1L;
		Set<String> tagIds = new HashSet<>();
		String t1 = "t1";
		tagIds.add(t1);

		String businessKey = "businessKey";
		String taskDefinitionKey = "taskDefinitionKey";
		String assignee = "assignee";

		String processDefinitionKey = "processDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);


		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		statementProcessService.setAttachmentTags(statementId, taskId, attachmentId, tagIds);

		Mockito.verify(statementService).setTags(statementId, attachmentId, tagIds);
	}

	@Test
	void createStatementAttachment() throws InternalErrorServiceException, ForbiddenServiceException,
			NotFoundServiceException, BadRequestServiceException {

		Long statementId = 1234L;
		String taskId = "taskId";
		Long attachmentId = 1L;
		Set<String> tagIds = new HashSet<>();
		String t1 = "t1";
		String statementTag = Tags.STATEMENT.tagId();
		tagIds.add(t1);
		tagIds.add(statementTag);

		String fileName = "fileName";
		String fileType = "fileType";
		InputStream inputStream = Mockito.mock(InputStream.class);
		long length = 12L;

		String businessKey = "businessKey";
		String taskDefinitionKey = "taskDefinitionKey";
		String assignee = "assignee";

		String processDefinitionKey = "processDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);


		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));
		TblAttachment attachmenttbl = new TblAttachment();
		attachmenttbl.setId(attachmentId);
		attachmenttbl.setFileName(fileName);
		attachmenttbl.setSize(length);
		attachmenttbl.setFileType(fileType);
		Mockito.when(statementService.setTags(attachmentId, tagIds)).thenReturn(tagIds);

		Mockito.when(statementService.createStatementTblAttachment(Mockito.eq(statementId), Mockito.eq(fileName),
				Mockito.eq(fileType), Mockito.any(), Mockito.eq(length))).thenReturn(Optional.of(attachmenttbl));

		List<AttachmentModel> attachments = new ArrayList<>();
		AttachmentModel attachment = new AttachmentModel();
		attachments.add(attachment);
		Long oldStatementAttachmentId = 99L;
		attachment.setId(oldStatementAttachmentId);
		List<String> oldStatementAttachmentTagIds = new ArrayList<>();
		oldStatementAttachmentTagIds.add("foo");
		oldStatementAttachmentTagIds.add("bar");
		oldStatementAttachmentTagIds.add(statementTag);

		attachment.setTagIds(oldStatementAttachmentTagIds);

		Mockito.when(statementService.getStatementAttachments(statementId))
				.thenReturn(Optional.of(attachments));

		Mockito.when(statementService.createStatementAttachment(statementId, fileName, fileType, inputStream, length))
				.thenReturn(Optional.of(attachmentId));
		Optional<AttachmentModel> newAttachmentId = statementProcessService
				.createStatementAttachmentToModel(statementId, taskId, fileName, fileType, inputStream, length, tagIds);

		assertTrue(newAttachmentId.isPresent());
		assertEquals(attachmentId, newAttachmentId.get().getId());

		Mockito.verify(statementService).deleteStatementAttachments(Mockito.anyString(), Mockito.eq(statementId),
				Mockito.eq(oldStatementAttachmentId), Mockito.anyString());
	}

	@Test
	void deleteStatementAttachments()
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException {

		Long statementId = 1234L;
		String taskId = "taskId";
		Long attachmentId = 1L;

		String businessKey = "businessKey";
		String taskDefinitionKey = "taskDefinitionKey";
		String assignee = "assignee";

		String processDefinitionKey = "processDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);

		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		statementProcessService.deleteStatementAttachments(statementId, taskId, attachmentId);
		Mockito.verify(statementService).deleteStatementAttachments(Mockito.anyString(), Mockito.eq(statementId),
				Mockito.eq(attachmentId), Mockito.anyString());
	}

	@Test
	void dispatchStatementResponse()
			throws NotFoundServiceException, InternalErrorServiceException, ForbiddenServiceException, BadRequestServiceException {

		Long statementId = 1234L;
		String taskId = "taskId";

		String businessKey = "businessKey";
		String taskDefinitionKey = "taskDefinitionKey";
		String assignee = "assignee";

		String processDefinitionKey = "processDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);

		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		MailSendReport report = new MailSendReport();
		Mockito.when(statementService.dispatchStatementResponse(statementId)).thenReturn(report);

		MailSendReport response = statementProcessService.dispatchStatementResponseAndCompleteTask(statementId, taskId);
		assertEquals(report, response);

	}

	
	@Test
	void getClaimDetails() throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException {
		
		
		Long statementId = 1234L;
		String taskId = "taskId";
		String taskDefinitionKey = "taskDefinitionKey";
		String assignee = "assignee";

		String businessKey = "businessKey";
		String processDefinitionKey = "processDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);


		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		
		CamundaUserOperationLog logEntry = new CamundaUserOperationLog();
		ZonedDateTime logTimestamp = ZonedDateTime.now().minusMinutes(30);
		TypeConversion.camundaDateStringOfZDT(logTimestamp).ifPresent(logEntry::setTimestamp);
	
		Mockito.when(workflowService.getLatestUserOperation(taskId)).thenReturn(Optional.of(logEntry));

		int claimDeltaMinutes = 35;
		ReflectionTestUtils.setField(statementProcessService, STATEMENTPROCESSSERVICE_MEMBER_NAME_UNCLAIM_TIMEOUT_MINUTES, claimDeltaMinutes);
		ReflectionTestUtils.setField(statementProcessService, STATEMENTPROCESSSERVICE_MEMBER_NAME_UNCLAIM_TIMEOUT_STATEMENT_ENABLE, true);
	
		
		ZonedDateTime now = ZonedDateTime.now();
		ClaimDetailsModel claimDetails = statementProcessService.getClaimDetails(statementId, taskId);
		
		
		assertEquals(assignee, claimDetails.getAssignee());

		ZonedDateTime testClaimedUntilTime = TypeConversion.zonedDateTimeOfIso8601InstantString(claimDetails.getClaimedUntilTime()).orElse(null);

		assertTrue(logTimestamp.plusMinutes(claimDeltaMinutes).truncatedTo(ChronoUnit.MINUTES).isEqual(testClaimedUntilTime.truncatedTo(ChronoUnit.MINUTES)));
		
		ZonedDateTime testCurrentTime = TypeConversion.zonedDateTimeOfIso8601InstantString(claimDetails.getCurrentTime()).orElse(null);
		assertTrue(now.isEqual(testCurrentTime) || now.isBefore(testCurrentTime));
		assertEquals(true, claimDetails.getCurrent());
		assertEquals(statementId, claimDetails.getStatementId());
		assertEquals(taskId, claimDetails.getTaskId());
	}
	
	@Test
	void getClaimDetailsNoTaskInfoReturnsOnlyBasicDetails() throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException {

		Long statementId = 1234L;
		String taskId = "invalidTaskId";
		int claimDeltaMinutes = 35;
		ReflectionTestUtils.setField(statementProcessService, STATEMENTPROCESSSERVICE_MEMBER_NAME_UNCLAIM_TIMEOUT_MINUTES, claimDeltaMinutes);
		ReflectionTestUtils.setField(statementProcessService, STATEMENTPROCESSSERVICE_MEMBER_NAME_UNCLAIM_TIMEOUT_STATEMENT_ENABLE, true);
	
		
		ZonedDateTime now = ZonedDateTime.now();
		ClaimDetailsModel claimDetails = statementProcessService.getClaimDetails(statementId, taskId);
		
		assertNull(claimDetails.getAssignee());
		assertNull(claimDetails.getClaimedUntilTime());
		assertEquals(false, claimDetails.getCurrent());
		
		ZonedDateTime testCurrentTime = TypeConversion.zonedDateTimeOfIso8601InstantString(claimDetails.getCurrentTime()).orElse(null);
		assertTrue(now.isEqual(testCurrentTime) || now.isBefore(testCurrentTime));
		assertEquals(statementId, claimDetails.getStatementId());
		assertEquals(taskId, claimDetails.getTaskId());

	}
	
	@Test
	void getClaimDetailsWithValidCurrentTaskIdAndWithoutAssigneeReturnsBasicDetails() throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException {

		Long statementId = 1234L;
		String taskId = "taskId";
		String taskDefinitionKey = "taskDefinitionKey";
		String assignee = null;

		String businessKey = "businessKey";
		String processDefinitionKey = "processDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);

		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		
		CamundaUserOperationLog logEntry = new CamundaUserOperationLog();
		ZonedDateTime logTimestamp = ZonedDateTime.now().minusMinutes(30);
		TypeConversion.camundaDateStringOfZDT(logTimestamp).ifPresent(logEntry::setTimestamp);
	
		Mockito.when(workflowService.getLatestUserOperation(taskId)).thenReturn(Optional.of(logEntry));

		int claimDeltaMinutes = 35;
		ReflectionTestUtils.setField(statementProcessService, STATEMENTPROCESSSERVICE_MEMBER_NAME_UNCLAIM_TIMEOUT_MINUTES, claimDeltaMinutes);
		ReflectionTestUtils.setField(statementProcessService, STATEMENTPROCESSSERVICE_MEMBER_NAME_UNCLAIM_TIMEOUT_STATEMENT_ENABLE, true);

		
		
		
		ZonedDateTime now = ZonedDateTime.now();
		ClaimDetailsModel claimDetails = statementProcessService.getClaimDetails(statementId, taskId);

		assertNull(claimDetails.getAssignee());
		assertNull(claimDetails.getClaimedUntilTime());
		assertEquals(true, claimDetails.getCurrent());
		
		ZonedDateTime testCurrentTime = TypeConversion.zonedDateTimeOfIso8601InstantString(claimDetails.getCurrentTime()).orElse(null);
		assertTrue(now.isEqual(testCurrentTime) || now.isBefore(testCurrentTime));
		assertEquals(statementId, claimDetails.getStatementId());
		assertEquals(taskId, claimDetails.getTaskId());
		
	}

	@Test
	void getClaimDetailsWithValidCurrentTaskIdAndWithAssigneeAndUnclaimTimeoutNotEnabledReturnsBasicDetails() throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException {
		
		Long statementId = 1234L;
		String taskId = "taskId";
		String taskDefinitionKey = "taskDefinitionKey";
		String assignee = null;

		String businessKey = "businessKey";
		String processDefinitionKey = "processDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);

		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		
		CamundaUserOperationLog logEntry = new CamundaUserOperationLog();
		ZonedDateTime logTimestamp = ZonedDateTime.now().minusMinutes(30);
		TypeConversion.camundaDateStringOfZDT(logTimestamp).ifPresent(logEntry::setTimestamp);
	
		Mockito.when(workflowService.getLatestUserOperation(taskId)).thenReturn(Optional.of(logEntry));

		int claimDeltaMinutes = 35;
		ReflectionTestUtils.setField(statementProcessService, STATEMENTPROCESSSERVICE_MEMBER_NAME_UNCLAIM_TIMEOUT_MINUTES, claimDeltaMinutes);
		ReflectionTestUtils.setField(statementProcessService, STATEMENTPROCESSSERVICE_MEMBER_NAME_UNCLAIM_TIMEOUT_STATEMENT_ENABLE, false);

		
		
		
		ZonedDateTime now = ZonedDateTime.now();
		ClaimDetailsModel claimDetails = statementProcessService.getClaimDetails(statementId, taskId);

		assertNull(claimDetails.getAssignee());
		assertNull(claimDetails.getClaimedUntilTime());
		assertEquals(true, claimDetails.getCurrent());
		
		ZonedDateTime testCurrentTime = TypeConversion.zonedDateTimeOfIso8601InstantString(claimDetails.getCurrentTime()).orElse(null);
		assertTrue(now.isEqual(testCurrentTime) || now.isBefore(testCurrentTime));
		assertEquals(statementId, claimDetails.getStatementId());
		assertEquals(taskId, claimDetails.getTaskId());	
		
	}
	
	@Test
	void touchNewUserOperation() throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 1234L;
		String taskId = "taskId";
		String taskDefinitionKey = "taskDefinitionKey";
		String assignee = "assignee";

		String businessKey = "businessKey";
		String processDefinitionKey = "processDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);

		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));
		Mockito.when(userInfoService.getUserName()).thenReturn(assignee);
		statementProcessService.touchNewUserOperation(statementId, taskId);
		Mockito.verify(userInfoService, Mockito.times(2)).getUserName();
		Mockito.verify(workflowService, Mockito.times(1)).touchNewUserOperation(taskId);
	}
	
	
	@Test
	void touchNewUserOperationNotCurrentOrInvalidTaskIdDoesNotTouchUserOperation() throws InternalErrorServiceException, ForbiddenServiceException {
		Long statementId = 123L;
		String taskId = "taskId";
		statementProcessService.touchNewUserOperation(statementId, taskId);
		Mockito.verify(userInfoService, Mockito.never()).getUserName();
		Mockito.verify(workflowService, Mockito.never()).touchNewUserOperation(taskId);
		
	}

	@Test
	void touchNewUserOperationCurrentTaskAndAssigneeDoesNotMatchRequestingUserDoesNotTouchUserOperation() throws InternalErrorServiceException, ForbiddenServiceException {

		Long statementId = 1234L;
		String taskId = "taskId";
		String taskDefinitionKey = "taskDefinitionKey";
		String assignee = "assignee";

		String businessKey = "businessKey";
		String processDefinitionKey = "processDefinitionKey";
		Map<String, String> requiredVariables = new HashMap<>();
		requiredVariables.put("var", "Boolean");

		StatementDetailsModel statementDetails = new StatementDetailsModel();
		statementDetails.setBusinessKey(businessKey);
		statementDetails.setCanceled(false);


		List<WorkflowTaskModel> currentTasks = new ArrayList<>();
		WorkflowTaskModel currentTask = new WorkflowTaskModel();
		currentTask.setAssignee(assignee);
		currentTask.setTaskId(taskId);
		currentTask.setProcessDefinitionKey(processDefinitionKey);
		currentTask.setTaskDefinitionKey(taskDefinitionKey);
		currentTask.setRequiredVariables(requiredVariables);
		currentTasks.add(currentTask);

		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetails));

		Mockito.when(workflowService.getCurrentTasksOfProcessBusinessKey(businessKey))
				.thenReturn(Optional.of(currentTasks));

		Mockito.when(userInfoService.getUserName()).thenReturn("otherUsej");
		statementProcessService.touchNewUserOperation(statementId, taskId);
		Mockito.verify(userInfoService, Mockito.times(2)).getUserName();
		Mockito.verify(workflowService, Mockito.never()).touchNewUserOperation(taskId);
		
		
	}
	
	
	@Test
	void cancelStatement() throws ForbiddenServiceException, NotFoundServiceException, InternalErrorServiceException, ConflictServiceException, BadRequestServiceException {
		Long statementId = 123L;
		String oldBusinessKey = "oldBk";

		Mockito.when(statementService.cancelStatement(statementId)).thenReturn(oldBusinessKey);
		statementProcessService.cancelStatement(statementId);
	
		Mockito.verify(workflowService).deleteProcessInstance(oldBusinessKey);
		
	}

	@Test
	void cancelStatementNoOldBk() throws ForbiddenServiceException, NotFoundServiceException, InternalErrorServiceException, ConflictServiceException, BadRequestServiceException {
		Long statementId = 123L;
		String oldBusinessKey = null;

		Mockito.when(statementService.cancelStatement(statementId)).thenReturn(oldBusinessKey);
		
		statementProcessService.cancelStatement(statementId);
	
		Mockito.verify(workflowService,Mockito.never()).deleteProcessInstance(Mockito.anyString());
		
	}

	@Test
	void cancelStatementBadRequestServiceExceptionOnDeleteProcess() throws ForbiddenServiceException, NotFoundServiceException, InternalErrorServiceException, ConflictServiceException, BadRequestServiceException {
		Long statementId = 123L;
		String oldBusinessKey = "oldBk";

		Mockito.when(statementService.cancelStatement(statementId)).thenReturn(oldBusinessKey);
		
		Mockito.doThrow(new BadRequestServiceException()).when(workflowService).deleteProcessInstance(oldBusinessKey);
		
		try {
			statementProcessService.cancelStatement(statementId);
			fail("Should have thrown Exception");
		} catch (InternalErrorServiceException e) {
			// pass
		}

	}

	@Test
	void reviveStatement() throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException, ConflictServiceException {
		Long statementId = 123L;
		String newBk = "newBk";
		
		StatementDetailsModel statementDetailsModel = new StatementDetailsModel();
		statementDetailsModel.setCanceled(true);
		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetailsModel));
		Mockito.when(workflowService.startProcessLatestVersion()).thenReturn(newBk);

		statementProcessService.reviveStatement(statementId);
		
		Mockito.verify(statementService).reviveStatement(statementId, newBk);
		
	}

	@Test
	void reviveStatementNotFound() throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException, ConflictServiceException {
		Long statementId = 123L;
		
		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.empty());

		statementProcessService.reviveStatement(statementId);
		
		Mockito.verify(workflowService, Mockito.never()).startProcessLatestVersion();
		Mockito.verify(statementService, Mockito.never()).reviveStatement(Mockito.anyLong(), Mockito.anyString());
		
	}

	@Test
	void reviveStatementNotCanceled() throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException, ConflictServiceException {
		Long statementId = 123L;
		
		StatementDetailsModel statementDetailsModel = new StatementDetailsModel();
		statementDetailsModel.setCanceled(false);
		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetailsModel));

		statementProcessService.reviveStatement(statementId);
		
		Mockito.verify(workflowService, Mockito.never()).startProcessLatestVersion();
		Mockito.verify(statementService, Mockito.never()).reviveStatement(Mockito.anyLong(), Mockito.anyString());

	}
	
	@Test
	void reviveStatementExceptionOnReviveAndOnCleanup() throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException, ConflictServiceException, BadRequestServiceException {
		Long statementId = 123L;
		String newBk = "newBk";
		
		StatementDetailsModel statementDetailsModel = new StatementDetailsModel();
		statementDetailsModel.setCanceled(true);
		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetailsModel));
		Mockito.when(workflowService.startProcessLatestVersion()).thenReturn(newBk);
		Mockito.doThrow(new ConflictServiceException()).when(statementService).reviveStatement(statementId, newBk);
		Mockito.doThrow(new BadRequestServiceException()).when(workflowService).deleteProcessInstance(newBk);

		try {
			statementProcessService.reviveStatement(statementId);
			fail("Should have thrown Exception");
		} catch (InternalErrorServiceException e) {
			// pass
		}
		
	}

	@Test
	void reviveStatementConflictExceptionOnRevive() throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException, ConflictServiceException, BadRequestServiceException {
		Long statementId = 123L;
		String newBk = "newBk";
		
		StatementDetailsModel statementDetailsModel = new StatementDetailsModel();
		statementDetailsModel.setCanceled(true);
		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetailsModel));
		Mockito.when(workflowService.startProcessLatestVersion()).thenReturn(newBk);
		Mockito.doThrow(new ConflictServiceException()).when(statementService).reviveStatement(statementId, newBk);

		try {
			statementProcessService.reviveStatement(statementId);
			fail("Should have thrown Exception");
		} catch (ConflictServiceException e) {
			// pass
		}
		
	}

	@Test
	void reviveStatementNotFoundExceptionOnRevive() throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException, ConflictServiceException, BadRequestServiceException {
		Long statementId = 123L;
		String newBk = "newBk";
		
		StatementDetailsModel statementDetailsModel = new StatementDetailsModel();
		statementDetailsModel.setCanceled(true);
		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetailsModel));
		Mockito.when(workflowService.startProcessLatestVersion()).thenReturn(newBk);
		Mockito.doThrow(new NotFoundServiceException()).when(statementService).reviveStatement(statementId, newBk);

		try {
			statementProcessService.reviveStatement(statementId);
			fail("Should have thrown Exception");
		} catch (NotFoundServiceException e) {
			// pass
		}
		
	}

	@Test
	void reviveStatementUnknownExceptionOnRevive() throws InternalErrorServiceException, ForbiddenServiceException, NotFoundServiceException, ConflictServiceException, BadRequestServiceException {
		Long statementId = 123L;
		String newBk = "newBk";
		
		StatementDetailsModel statementDetailsModel = new StatementDetailsModel();
		statementDetailsModel.setCanceled(true);
		Mockito.when(statementService.getStatement(statementId)).thenReturn(Optional.of(statementDetailsModel));
		Mockito.when(workflowService.startProcessLatestVersion()).thenReturn(newBk);
		Mockito.doThrow(new NullPointerException()).when(statementService).reviveStatement(statementId, newBk);

		try {
			statementProcessService.reviveStatement(statementId);
			fail("Should have thrown Exception");
		} catch (InternalErrorServiceException e) {
			// pass
		}
		
	}
	
}
