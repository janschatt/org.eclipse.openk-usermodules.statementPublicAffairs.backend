/*
 *******************************************************************************
 * Copyright (c) 2022 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************
*/
package org.eclipse.openk.statementpublicaffairs.util;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.openk.statementpublicaffairs.model.Textblock;
import org.eclipse.openk.statementpublicaffairs.model.Textblock.TextblockType;
import org.junit.jupiter.api.Test;

class StatementDraftUtilsTest {
	

	@Test
	void testEqualIfnoreBothNull() {
		
		List<Textblock> notNull = new ArrayList<>();
		List<Textblock> notEmpty = new ArrayList<>();
		List<Textblock> notEmpty2 = new ArrayList<>();
		notEmpty.add(new Textblock());
		notEmpty2.add(new Textblock());

		assertTrue(StatementDraftUtils.equalIgnoreBothNull(null, null));
		assertFalse(StatementDraftUtils.equalIgnoreBothNull(null, notNull));
		assertFalse(StatementDraftUtils.equalIgnoreBothNull(notNull, null));
		assertFalse(StatementDraftUtils.equalIgnoreBothNull(notNull, notEmpty));
		assertTrue(StatementDraftUtils.equalIgnoreBothNull(notEmpty, notEmpty2));
	}

	@Test
	void test() {
		List<Textblock> textArrangement = new ArrayList<Textblock>();
		Textblock tbBlock = new Textblock();
		tbBlock.setType(TextblockType.block);
		textArrangement.add(tbBlock);

		Textblock tbText = new Textblock();
		tbText.setType(TextblockType.text);
		textArrangement.add(tbText);

		Textblock tbNewline = new Textblock();
		tbNewline.setType(TextblockType.newline);
		textArrangement.add(tbNewline);
		
		Textblock tbPagebreak = new Textblock();
		tbPagebreak.setType(TextblockType.pagebreak);
		textArrangement.add(tbPagebreak);

		StatementDraftUtils.fillEmptyTextblockUids(textArrangement);
		
		assertNull(tbBlock.getTextblockId());
		assertNotNull(tbText.getTextblockId());
		assertNotNull(tbNewline.getTextblockId());
		assertNotNull(tbPagebreak.getTextblockId());
	}

}
